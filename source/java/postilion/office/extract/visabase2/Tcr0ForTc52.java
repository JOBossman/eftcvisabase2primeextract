package postilion.office.extract.visabase2;

import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;
import postilion.core.util.Convert;

public class Tcr0ForTc52 extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String ACCOUNT_NUMBER = new String("acc number");
        public static final String ACCOUNT_NUMBER_EXT = new String("acc number ext");
        public static final String ACQ_REFERENCE_NO = new String("acq reference no");
        public static final String ACQ_BUSINESS_ID = new String("acq business id");
        public static final String PURCHASE_DATE = new String("purchase date");
        public static final String TRAN_AMOUNT = new String("tran amount");
        public static final String TRAN_CURRENCY_CODE = new String("tran currency code");
        public static final String MERCHANT_NAME = new String("merchant name");
        public static final String MERCHANT_CITY = new String("merchant city");
        public static final String MERCHANT_COUNTRY_CODE = new String("merchant country code");
        public static final String MERCHANT_CATEGORY_CODE = new String("merchant category code");
        public static final String MERCHANT_ZIP_CODE = new String("merchant zip code");
        public static final String MERCHANT_STATE_CODE = new String("merchant state/province code");
        public static final String ISSUER_CONTROL_NO = new String("issuer control no");
        public static final String REQUEST_REASON_CODE = new String("request reason code");
        public static final String SETTLEMENT_FLAG = new String("settlement flag");
        public static final String NATIONAL_REIMBURSEMENT_FEE = new String("national reimbursement fee");
        public static final String ATM_ACCOUNT_SELECTION = new String("atm account selection");
        public static final String RETRIEVAL_REQUEST_ID = new String("retrieval request id");
        public static final String CENTRAL_PROCESSING_DATE = new String("central processing date");
        public static final String REIMBURSEMENT_ATTR = new String("reimbursement attribute");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr0ForTc52()
    {
        super(168, stream);
        putField(Field.TRANSACTION_CODE, "52");
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "0");
        putField(Field.REIMBURSEMENT_ATTR, "0");
        putField(Field.CENTRAL_PROCESSING_DATE, "0000");
        putField(Field.RETRIEVAL_REQUEST_ID, Convert.resize("", 12, ' ', true));
    }

    public static void main(String args[])
    {
        Tcr0ForTc52 tcr0 = new Tcr0ForTc52();
        System.out.println(tcr0.describeStructure());
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN(), 1);
        IStreamFormatter acc_number = new StreamFormatterFieldFixed(Field.ACCOUNT_NUMBER, Validator.getN(), 19);
        IStreamFormatter acq_reference_no = new StreamFormatterFieldFixed(Field.ACQ_REFERENCE_NO, Validator.getNs(), 23);
        IStreamFormatter acq_business_id = new StreamFormatterFieldFixed(Field.ACQ_BUSINESS_ID, Validator.getN(), 8);
        IStreamFormatter purchase_date = new StreamFormatterFieldFixed(Field.PURCHASE_DATE, Validator.getN(), 4);
        IStreamFormatter tran_amount = new StreamFormatterFieldFixed(Field.TRAN_AMOUNT, Validator.getN(), 12, '0', false, true);
        IStreamFormatter tran_currency_code = new StreamFormatterFieldFixed(Field.TRAN_CURRENCY_CODE, Validator.getN(), 3);
        IStreamFormatter merchant_name = new StreamFormatterFieldFixed(Field.MERCHANT_NAME, Validator.getAns(), 25);
        IStreamFormatter merchant_city = new StreamFormatterFieldFixed(Field.MERCHANT_CITY, Validator.getAns(), 13);
        IStreamFormatter merchant_country_code = new StreamFormatterFieldFixed(Field.MERCHANT_COUNTRY_CODE, Validator.getAns(), 3);
        IStreamFormatter merchant_category_code = new StreamFormatterFieldFixed(Field.MERCHANT_CATEGORY_CODE, Validator.getN(), 4);
        IStreamFormatter merchant_zip_code = new StreamFormatterFieldFixed(Field.MERCHANT_ZIP_CODE, Validator.getN(), 5);
        IStreamFormatter merchant_state_code = new StreamFormatterFieldFixed(Field.MERCHANT_STATE_CODE, Validator.getAns(), 3);
        IStreamFormatter issuer_control_no = new StreamFormatterFieldFixed(Field.ISSUER_CONTROL_NO, Validator.getNs(), 9);
        IStreamFormatter request_reason_code = new StreamFormatterFieldFixed(Field.REQUEST_REASON_CODE, Validator.getN(), 2);
        IStreamFormatter settlement_flag = new StreamFormatterFieldFixed(Field.SETTLEMENT_FLAG, Validator.getN(), 1);
        IStreamFormatter national_reimbursement_fee = new StreamFormatterFieldFixed(Field.NATIONAL_REIMBURSEMENT_FEE, Validator.getN(), 12, '0', false, true);
        IStreamFormatter atm_account_selection = new StreamFormatterFieldFixed(Field.ATM_ACCOUNT_SELECTION, Validator.getAns(), 1);
        IStreamFormatter retrieval_request_id = new StreamFormatterFieldFixed(Field.RETRIEVAL_REQUEST_ID, Validator.getAns(), 12);
        IStreamFormatter central_processing_Date = new StreamFormatterFieldFixed(Field.CENTRAL_PROCESSING_DATE, Validator.getN(), 4);
        IStreamFormatter reimbursement_attr = new StreamFormatterFieldFixed(Field.REIMBURSEMENT_ATTR, Validator.getAn(), 1);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(acc_number);
        msg.add(acq_reference_no);
        msg.add(acq_business_id);
        msg.add(purchase_date);
        msg.add(tran_amount);
        msg.add(tran_currency_code);
        msg.add(merchant_name);
        msg.add(merchant_city);
        msg.add(merchant_country_code);
        msg.add(merchant_category_code);
        msg.add(merchant_zip_code);
        msg.add(merchant_state_code);
        msg.add(issuer_control_no);
        msg.add(request_reason_code);
        msg.add(settlement_flag);
        msg.add(national_reimbursement_fee);
        msg.add(atm_account_selection);
        msg.add(retrieval_request_id);
        msg.add(central_processing_Date);
        msg.add(reimbursement_attr);
        stream = msg;
    }
}
