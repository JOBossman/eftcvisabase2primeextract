package postilion.office.extract.visabase2;


import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

class CollectionOnlyFlagChecker
{

    private Hashtable sink_node_to_bin_table;
    private Hashtable acquirer_to_bin_table;

    public boolean isCollectionOnlyTransaction(String sink_node_name, String acquirer_id, String pan)
    {
        sink_node_name = processName(sink_node_name);
        boolean found_in_sink_bin_list = false;
        Vector sink_bin_list = (Vector)sink_node_to_bin_table.get(sink_node_name);
        if(sink_bin_list != null)
        {
            found_in_sink_bin_list = foundMatchingBin(pan, sink_bin_list);
        }
        acquirer_id = processName(acquirer_id);
        boolean found_in_acquirer_bin_list = false;
        Vector acquirer_bin_list = (Vector)acquirer_to_bin_table.get(acquirer_id);
        if(acquirer_bin_list != null)
        {
            found_in_acquirer_bin_list = foundMatchingBin(pan, acquirer_bin_list);
        }
        return found_in_sink_bin_list || found_in_acquirer_bin_list;
    }

    public CollectionOnlyFlagChecker()
    {
        sink_node_to_bin_table = new Hashtable();
        acquirer_to_bin_table = new Hashtable();
    }

    public CollectionOnlyFlagChecker(Hashtable combined_hashtable, String sink_node_names[])
    {
        sink_node_to_bin_table = new Hashtable();
        acquirer_to_bin_table = new Hashtable();
        acquirer_to_bin_table = prepareTable(combined_hashtable);
        sink_node_to_bin_table = removeSinkNodes(acquirer_to_bin_table, sink_node_names);
    }

    private boolean foundMatchingBin(String pan, Vector bin_list)
    {
        for(int i = 0; i < bin_list.size(); i++)
        {
            String bin_prefix = (String)bin_list.elementAt(i);
            if(pan.startsWith(bin_prefix))
            {
                return true;
            }
        }

        return false;
    }

    private Hashtable prepareTable(Hashtable combined_hashtable)
    {
        Hashtable result = new Hashtable();
        String name;
        Vector bin_list;
        for(Enumeration keys = combined_hashtable.keys(); keys.hasMoreElements(); result.put(processName(name), bin_list))
        {
            name = (String)keys.nextElement();
            String delimited_string = (String)combined_hashtable.get(name);
            StringTokenizer tokenizer = new StringTokenizer(delimited_string, ",", false);
            bin_list = new Vector();
            while(tokenizer.hasMoreTokens())
            {
                String bin = tokenizer.nextToken().trim();
                if(bin.length() != 0)
                {
                    bin_list.addElement(bin);
                }
            }
        }

        return result;
    }

    Hashtable getSinkNodeToBinTable()
    {
        return sink_node_to_bin_table;
    }

    Hashtable getAcquirerToBinTable()
    {
        return acquirer_to_bin_table;
    }

    private Hashtable removeSinkNodes(Hashtable source, String sink_node_names[])
    {
        Hashtable result = new Hashtable();
        int i = 0;
        for(int len = sink_node_names.length; i < len; i++)
        {
            String sink_node_name = processName(sink_node_names[i]);
            if(source.containsKey(sink_node_name))
            {
                Object bin_list = source.remove(sink_node_name);
                result.put(sink_node_name, bin_list);
            }
        }

        return result;
    }

    private String processName(String key)
    {
        return key.trim().toLowerCase();
    }
}
