package postilion.office.extract.visabase2;

import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;
import postilion.core.util.Convert;

public class Tcr0ForTc48 extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String DESTINATION_BIN = new String("dest bin");
        public static final String SOURCE_BIN = new String("source bin");
        public static final String FORMAT_CODE = new String("format code");
        public static final String ACCOUNT_NUMBER = new String("acc number");
        public static final String AUTH_RESP_CODE = new String("auth resp code");
        public static final String DESTINATION_AMOUNT = new String("dest_amount");
        public static final String DESTINATION_CURRENCY_CODE = new String("dest curr code");
        public static final String SOURCE_AMOUNT = new String("source amount");
        public static final String SOURCE_CURRENCY_CODE = new String("src curr code");
        public static final String POS_ENTRY_MODE = new String("pos entry mode");
        public static final String POS_TERMINAL_CAPABILITY = new String("pos term cap");
        public static final String MERCHANT_NAME = new String("merchant name");
        public static final String MERCHANT_CITY = new String("merchant city");
        public static final String MERCHANT_COUNTRY_CODE = new String("merchant country code");
        public static final String MERCHANT_ZIP_CODE = new String("merchant zip code");
        public static final String RESERVED = new String("reserved");
        public static final String MERCHANT_STATE_CODE = new String("merchant state/province code");
        public static final String CARD_ACCEPTOR_ID = new String("card acceptor id");
        public static final String TERMINAL_ID = new String("terminal id");
        public static final String MERCHANT_CATEGORY_CODE = new String("merchant category code");
        public static final String RESERVED_2 = new String("reserved 2");
        public static final String REIMBURSEMENT_ATTR = new String("reimbursement attr");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr0ForTc48()
    {
        super(168, stream);
        putField(Field.TRANSACTION_CODE, "48");
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "0");
        putField(Field.FORMAT_CODE, "2");
        putField(Field.RESERVED, Convert.resize("", 4, ' ', true));
        putField(Field.RESERVED_2, Convert.resize("", 7, ' ', true));
        putField(Field.REIMBURSEMENT_ATTR, "0");
    }

    public static void main(String args[])
    {
        Tcr0ForTc48 tcr0 = new Tcr0ForTc48();
        System.out.println(tcr0.describeStructure());
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN(), 1);
        IStreamFormatter dest_bin = new StreamFormatterFieldFixed(Field.DESTINATION_BIN, Validator.getN(), 6);
        IStreamFormatter source_bin = new StreamFormatterFieldFixed(Field.SOURCE_BIN, Validator.getN(), 6);
        IStreamFormatter format_code = new StreamFormatterFieldFixed(Field.FORMAT_CODE, Validator.getN(), 1);
        IStreamFormatter acc_number = new StreamFormatterFieldFixed(Field.ACCOUNT_NUMBER, Validator.getAns(), 28);
        IStreamFormatter auth_resp_code = new StreamFormatterFieldFixed(Field.AUTH_RESP_CODE, Validator.getAns(), 2);
        IStreamFormatter destination_amount = new StreamFormatterFieldFixed(Field.DESTINATION_AMOUNT, Validator.getN(), 12, '0', false, true);
        IStreamFormatter destination_currency_code = new StreamFormatterFieldFixed(Field.DESTINATION_CURRENCY_CODE, Validator.getN(), 3);
        IStreamFormatter source_amount = new StreamFormatterFieldFixed(Field.SOURCE_AMOUNT, Validator.getN(), 12, '0', false, true);
        IStreamFormatter source_currency_code = new StreamFormatterFieldFixed(Field.SOURCE_CURRENCY_CODE, Validator.getN(), 3);
        IStreamFormatter pos_entry_mode = new StreamFormatterFieldFixed(Field.POS_ENTRY_MODE, Validator.getN(), 2);
        IStreamFormatter pos_term_cap = new StreamFormatterFieldFixed(Field.POS_TERMINAL_CAPABILITY, Validator.getAns(), 1);
        IStreamFormatter merchant_name = new StreamFormatterFieldFixed(Field.MERCHANT_NAME, Validator.getAns(), 25);
        IStreamFormatter merchant_city = new StreamFormatterFieldFixed(Field.MERCHANT_CITY, Validator.getAns(), 13);
        IStreamFormatter merchant_country_code = new StreamFormatterFieldFixed(Field.MERCHANT_COUNTRY_CODE, Validator.getAns(), 3);
        IStreamFormatter merchant_zip_code = new StreamFormatterFieldFixed(Field.MERCHANT_ZIP_CODE, Validator.getN(), 5);
        IStreamFormatter reserved = new StreamFormatterFieldFixed(Field.RESERVED, Validator.getAns(), 4);
        IStreamFormatter merchant_state_code = new StreamFormatterFieldFixed(Field.MERCHANT_STATE_CODE, Validator.getAns(), 3);
        IStreamFormatter card_acceptor_id = new StreamFormatterFieldFixed(Field.CARD_ACCEPTOR_ID, Validator.getAns(), 15);
        IStreamFormatter term_id = new StreamFormatterFieldFixed(Field.TERMINAL_ID, Validator.getAns(), 8);
        IStreamFormatter merchant_category_code = new StreamFormatterFieldFixed(Field.MERCHANT_CATEGORY_CODE, Validator.getN(), 4);
        IStreamFormatter reserved_2 = new StreamFormatterFieldFixed(Field.RESERVED_2, Validator.getAns(), 7);
        IStreamFormatter reimbursement_attr = new StreamFormatterFieldFixed(Field.REIMBURSEMENT_ATTR, Validator.getAn(), 1);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(dest_bin);
        msg.add(source_bin);
        msg.add(format_code);
        msg.add(acc_number);
        msg.add(auth_resp_code);
        msg.add(destination_amount);
        msg.add(destination_currency_code);
        msg.add(source_amount);
        msg.add(source_currency_code);
        msg.add(pos_entry_mode);
        msg.add(pos_term_cap);
        msg.add(merchant_name);
        msg.add(merchant_city);
        msg.add(merchant_country_code);
        msg.add(merchant_zip_code);
        msg.add(reserved);
        msg.add(merchant_state_code);
        msg.add(card_acceptor_id);
        msg.add(term_id);
        msg.add(merchant_category_code);
        msg.add(reserved_2);
        msg.add(reimbursement_attr);
        stream = msg;
    }
}
