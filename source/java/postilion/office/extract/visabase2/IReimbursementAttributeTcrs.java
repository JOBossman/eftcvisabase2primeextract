package postilion.office.extract.visabase2;


public interface IReimbursementAttributeTcrs
{

    public abstract String getReimbursementAttribute(Tcr0 tcr0, Tcr1 tcr1, Tcr5 tcr5, Tcr7 tcr7);
}

