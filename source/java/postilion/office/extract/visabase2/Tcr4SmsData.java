package postilion.office.extract.visabase2;


import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;

public class Tcr4SmsData extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String RESERVED = new String("reserved");
        public static final String BUSINESS_FORMAT_CODE = new String("business format code");
        public static final String DEBIT_PRODUCT_CODE = new String("debit product code");
        public static final String CONTACT_INFORMATION = new String("contact information");
        public static final String ADJUSTMENT_PROCESSING_INDICATOR = new String("adjustment processing indicator");
        public static final String MESSAGE_REASON_CODE = new String("message reason code");
        public static final String SURCHARGE_AMOUNT = new String("surcharge amount");
        public static final String SURCHARGE_INDICATOR = new String("surcharge amount indicator");
        public static final String VISA_INTERNAL_USE = new String("visa internal use");
        public static final String FILLER = new String("filler");
        public static final String SURCHARGE_CARDHOLDER_BILLING = new String("surcharge cardholder billing");
        public static final String TCR4_REMAINDER = new String("tcr4 remainder");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr4SmsData()
    {
        super(168, stream);
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "4");
        putField(Field.BUSINESS_FORMAT_CODE, "SD");
    }

    public static void main(String args[])
    {
        Tcr4SmsData tcr4 = new Tcr4SmsData();
        System.out.println(tcr4.describeStructure());
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN(), 1);
        IStreamFormatter reserved = new StreamFormatterFieldFixed(Field.RESERVED, Validator.getAns(), 10, ' ', true, true);
        IStreamFormatter business_format_code = new StreamFormatterFieldFixed(Field.BUSINESS_FORMAT_CODE, Validator.getAns(), 2);
        IStreamFormatter debit_product_code = new StreamFormatterFieldFixed(Field.DEBIT_PRODUCT_CODE, Validator.getN(), 4);
        IStreamFormatter contact_information = new StreamFormatterFieldFixed(Field.CONTACT_INFORMATION, Validator.getAns(), 25, ' ', true, true);
        IStreamFormatter adjustment_processing_indicator = new StreamFormatterFieldFixed(Field.ADJUSTMENT_PROCESSING_INDICATOR, Validator.getAns(), 1);
        IStreamFormatter message_reason_code = new StreamFormatterFieldFixed(Field.MESSAGE_REASON_CODE, Validator.getAns(), 4);
        IStreamFormatter surcharge_amount = new StreamFormatterFieldFixed(Field.SURCHARGE_AMOUNT, Validator.getN(), 8, '0', false, true);
        IStreamFormatter surcharge_indicator = new StreamFormatterFieldFixed(Field.SURCHARGE_INDICATOR, Validator.getAns(), 2, ' ', true, true);
        IStreamFormatter visa_internal_use = new StreamFormatterFieldFixed(Field.VISA_INTERNAL_USE, Validator.getAns(), 16, ' ', true, true);
        IStreamFormatter filler = new StreamFormatterFieldFixed(Field.FILLER, Validator.getAns(), 27, ' ', true, true);
        IStreamFormatter surcharge_cardholder_billing = new StreamFormatterFieldFixed(Field.SURCHARGE_CARDHOLDER_BILLING, Validator.getN(), 8, '0', false, true);
        IStreamFormatter tcr4_remainder = new StreamFormatterFieldFixed(Field.TCR4_REMAINDER, Validator.getAns(), 57, ' ', true, true);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(reserved);
        msg.add(business_format_code);
        msg.add(debit_product_code);
        msg.add(contact_information);
        msg.add(adjustment_processing_indicator);
        msg.add(message_reason_code);
        msg.add(surcharge_amount);
        msg.add(surcharge_indicator);
        msg.add(visa_internal_use);
        msg.add(filler);
        msg.add(surcharge_cardholder_billing);
        msg.add(tcr4_remainder);
        stream = msg;
    }
}
