package postilion.office.extract.visabase2;


public class CpsAtmRA
        implements IReimbursementAttribute
{

    public CpsAtmRA()
    {
    }

    public String getReimbursementAttribute(Tcr0 tcr0)
    {
        String auth_char = tcr0.getField(Tcr0.Field.AUTH_CHARACTERISTICS);
        if(auth_char != null && (auth_char.equals("E") || auth_char.equals("M") || auth_char.equals("C")))
        {
            return "H";
        } else
        {
            return "2";
        }
    }
}

