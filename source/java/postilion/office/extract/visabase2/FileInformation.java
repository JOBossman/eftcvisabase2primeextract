package postilion.office.extract.visabase2;


import java.io.PrintWriter;

// Referenced classes of package postilion.office.extract.visabase2:
//            BatchInformation

public class FileInformation
{

    public String sink_node_name;
    public String full_file_name;
    public int extract_session_id;
    public PrintWriter settle_file;
    public PrintWriter log_file;
    public BatchInformation batch_info;

    public FileInformation(String full_file_name, int extract_session_id, PrintWriter settle_file, PrintWriter log_file, BatchInformation batch_info)
    {
        this.extract_session_id = 0;
        this.settle_file = null;
        this.log_file = null;
        this.batch_info = null;
        this.full_file_name = full_file_name;
        this.extract_session_id = extract_session_id;
        this.settle_file = settle_file;
        this.log_file = log_file;
        this.batch_info = batch_info;
    }
}
