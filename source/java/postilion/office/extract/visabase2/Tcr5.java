package postilion.office.extract.visabase2;


import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;
import postilion.core.util.Convert;

public class Tcr5 extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String TRANSACTION_ID = new String("transaction id");
        public static final String AUTHORIZATION_AMOUNT = new String("authorized amount");
        public static final String AUTH_CURRENCY_CODE = new String("auth currency code");
        public static final String AUTH_RESP_CODE = new String("auth resp code");
        public static final String VALIDATION_CODE = new String("validation code");
        public static final String EXCLUDED_TRAN_ID = new String("excluded tran id");
        public static final String CRS_PROCESSING_CODE = new String("crs processing code");
        public static final String CHARGE_BACK_RIGHTS = new String("charge back rights");
        public static final String MULTIPLE_CLEARING_SEQ = new String("multiple clearing seq");
        public static final String MULTIPLE_CLEARING_CNT = new String("multiple clearing cnt");
        public static final String MARKET_AUTH_DATA = new String("market auth data");
        public static final String TOTAL_AUTH_AMOUNT = new String("total auth amount");
        public static final String INFORMATION_INDICATOR = new String("information indicator");
        public static final String MERCHANT_TEL = new String("merchant tel");
        public static final String ADD_DATA_INDICATOR = new String("additional data indicator");
        public static final String RESERVED1 = new String("reserved1");
        public static final String MERCHANT_VERIFICATION_VALUE = new String("merchant verification value");
        public static final String RESERVED2 = new String("reserved2");
        public static final String PRODUCT_ID = new String("product id");
        public static final String RESERVED3 = new String("reserved3");
        public static final String DCC_INDICATOR = new String("dcc_indicator");
        public static final String RESERVED4 = new String("reserved4");
        public static final String CVV2_RESULT_VALUE = new String("cvv2 result value");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr5()
    {
        super(168, stream);
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "5");
        putField(Field.EXCLUDED_TRAN_ID, " ");
        putField(Field.CHARGE_BACK_RIGHTS, "  ");
        putField(Field.MULTIPLE_CLEARING_SEQ, "01");
        putField(Field.MULTIPLE_CLEARING_CNT, "01");
    }

    public static void main(String args[])
    {
        try
        {
            Tcr5 tcr5 = new Tcr5();
            tcr5.fromMsg("050512345678911111100000000100084000        0101 000000000000                   " + "      " +
                    "                                                                          " + "       M");
            tcr5.putField(Field.CVV2_RESULT_VALUE, "M");
            System.out.println(Convert.getString(tcr5.toMsg()));
            System.out.println(tcr5.describeStructure());
        }
        catch (Exception e)
        {
        }
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN
                (), 1);
        IStreamFormatter transaction_id = new StreamFormatterFieldFixed(Field.TRANSACTION_ID, Validator.getNs(), 15);
        IStreamFormatter authorized_amount = new StreamFormatterFieldFixed(Field.AUTHORIZATION_AMOUNT, Validator.getN
                (), 12, '0', false, true);
        IStreamFormatter auth_currency_code = new StreamFormatterFieldFixed(Field.AUTH_CURRENCY_CODE, Validator
                .getAns(), 3);
        IStreamFormatter auth_resp_code = new StreamFormatterFieldFixed(Field.AUTH_RESP_CODE, Validator.getAns(), 2);
        IStreamFormatter validation_code = new StreamFormatterFieldFixed(Field.VALIDATION_CODE, Validator.getAns(),
                4, ' ', true, true);
        IStreamFormatter excluded_tran_id = new StreamFormatterFieldFixed(Field.EXCLUDED_TRAN_ID, Validator.getAns(),
                1);
        IStreamFormatter crs_processing_code = new StreamFormatterFieldFixed(Field.CRS_PROCESSING_CODE, Validator
                .getAns(), 1, ' ', true, true);
        IStreamFormatter charge_back_rights = new StreamFormatterFieldFixed(Field.CHARGE_BACK_RIGHTS, Validator
                .getAns(), 2);
        IStreamFormatter multiple_clearing_seq = new StreamFormatterFieldFixed(Field.MULTIPLE_CLEARING_SEQ, Validator
                .getAns(), 2);
        IStreamFormatter multiple_clearing_cnt = new StreamFormatterFieldFixed(Field.MULTIPLE_CLEARING_CNT, Validator
                .getAns(), 2);
        IStreamFormatter market_auth_data = new StreamFormatterFieldFixed(Field.MARKET_AUTH_DATA, Validator.getAns(),
                1);
        IStreamFormatter total_auth_amount = new StreamFormatterFieldFixed(Field.TOTAL_AUTH_AMOUNT, Validator.getN(),
                12, '0', false, true);
        IStreamFormatter information_indicator = new StreamFormatterFieldFixed(Field.INFORMATION_INDICATOR, Validator
                .getAns(), 1, ' ', true, true);
        IStreamFormatter merchant_tel = new StreamFormatterFieldFixed(Field.MERCHANT_TEL, Validator.getAns(), 14, ' ', true, true);
        IStreamFormatter add_data_indicator = new StreamFormatterFieldFixed(Field.ADD_DATA_INDICATOR, Validator
                .getAns(), 1, ' ', true, true);
        IStreamFormatter reserved1 = new StreamFormatterFieldFixed(Field.RESERVED1, Validator.getAns(), 4, ' ', true,
                true);
        IStreamFormatter merchant_verification_value = new StreamFormatterFieldFixed(Field
                .MERCHANT_VERIFICATION_VALUE, Validator.getAns(), 10, ' ', true, true);
        IStreamFormatter reserved2 = new StreamFormatterFieldFixed(Field.RESERVED2, Validator.getAns(), 44, ' ',
                true, true);
        IStreamFormatter product_id = new StreamFormatterFieldFixed(Field.PRODUCT_ID, Validator.getAns(), 2);
        IStreamFormatter reserved3 = new StreamFormatterFieldFixed(Field.RESERVED3, Validator.getAns(), 6, ' ', true,
                true);
        IStreamFormatter dcc_indicator = new StreamFormatterFieldFixed(
                Field.DCC_INDICATOR, Validator.getAns(), 1, ' ', true, true);
        IStreamFormatter reserved4 = new StreamFormatterFieldFixed(Field.RESERVED4, Validator.getAns(), 23, ' ',
                true, true);
        IStreamFormatter cvv2_result_value = new StreamFormatterFieldFixed(Field.CVV2_RESULT_VALUE, Validator.getAnp
                (), 1, ' ', true, false);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(transaction_id);
        msg.add(authorized_amount);
        msg.add(auth_currency_code);
        msg.add(auth_resp_code);
        msg.add(validation_code);
        msg.add(excluded_tran_id);
        msg.add(crs_processing_code);
        msg.add(charge_back_rights);
        msg.add(multiple_clearing_seq);
        msg.add(multiple_clearing_cnt);
        msg.add(market_auth_data);
        msg.add(total_auth_amount);
        msg.add(information_indicator);
        msg.add(merchant_tel);
        msg.add(add_data_indicator);
        msg.add(reserved1);
        msg.add(merchant_verification_value);
        msg.add(reserved2);
        msg.add(product_id);
        msg.add(reserved3);
        msg.add(dcc_indicator);
        msg.add(reserved4);
        msg.add(cvv2_result_value);
        stream = msg;
    }
}

