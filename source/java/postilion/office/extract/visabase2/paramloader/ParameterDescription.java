package postilion.office.extract.visabase2.paramloader;


public class ParameterDescription
{

    public static final String FILE_NAME = "file name";
    public static final String VISA_TABLE_NAME = "visa table";
    public static final String WORKING_DIRECTORY = "working directory";
    public static final String ROLLBACK_CRITERIA = "rollback criteria";
    public static final String USER_SPECIFIED_ROLLBACK_VALUE = "user value";
    public static final String NO_ROLLBACK = "A";
    public static final String ROLLBACK_ON_ANY_ERRORS = "B";
    public static final String ROLLBACK_ON_LESS_THAN_PREVIOUS = "C";
    public static final String ROLLBACK_ON_LESS_THAN_USER_SPECIFIED = "D";

    public ParameterDescription()
    {
    }
}
