package postilion.office.extract.visabase2.paramloader;

import postilion.office.AProcessApp;
import postilion.office.OfficeProcess;
import postilion.office.XProcessError;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;


public class LoaderProcessApp extends AProcessApp
{
    public static final class Phases
    {

        public static final int DEFAULT = 0;
        public static final int INIT = 1;
        public static final int PARSE = 2;
        public static final int COMPLETE = 3;

        public Phases()
        {
        }
    }


    protected static final int NR_PARAMS_MANDATORY = 4;
    public static final String FILE_SEPERATOR = "\\";
    public static Vector SUPPORTED_VISA_TABLES;
    private String command_line_args[];
    private AParser parser;
    Properties params;
    private int current_phase;

    public LoaderProcessApp()
    {
        command_line_args = null;
        parser = null;
        params = null;
        current_phase = 0;
    }

    public Properties parseParam(String arg[])
            throws Exception
    {
        command_line_args = arg;
        return null;
    }

    public boolean rollback(boolean incomplete_only)
            throws Exception
    {
        if(current_phase == 0)
        {
            return true;
        }
        try
        {
            OfficeProcess.startNextPhase("Rollback", 20002, new String[0]);
            if(current_phase == 2)
            {
                parser.restoreTables();
                OfficeProcess.phaseDetail(20102, new String[0], 10);
            } else
            {
                OfficeProcess.phaseDetail(20104, new String[0], 10);
            }
        }
        catch(Exception e)
        {
            OfficeProcess.phaseDetail(e);
            OfficeProcess.phaseDetail(20108, new String[] {
                    params.getProperty("visa table")
            }, 30);
            return false;
        }
        return true;
    }

    public boolean runProcess()
            throws Exception
    {
        try
        {
            current_phase = 1;
            params = init();
            OfficeProcess.phaseDetail(20100, new String[] {
                    params.getProperty("file name"), params.getProperty("visa table"), params.getProperty("working directory"), params.getProperty("rollback criteria")
            }, 10);
            parser.backupTables();
            OfficeProcess.phaseDetail(20103, new String[] {
                    params.getProperty("visa table")
            }, 10);
            current_phase = 2;
            OfficeProcess.startNextPhase("Parsing", 20000, null);
            parser.parseFile();
            current_phase = 3;
            OfficeProcess.startNextPhase("Completion", 20001, null);
            OfficeProcess.phaseDetail(20101, new String[0], 10);
            return true;
        }
        catch(Exception e)
        {
            OfficeProcess.phaseDetail(e);
        }
        throw new XProcessError(20107, new String[0]);
    }

    private Properties init()
            throws XProcessError, FileNotFoundException, IOException
    {
        Properties params = new Properties();
        if(command_line_args.length != 4)
        {
            throw new XProcessError(20105, new String[] {
                    4 + " user parameters expected. " + command_line_args.length + " user parameter(s) supplied."
            });
        }
        File file = new File(command_line_args[0]);
        if(file.exists())
        {
            params.put("file name", command_line_args[0]);
        } else
        {
            throw new XProcessError(20105, new String[] {
                    "Unable to open input file '" + command_line_args[0] + "'"
            });
        }
        if(command_line_args[1] != null && SUPPORTED_VISA_TABLES.contains(command_line_args[1].toUpperCase()))
        {
            params.put("visa table", command_line_args[1].toUpperCase());
        } else
        {
            throw new XProcessError(20105, new String[] {
                    command_line_args[1] + " is not a supported Visa table."
            });
        }
        file = new File(command_line_args[2]);
        if(file.isDirectory())
        {
            String working_dir = command_line_args[2];
            if(!working_dir.endsWith("\\"))
            {
                working_dir = working_dir + "\\";
            }
            params.put("working directory", working_dir);
        } else
        {
            throw new XProcessError(20105, new String[] {
                    "Unable to locate working directory '" + command_line_args[2] + "'"
            });
        }
        if(command_line_args[3].equalsIgnoreCase("A") || command_line_args[3].equalsIgnoreCase("B") || command_line_args[3].equalsIgnoreCase("C"))
        {
            params.put("rollback criteria", command_line_args[3].toUpperCase());
        } else
        {
            try
            {
                int value = Integer.parseInt(command_line_args[3]);
                params.put("rollback criteria", "D");
                params.put("user value", command_line_args[3]);
            }
            catch(NumberFormatException nfe)
            {
                throw new XProcessError(20105, new String[] {
                        "Invalid rollback criteria parameter specified."
                });
            }
        }
        if(params.get("visa table").equals("ARDEF"))
        {
            parser = new ArdefTableParser(params);
        }
        return params;
    }

    static
    {
        SUPPORTED_VISA_TABLES = new Vector();
        SUPPORTED_VISA_TABLES.addElement(new String("ARDEF"));
    }
}

