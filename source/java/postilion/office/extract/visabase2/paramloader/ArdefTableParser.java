package postilion.office.extract.visabase2.paramloader;

import postilion.core.util.DateTime;
import postilion.core.util.XPostilion;
import postilion.office.OfficeLib;
import postilion.office.OfficeProcess;
import postilion.office.XProcessError;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

// Referenced classes of package postilion.office.extract.visabase2.paramloader:
//            AParser, ArdefTableRecord

public class ArdefTableParser extends AParser
{

    private String file_name;
    private String table_name;
    private String working_dir;
    private String rollback_condition;
    private int rollback_user_value;
    private BufferedReader current_file_reader;
    private PrintWriter error_log_file_writer;
    protected static final String PARSE_LOG_FILE_SUFFIX = "_parse_error.log";
    protected static final String OLD_PREFIX = "_old_";
    protected static final String VISA_DB_TABLE_PREFIX = "visabase2_";
    protected static final String VISA_DB_TABLE_SUFFIX = "_table";
    protected static final String ARDEF_TABLE_NAME = "visabase2_" + "ARDEF" + "_table";
    protected static final String BACKUP_BCP_FILE_PREFIX = "backup_";
    protected static final String BCP_FILE_SUFFIX = ".dat";
    protected static final String LOG_FILE_SUFFIX = ".log";
    protected static final String BACKUP_FILE_SUFFIX = ".bak";
    protected static final String BCP_BACKUP_CREATE_OUTPUT_PREFIX = "bcp_backup_create_out_";
    protected static final String BCP_BACKUP_CREATE_ERROR_PREFIX = "bcp_backup_create_err_";
    protected static final String BCP_BACKUP_LOAD_OUTPUT_PREFIX = "bcp_backup_load_out_";
    protected static final String BCP_BACKUP_LOAD_ERROR_PREFIX = "bcp_backup_load_err_";
    protected static final String ARDEF_INSERT_STATEMENT = "INSERT INTO " + ARDEF_TABLE_NAME + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    ArdefTableParser(Properties param)
    {
        file_name = null;
        table_name = null;
        working_dir = null;
        rollback_condition = null;
        current_file_reader = null;
        error_log_file_writer = null;
        file_name = param.getProperty("file name");
        table_name = param.getProperty("visa table");
        working_dir = param.getProperty("working directory");
        rollback_condition = param.getProperty("rollback criteria");
        if("D".equalsIgnoreCase(rollback_condition))
        {
            rollback_user_value = Integer.parseInt(param.getProperty("user value"));
        }
        File working_directory = new File(working_dir);
        String file_names_list[] = working_directory.list();
        if(file_names_list != null)
        {
            String timestamp = String.valueOf(DateTime.getSystemTime().getDay()) + String.valueOf(DateTime.getSystemTime().getMonth()) + String.valueOf(DateTime.getSystemTime().getYear()) + "_" + String.valueOf(DateTime.getSystemTime().getHour()) + String.valueOf(DateTime.getSystemTime().getMinute()) + String.valueOf(DateTime.getSystemTime().getSecond());
            for(int i = 0; i < file_names_list.length; i++)
            {
                String file = file_names_list[i];
                if(file.endsWith("_parse_error.log") || file.startsWith("backup_") && file.endsWith(".dat") || file.startsWith("bcp_backup_create_out_") || file.startsWith("bcp_backup_create_err_") || file.startsWith("bcp_backup_load_out_") || file.startsWith("bcp_backup_load_err_"))
                {
                    File rename_file = new File(working_dir + file);
                    rename_file.renameTo(new File(working_dir + "[" + timestamp + "]" + "_old_" + rename_file.getName()));
                }
            }

        }
    }

    protected void processTable()
            throws Exception
    {
        int line_nr = 1;
        int processed_record_count = 0;
        int skipped_record_count = 0;
        boolean has_print_header = false;
        int previous_record_count = clearTable(ARDEF_TABLE_NAME);
        Connection cn = OfficeLib.getOfficeConnection();
        try
        {
            for(String line_read = current_file_reader.readLine(); line_read != null;)
            {
                ArdefTableRecord record = new ArdefTableRecord();
                try
                {
                    record.fromMsg(line_read.getBytes());
                    PreparedStatement stmt = null;
                    try
                    {
                        stmt = cn.prepareCall(ARDEF_INSERT_STATEMENT);
                        stmt.setString(1, record.getField(ArdefTableRecord.Field.HIGH_KEY_FOR_RANGE));
                        stmt.setString(2, record.getField(ArdefTableRecord.Field.LOW_KEY_FOR_RANGE));
                        stmt.setString(3, record.getField(ArdefTableRecord.Field.ISSUER_BIN));
                        stmt.setString(4, record.getField(ArdefTableRecord.Field.CHECK_DIGIT_ALG));
                        stmt.setString(5, record.getField(ArdefTableRecord.Field.ACC_NR_LENGTH));
                        stmt.setString(6, record.getField(ArdefTableRecord.Field.CARD_TYPE));
                        stmt.setString(7, record.getField(ArdefTableRecord.Field.USAGE));
                        stmt.setString(8, record.getField(ArdefTableRecord.Field.PROCESSOR_BIN));
                        stmt.setString(9, record.getField(ArdefTableRecord.Field.DOMAIN));
                        stmt.setString(10, record.getField(ArdefTableRecord.Field.REGION));
                        stmt.setString(11, record.getField(ArdefTableRecord.Field.COUNTRY));
                        stmt.setString(12, record.getField(ArdefTableRecord.Field.ISSUER_COMMERCIAL));
                        stmt.setString(13, record.getField(ArdefTableRecord.Field.TECHNOLOGY_INDICATOR));
                        stmt.setString(14, record.getField(ArdefTableRecord.Field.ARDEF_REGION));
                        stmt.setString(15, record.getField(ArdefTableRecord.Field.ARDEF_COUNTRY));
                        stmt.execute();
                        processed_record_count++;
                    }
                    catch(SQLException ex)
                    {
                        throw new XProcessError(20106, new String[] {
                                ex.toString()
                        });
                    }
                    finally
                    {
                        OfficeLib.cleanupStatement(cn, stmt, null);
                    }
                }
                catch(XPostilion exception)
                {
                    skipped_record_count++;
                    if(!has_print_header)
                    {
                        has_print_header = true;
                        logRecordProcessError("The following record(s) failed:");
                        logRecordProcessError("");
                    }
                    String error_detail = "(Line " + line_nr + ") " + line_read;
                    logRecordProcessError(error_detail);
                    logRecordProcessError(exception.getMessage());
                }
                line_read = current_file_reader.readLine();
                line_nr++;
            }

        }
        catch(SQLException ex)
        {
            OfficeProcess.phaseDetail(20106, null, 30);
        }
        finally
        {
            OfficeLib.cleanupConnection(cn);
        }
        verifyLoadSuccess(skipped_record_count, processed_record_count, previous_record_count);
    }

    private void bcpDbTableToFile(String table_name, String file_name)
            throws IOException, InterruptedException
    {
        Process process = null;
        BufferedReader out_file_reader = null;
        String trace_line = null;
        String out_file_name = working_dir + "bcp_backup_create_out_" + table_name + ".log";
        String err_file_name = working_dir + "bcp_backup_create_err_" + table_name + ".log";
        boolean load_success = true;
        try
        {
            Runtime runtime = Runtime.getRuntime();
            String bcp_command = generateBcpCommandLine(table_name, false, working_dir + file_name, out_file_name, err_file_name);
            process = runtime.exec(bcp_command);
            process.waitFor();
            File err_file = new File(err_file_name);
            if(err_file.length() > 0L)
            {
                load_success = false;
                throw new XProcessError(20115, new String[] {
                        table_name, err_file_name
                });
            }
            out_file_reader = new BufferedReader(new FileReader(new File(out_file_name)));
            for(trace_line = out_file_reader.readLine(); trace_line != null; trace_line = out_file_reader.readLine())
            {
                trace_line = trace_line.toLowerCase();
                if(trace_line.indexOf("error") != -1)
                {
                    load_success = false;
                    throw new XProcessError(20115, new String[] {
                            table_name, out_file_name
                    });
                }
            }

        }
        finally
        {
            if(process != null)
            {
                process.destroy();
            }
            if(out_file_reader != null)
            {
                out_file_reader.close();
            }
            if(load_success)
            {
                (new File(out_file_name)).delete();
                (new File(err_file_name)).delete();
            }
        }
    }

    private void bcpFileToDbTable(String file_name, String table_name)
            throws Exception
    {
        Process process = null;
        BufferedReader out_file_reader = null;
        String trace_line = null;
        String out_file_name = working_dir + "bcp_backup_load_out_" + table_name + ".log";
        String err_file_name = working_dir + "bcp_backup_load_err_" + table_name + ".log";
        boolean load_success = true;
        try
        {
            clearTable(table_name);
            Runtime runtime = Runtime.getRuntime();
            String bcp_command = generateBcpCommandLine(table_name, true, working_dir + file_name, out_file_name, err_file_name);
            process = runtime.exec(bcp_command);
            process.waitFor();
            File err_file = new File(err_file_name);
            if(err_file.length() > 0L)
            {
                load_success = false;
                throw new XProcessError(20114, new String[] {
                        table_name, err_file_name
                });
            }
            out_file_reader = new BufferedReader(new FileReader(new File(out_file_name)));
            for(trace_line = out_file_reader.readLine(); trace_line != null; trace_line = out_file_reader.readLine())
            {
                trace_line = trace_line.toLowerCase();
                if(trace_line.indexOf("error") != -1)
                {
                    load_success = false;
                    throw new XProcessError(20114, new String[] {
                            table_name, out_file_name
                    });
                }
            }

        }
        finally
        {
            if(process != null)
            {
                process.destroy();
            }
            if(out_file_reader != null)
            {
                out_file_reader.close();
            }
            if(load_success)
            {
                (new File(out_file_name)).delete();
                (new File(err_file_name)).delete();
            }
        }
    }

    public void restoreTables()
            throws Exception
    {
        String visa_table_name = table_name.toLowerCase();
        String db_table_name = "visabase2_" + visa_table_name + "_table";
        String backup_file_name = "backup_" + visa_table_name + ".dat";
        bcpFileToDbTable(backup_file_name, db_table_name);
    }

    public void parseFile()
            throws Exception
    {
        current_file_reader = new BufferedReader(new FileReader(file_name));
        processTable();
        current_file_reader.close();
    }

    public void backupTables()
            throws IOException, InterruptedException
    {
        String visa_table_name = table_name.toLowerCase();
        String db_table_name = "visabase2_" + visa_table_name + "_table";
        String backup_file_name = "backup_" + visa_table_name + ".dat";
        bcpDbTableToFile(db_table_name, backup_file_name);
    }

    private void logRecordProcessError(String error_str)
            throws IOException
    {
        if(error_log_file_writer == null)
        {
            error_log_file_writer = new PrintWriter(new BufferedWriter(new FileWriter(working_dir + table_name + "_parse_error.log")));
            error_log_file_writer.println("VisaBase2TableLoader Error Log");
            error_log_file_writer.println("------------------------------------------");
        }
        error_log_file_writer.println(error_str);
        error_log_file_writer.flush();
    }

    protected void verifyLoadSuccess(int skipped_record_count, int processed_record_count, int previous_record_count)
            throws Exception
    {
        String error_log_file = working_dir + table_name + "_parse_error.log";
        if("B".equalsIgnoreCase(rollback_condition) && skipped_record_count != 0)
        {
            logRecordProcessError("------------------------------------------");
            logRecordProcessError("Rollback criteria: " + rollback_condition);
            logRecordProcessError("Load failed since " + String.valueOf(skipped_record_count) + " error(s) occured!");
            logRecordProcessError("Rolling back...");
            logRecordProcessError("------------------------------------------");
            throw new XProcessError(20111, new String[] {
                    table_name, String.valueOf(skipped_record_count), error_log_file
            });
        }
        if("C".equalsIgnoreCase(rollback_condition) && processed_record_count < previous_record_count)
        {
            logRecordProcessError("------------------------------------------");
            logRecordProcessError("Rollback criteria: " + rollback_condition);
            logRecordProcessError("Number of records in database prior to load: " + String.valueOf(previous_record_count));
            logRecordProcessError("Number of records loaded: " + String.valueOf(processed_record_count));
            logRecordProcessError("Load failed!");
            logRecordProcessError("Rolling back...");
            logRecordProcessError("------------------------------------------");
            throw new XProcessError(20112, new String[] {
                    table_name, error_log_file
            });
        }
        if("D".equalsIgnoreCase(rollback_condition) && processed_record_count < rollback_user_value)
        {
            logRecordProcessError("------------------------------------------");
            logRecordProcessError("Rollback user value: " + String.valueOf(rollback_user_value));
            logRecordProcessError("Number of records loaded: " + String.valueOf(processed_record_count));
            logRecordProcessError("Load failed!");
            logRecordProcessError("Rolling back...");
            logRecordProcessError("------------------------------------------");
            throw new XProcessError(20113, new String[] {
                    table_name, String.valueOf(rollback_user_value), String.valueOf(processed_record_count), error_log_file
            });
        }
        if(skipped_record_count != 0)
        {
            logRecordProcessError("------------------------------------------");
            logRecordProcessError("Number of records loaded: " + String.valueOf(processed_record_count));
            logRecordProcessError("Number of records failed: " + String.valueOf(skipped_record_count));
            logRecordProcessError("Rollback criteria not met");
            logRecordProcessError("Not rolling back...");
            logRecordProcessError("------------------------------------------");
            OfficeProcess.phaseDetail(20110, new String[] {
                    table_name, String.valueOf(processed_record_count), String.valueOf(skipped_record_count), error_log_file
            }, 20);
        } else
        {
            OfficeProcess.phaseDetail(20109, new String[] {
                    table_name, String.valueOf(processed_record_count)
            }, 10);
        }
    }

}

