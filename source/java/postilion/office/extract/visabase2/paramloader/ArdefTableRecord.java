package postilion.office.extract.visabase2.paramloader;

import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;

public final class ArdefTableRecord extends StreamMessage
{
    public static final class Field
    {

        public static final String HIGH_KEY_FOR_RANGE = new String("High Key for Range");
        public static final String RESERVED = new String("Reserved");
        public static final String LOW_KEY_FOR_RANGE = new String("Low Key for Range");
        public static final String RESERVED_2 = new String("Reserved 2");
        public static final String ISSUER_BIN = new String("Issuer BIN");
        public static final String CHECK_DIGIT_ALG = new String("Check Digit Algorithm");
        public static final String ACC_NR_LENGTH = new String("Account Number Length");
        public static final String CARD_TYPE = new String("Card Type");
        public static final String USAGE = new String("Usage");
        public static final String PROCESSOR_BIN = new String("Processor BIN");
        public static final String DOMAIN = new String("Domain");
        public static final String REGION = new String("Region");
        public static final String COUNTRY = new String("Country");
        public static final String ISSUER_COMMERCIAL = new String("Issuer Commercial");
        public static final String TECHNOLOGY_INDICATOR = new String("Technology Indicator");
        public static final String ARDEF_REGION = new String("ARDEF Region");
        public static final String ARDEF_COUNTRY = new String("ARDEF Country");

        public Field()
        {
        }
    }


    private static final int MAX_SIZE = 50;
    private static IStreamFormatter stream;

    public ArdefTableRecord()
    {
        super(50, stream);
    }

    static
    {
        StreamFormatterContainer record = new StreamFormatterContainer();
        record.add(new StreamFormatterFieldFixed(Field.HIGH_KEY_FOR_RANGE, Validator.getN(), 9));
        record.add(new StreamFormatterFieldFixed(Field.RESERVED, Validator.getNone(), 3));
        record.add(new StreamFormatterFieldFixed(Field.LOW_KEY_FOR_RANGE, Validator.getN(), 9));
        record.add(new StreamFormatterFieldFixed(Field.RESERVED_2, Validator.getNone(), 3));
        record.add(new StreamFormatterFieldFixed(Field.ISSUER_BIN, Validator.getN(), 6));
        record.add(new StreamFormatterFieldFixed(Field.CHECK_DIGIT_ALG, Validator.getN(), 1));
        record.add(new StreamFormatterFieldFixed(Field.ACC_NR_LENGTH, Validator.getN(), 2));
        record.add(new StreamFormatterFieldFixed(Field.CARD_TYPE, Validator.getAns(), 1));
        record.add(new StreamFormatterFieldFixed(Field.USAGE, Validator.getAnp(), 1));
        record.add(new StreamFormatterFieldFixed(Field.PROCESSOR_BIN, Validator.getN(), 6));
        record.add(new StreamFormatterFieldFixed(Field.DOMAIN, Validator.getAn(), 1));
        record.add(new StreamFormatterFieldFixed(Field.REGION, Validator.getN(), 1));
        record.add(new StreamFormatterFieldFixed(Field.COUNTRY, Validator.getAn(), 2));
        record.add(new StreamFormatterFieldFixed(Field.ISSUER_COMMERCIAL, Validator.getAnp(), 1));
        record.add(new StreamFormatterFieldFixed(Field.TECHNOLOGY_INDICATOR, Validator.getAnp(), 1));
        record.add(new StreamFormatterFieldFixed(Field.ARDEF_REGION, Validator.getN(), 1));
        record.add(new StreamFormatterFieldFixed(Field.ARDEF_COUNTRY, Validator.getAn(), 2));
        stream = record;
    }
}

