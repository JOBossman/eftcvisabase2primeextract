package postilion.office.extract.visabase2.paramloader;

public class ProcessLogIdVisaBase2TableLoader
{

    public static final int PHASE_PARSING = 20000;
    public static final int PHASE_COMPLETION = 20001;
    public static final int PHASE_ROLLBACK = 20002;
    public static final int INIT_PARAM_SUCCESS = 20100;
    public static final int COMPLETION_SUCCESS = 20101;
    public static final int ROLLBACK_SUCCESS = 20102;
    public static final int BACKUP_SUCCESS = 20103;
    public static final int ROLLBACK_NOT_NECESSARY = 20104;
    public static final int INVALID_USER_PARAMETERS = 20105;
    public static final int DATABASE_ERROR = 20106;
    public static final int PROCESS_SOFTWARE_FAILURE = 20107;
    public static final int ROLLBACK_FAILURE = 20108;
    public static final int PARSING_COMPLETED_NO_ERRORS = 20109;
    public static final int PARSING_COMPLETED_WITH_ERRORS = 20110;
    public static final int DO_ROLLBACK_ANY_ERRORS_OCCURED = 20111;
    public static final int DO_ROLLBACK_LESS_SUCCESS_THAN_PREVIOUS = 20112;
    public static final int DO_ROLLBACK_LESS_SUCCESS_THAN_USER_SPECIFIED = 20113;
    public static final int TABLE_BCP_LOAD_FAILED = 20114;
    public static final int TABLE_BCP_BACKUP_FAILED = 20115;

    public ProcessLogIdVisaBase2TableLoader()
    {
    }
}

