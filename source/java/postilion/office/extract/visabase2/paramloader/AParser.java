package postilion.office.extract.visabase2.paramloader;

import postilion.office.OfficeLib;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public abstract class AParser
{

    public AParser()
    {
    }

    public abstract void restoreTables()
            throws Exception;

    protected String generateBcpCommandLine(String table_name, boolean in, String data_file_name, String out_file_name, String err_file_name)
    {
        return "bcp postilion_office.dbo." + table_name + (in ? " in " : " out ") + data_file_name + " /c /t; /r\\n /T" + " /o " + out_file_name + " /e " + err_file_name;
    }

    public abstract void parseFile()
            throws Exception;

    public abstract void backupTables()
            throws Exception;

    protected int clearTable(String table_name)
            throws Exception
    {
        int record_count = 0;
        Connection cn = OfficeLib.getOfficeConnection();
        try
        {
            PreparedStatement stmt = null;
            ResultSet rs = null;
            stmt = cn.prepareCall("SELECT COUNT(*) FROM " + table_name);
            for(rs = stmt.executeQuery(); rs.next();)
            {
                record_count = rs.getInt(1);
            }

            OfficeLib.cleanupStatement(cn, stmt, rs);
            stmt = cn.prepareCall("truncate table " + table_name);
            stmt.execute();
        }
        finally
        {
            OfficeLib.cleanupConnection(cn);
        }
        return record_count;
    }
}

