package postilion.office.extract.visabase2;


import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;
import postilion.core.util.Convert;

public class Tcr0ForFees extends StreamMessage
{
    public static class Field
    {
        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String DESTINATION_BIN = new String("destination bin");
        public static final String SOURCE_BIN = new String("source bin");
        public static final String REASON_CODE = new String("reason code");
        public static final String COUNTRY_CODE = new String("country code");
        public static final String EVENT_DATE = new String("event date");
        public static final String ACCOUNT_NUMBER = new String("account number");
        public static final String DESTINATION_AMOUNT = new String("destination amount");
        public static final String DESTINATION_CURRENCY_CODE = new String("destination currency code");
        public static final String SOURCE_AMOUNT = new String("source amount");
        public static final String SOURCE_CURRENCY_CODE = new String("source currency code");
        public static final String TEXT = new String("text");
        public static final String SETTLEMENT_FLAG = new String("settlement flag");
        public static final String TRANSACTION_IDENTIFIER = new String("transaction identifier");
        public static final String RESERVED = new String("reserved");
        public static final String CENTRAL_PROCESSING_DATE = new String("central processing date");
        public static final String REIMBURSEMENT_ATTR = new String("reimbursement attribute");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr0ForFees()
    {
        super(168, stream);
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "0");
        putField(Field.REIMBURSEMENT_ATTR, "0");
        putField(Field.DESTINATION_BIN, Convert.resize("", 6, ' ', true));
        putField(Field.DESTINATION_AMOUNT, Convert.resize("", 12, '0', true));
        putField(Field.RESERVED, " ");
        putField(Field.CENTRAL_PROCESSING_DATE, Convert.resize("", 4, '0', true));
        putField(Field.DESTINATION_CURRENCY_CODE, Convert.resize("", 3, ' ', true));
    }

    public static void main(String args[])
    {
        Tcr0ForFees tcr0 = new Tcr0ForFees();
        System.out.println(tcr0.describeStructure());
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN(), 1);
        IStreamFormatter destination_bin = new StreamFormatterFieldFixed(Field.DESTINATION_BIN, Validator.getNs(), 6);
        IStreamFormatter source_bin = new StreamFormatterFieldFixed(Field.SOURCE_BIN, Validator.getNs(), 6);
        IStreamFormatter reason_code = new StreamFormatterFieldFixed(Field.REASON_CODE, Validator.getN(), 4);
        IStreamFormatter country_code = new StreamFormatterFieldFixed(Field.COUNTRY_CODE, Validator.getAns(), 3);
        IStreamFormatter event_date = new StreamFormatterFieldFixed(Field.EVENT_DATE, Validator.getN(), 4);
        IStreamFormatter account_number = new StreamFormatterFieldFixed(Field.ACCOUNT_NUMBER, Validator.getN(), 19);
        IStreamFormatter destination_amount = new StreamFormatterFieldFixed(Field.DESTINATION_AMOUNT, Validator.getN(), 12);
        IStreamFormatter destination_currency_code = new StreamFormatterFieldFixed(Field.DESTINATION_CURRENCY_CODE, Validator.getAns(), 3);
        IStreamFormatter source_amount = new StreamFormatterFieldFixed(Field.SOURCE_AMOUNT, Validator.getN(), 12, '0', false, false);
        IStreamFormatter source_currency_code = new StreamFormatterFieldFixed(Field.SOURCE_CURRENCY_CODE, Validator.getAns(), 3);
        IStreamFormatter text = new StreamFormatterFieldFixed(Field.TEXT, Validator.getAns(), 70, ' ', true, false);
        IStreamFormatter settlement_flag = new StreamFormatterFieldFixed(Field.SETTLEMENT_FLAG, Validator.getN(), 1);
        IStreamFormatter transaction_identifier = new StreamFormatterFieldFixed(Field.TRANSACTION_IDENTIFIER, Validator.getN(), 15);
        IStreamFormatter reserved = new StreamFormatterFieldFixed(Field.RESERVED, Validator.getAns(), 1);
        IStreamFormatter central_processing_date = new StreamFormatterFieldFixed(Field.CENTRAL_PROCESSING_DATE, Validator.getN(), 4);
        IStreamFormatter reimbursement_attr = new StreamFormatterFieldFixed(Field.REIMBURSEMENT_ATTR, Validator.getAns(), 1);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(destination_bin);
        msg.add(source_bin);
        msg.add(reason_code);
        msg.add(country_code);
        msg.add(event_date);
        msg.add(account_number);
        msg.add(destination_amount);
        msg.add(destination_currency_code);
        msg.add(source_amount);
        msg.add(source_currency_code);
        msg.add(text);
        msg.add(settlement_flag);
        msg.add(transaction_identifier);
        msg.add(reserved);
        msg.add(central_processing_date);
        msg.add(reimbursement_attr);
        stream = msg;
    }
}
