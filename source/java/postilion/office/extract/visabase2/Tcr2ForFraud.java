package postilion.office.extract.visabase2;


import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;
import postilion.core.util.Convert;

public class Tcr2ForFraud extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String TRANSACTION_IDENTIFIER = new String("transaction ident");
        public static final String EXCLUDED_TRAN_ID_REASON = new String("excluded tran id reason");
        public static final String MULTIPLE_CLEARING_SEQ_NR = new String("multiple clearing seq nr");
        public static final String CARD_ACCEPTOR_ID = new String("card_acceptor_id");
        public static final String TERMINAL_ID = new String("terminal id");
        public static final String TRAVEL_AGENCY_ID = new String("travel agency id");
        public static final String CASHBACK_INDICATOR = new String("cashback indicator");
        public static final String AUTHORIZATION_CODE = new String("authorization code");
        public static final String CARDHOLDER_ID_METHOD_USED = new String("cardholder id method used");
        public static final String POS_ENTRY_MODE = new String("pos entry mode");
        public static final String POS_TERM_CAPABILITY = new String("pos term capability");
        public static final String CARD_CAPABILITY = new String("card capability");
        public static final String RESERVED1 = new String("reserved 1");
        public static final String CASHBACK_AMOUNT = new String("cashback amount");
        public static final String CARDHOLDER_ACTIVATED_TERM_IND = new String("cardholder activated term ind");
        public static final String COMMERCE_INDICATOR = new String("commerce indicator");
        public static final String RESERVED2 = new String("reserved 2");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr2ForFraud()
    {
        super(168, stream);
        putField(Field.TRANSACTION_CODE, "40");
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "2");
        putField(Field.MULTIPLE_CLEARING_SEQ_NR, "01");
        putField(Field.TRAVEL_AGENCY_ID, Convert.resize("", 8, ' ', true));
        putField(Field.CARD_CAPABILITY, " ");
        putField(Field.RESERVED1, Convert.resize("", 6, ' ', true));
        putField(Field.COMMERCE_INDICATOR, "N");
        putField(Field.RESERVED2, Convert.resize("", 86, ' ', true));
    }

    public static void main(String args[])
    {
        Tcr2ForFraud tcr2forfraud = new Tcr2ForFraud();
        System.out.println(tcr2forfraud.describeStructure());
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN(), 1);
        IStreamFormatter tran_id = new StreamFormatterFieldFixed(Field.TRANSACTION_IDENTIFIER, Validator.getNs(), 15);
        IStreamFormatter excluded_tran_id_reason = new StreamFormatterFieldFixed(Field.EXCLUDED_TRAN_ID_REASON, Validator.getAns(), 1);
        IStreamFormatter multi_clear_seq_nr = new StreamFormatterFieldFixed(Field.MULTIPLE_CLEARING_SEQ_NR, Validator.getN(), 2);
        IStreamFormatter card_acpt_id = new StreamFormatterFieldFixed(Field.CARD_ACCEPTOR_ID, Validator.getAns(), 15);
        IStreamFormatter term_id = new StreamFormatterFieldFixed(Field.TERMINAL_ID, Validator.getAns(), 8);
        IStreamFormatter trav_agen_id = new StreamFormatterFieldFixed(Field.TRAVEL_AGENCY_ID, Validator.getAns(), 8);
        IStreamFormatter cashback_ind = new StreamFormatterFieldFixed(Field.CASHBACK_INDICATOR, Validator.getAns(), 1);
        IStreamFormatter auth_code = new StreamFormatterFieldFixed(Field.AUTHORIZATION_CODE, Validator.getAns(), 6);
        IStreamFormatter cardholder_id_method = new StreamFormatterFieldFixed(Field.CARDHOLDER_ID_METHOD_USED, Validator.getAns(), 1);
        IStreamFormatter pos_entry_mode = new StreamFormatterFieldFixed(Field.POS_ENTRY_MODE, Validator.getAns(), 2);
        IStreamFormatter pos_term_capability = new StreamFormatterFieldFixed(Field.POS_TERM_CAPABILITY, Validator.getAns(), 1);
        IStreamFormatter card_capability = new StreamFormatterFieldFixed(Field.CARD_CAPABILITY, Validator.getAns(), 1);
        IStreamFormatter reserved1 = new StreamFormatterFieldFixed(Field.RESERVED1, Validator.getAns(), 6);
        IStreamFormatter cashback_amount = new StreamFormatterFieldFixed(Field.CASHBACK_AMOUNT, Validator.getN(), 9);
        IStreamFormatter cardholder_activated_term_ind = new StreamFormatterFieldFixed(Field.CARDHOLDER_ACTIVATED_TERM_IND, Validator.getAns(), 1);
        IStreamFormatter commerce_indicacator = new StreamFormatterFieldFixed(Field.COMMERCE_INDICATOR, Validator.getAns(), 1);
        IStreamFormatter reserved2 = new StreamFormatterFieldFixed(Field.RESERVED2, Validator.getAns(), 86);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(tran_id);
        msg.add(excluded_tran_id_reason);
        msg.add(multi_clear_seq_nr);
        msg.add(card_acpt_id);
        msg.add(term_id);
        msg.add(trav_agen_id);
        msg.add(cashback_ind);
        msg.add(auth_code);
        msg.add(cardholder_id_method);
        msg.add(pos_entry_mode);
        msg.add(pos_term_capability);
        msg.add(card_capability);
        msg.add(reserved1);
        msg.add(cashback_amount);
        msg.add(cardholder_activated_term_ind);
        msg.add(commerce_indicacator);
        msg.add(reserved2);
        stream = msg;
    }
}
