package postilion.office.extract.visabase2;


import java.util.Hashtable;

public class CompliancyChecker
{
    public static class CompliancyLevel
    {

        public static final int _201104 = 1;

        public CompliancyLevel()
        {
        }
    }


    private static final Hashtable compliancyLevels;

    public CompliancyChecker()
    {
    }

    public static int getCompliancyLevel(String tag)
            throws IllegalArgumentException
    {
        Object val = compliancyLevels.get(tag.trim());
        if(val == null)
        {
            return -1;
        } else
        {
            return ((Integer)val).intValue();
        }
    }

    static
    {
        compliancyLevels = new Hashtable();
        compliancyLevels.put("201104", new Integer(1));
    }
}

