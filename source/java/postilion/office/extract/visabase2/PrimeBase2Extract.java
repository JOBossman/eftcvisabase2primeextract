package postilion.office.extract.visabase2;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

import postilion.core.message.Validator;
import postilion.core.message.bitmap.CardAcceptorNameLocation;
import postilion.core.message.bitmap.ProcessingCode;
import postilion.core.message.bitmap.ServiceRestrictionCode;
import postilion.core.message.bitmap.StructuredData;
import postilion.core.message.stream.XStreamBase;
import postilion.core.util.Convert;
import postilion.core.util.DateTime;
import postilion.core.util.JdbcUtils;
import postilion.core.util.XPostilion;
import postilion.core.xml.iccdata.IccData;
import postilion.core.xml.iccdata.IccRequest;
import postilion.core.xml.iccdata.IssuerActionCode;
import postilion.office.OfficeLib;
import postilion.office.extract.AExtractEnhanced;
import postilion.office.extract.Extract;
import postilion.office.extract.ExtractResult;
import postilion.office.extract.IExtractInfo;
import postilion.office.extract.visabase2.xml.AdjustmentComponent.AdjustmentComponent;
import postilion.office.extract.visabase2.xml.BaseI.BaseI;
import postilion.office.extract.visabase2.xml.BaseI.ObjectFactory;
import postilion.office.extract.visabase2.xml.BaseI.ProcessingCodeData;
import postilion.office.extract.visabase2.xml.BaseII.BaseII;


public class PrimeBase2Extract extends AExtractEnhanced
{
    private class SettleEntity
    {

        String sink_node_name;
        int settle_entity_id;
        String acquiring_inst_id_code;

        public SettleEntity(String sink_node_name, int settle_entity_id, String acquiring_inst_id_code)
        {
            this.settle_entity_id = 0;
            this.sink_node_name = sink_node_name;
            this.settle_entity_id = settle_entity_id;
            this.acquiring_inst_id_code = acquiring_inst_id_code;
        }
    }

    protected class ExtractedTransaction
    {

        public String structured_data_0100;
        public long cashback_0100;
        public long post_tran_id;
        public long prev_post_tran_id;
        public String acquiring_inst_id_code;
        public String auth_id_rsp;
        public int batch_nr;
        public DateTime datetime_tran_local;
        public String from_account_type;
        public String icc_data_req;
        public String message_type;
        public String pos_condition_code;
        public String pos_entry_mode;
        public RetentionData retention_data;
        public String rsp_code_req;
        public long settle_amount_impact;
        public int settle_entity_id;
        public String sponsor_bank;
        public String structured_data_req;
        public String structured_data_rsp;
        public long tran_amount_rsp;
        public long tran_cash_rsp;
        public long tran_tran_fee_rsp;
        public String tran_currency_code;
        public long tran_nr;
        public String tran_reversed;
        public String tran_type;
        public String card_acceptor_id_code;
        public String card_acceptor_name_loc;
        public String card_seq_nr;
        public String merchant_type;
        public String pan;
        public String expiry_date;
        public String pos_operating_environment;
        public String pos_card_data_input_ability;
        public String pos_cardholder_auth_method;
        public String pos_cardholder_present;
        public String pos_terminal_type;
        public long post_tran_cust_id;
        public String terminal_id;
        public String ext_tran_type;
        public String message_reason_code;
        public long new_tran_amount_rsp;
        public long new_tran_cash_rsp;
        public DateTime prev_datetime_tran_local;
        public DateTime batch_datetime_end;
        public boolean reversal;
        public boolean representment;
        public boolean is_open;
        private String retention_data_string;
        public String card_verification_result;
        public String service_restriction_code;
        private String prev_retention_data_string;
        public String prev_structured_data_req;
        public String prev_structured_data_rsp;
        public String node;
        public long prev_prev_post_tran_id;
        public String prev_pos_entry_mode;

        public ExtractedTransaction(ResultSet rs, long post_tran_id, int first_plugin_column_index)
                throws SQLException
        {
            retention_data = new RetentionData();
            reversal = false;
            representment = false;
            is_open = false;
            int i = first_plugin_column_index;
            this.post_tran_id = post_tran_id;
            acquiring_inst_id_code = rs.getString(i++);
            if(set_acquiring_institution_id != null)
            {
                acquiring_inst_id_code = set_acquiring_institution_id;
            }
            auth_id_rsp = rs.getString(i++);
            batch_nr = rs.getInt(i++);
            datetime_tran_local = new DateTime(rs.getTimestamp(i++));
            from_account_type = rs.getString(i++);
            icc_data_req = rs.getString(i++);
            message_type = rs.getString(i++);
            pos_condition_code = rs.getString(i++);
            pos_entry_mode = rs.getString(i++);
            retention_data_string = rs.getString(i++);
            rsp_code_req = rs.getString(i++);
            settle_amount_impact = rs.getLong(i++);
            settle_entity_id = rs.getInt(i++);
            sponsor_bank = rs.getString(i++);
            structured_data_req = rs.getString(i++);
            structured_data_rsp = rs.getString(i++);
            tran_amount_rsp = rs.getLong(i++);
            tran_cash_rsp = rs.getLong(i++);
            tran_tran_fee_rsp = rs.getLong(i++);
            tran_currency_code = rs.getString(i++);
            tran_nr = rs.getLong(i++);
            tran_reversed = rs.getString(i++);
            tran_type = rs.getString(i++);
            card_acceptor_id_code = rs.getString(i++);
            card_acceptor_name_loc = rs.getString(i++);
            card_seq_nr = rs.getString(i++);
            merchant_type = rs.getString(i++);
            pan = rs.getString(i++);
            expiry_date = rs.getString(i++);
            pos_operating_environment = rs.getString(i++);
            pos_card_data_input_ability = rs.getString(i++);
            pos_cardholder_auth_method = rs.getString(i++);
            pos_cardholder_present = rs.getString(i++);
            pos_terminal_type = rs.getString(i++);
            post_tran_cust_id = rs.getLong(i++);
            terminal_id = rs.getString(i++);
            new_tran_amount_rsp = 0L;
            new_tran_cash_rsp = 0L;
            prev_datetime_tran_local = null;
            java.sql.Timestamp ts = rs.getTimestamp(i++);
            ext_tran_type = rs.getString(i++);
            message_reason_code = rs.getString(i++);
            card_verification_result = rs.getString(i++);
            prev_post_tran_id = rs.getLong(i++);
            service_restriction_code = rs.getString(i++);
            prev_retention_data_string = rs.getString(i++);
            prev_structured_data_req = rs.getString(i++);
            prev_structured_data_rsp = rs.getString(i++);
            node = rs.getString(i++);
            prev_prev_post_tran_id = rs.getLong(i++);
            prev_pos_entry_mode = rs.getString(i++);
            if(ts == null)
            {
                batch_datetime_end = null;
            } else
            {
                batch_datetime_end = new DateTime(ts);
            }
            if(retention_data_string != null && retention_data_string != "")
            {
                try
                {
                    retention_data.fromMsg(retention_data_string);
                }
                catch(XStreamBase x)
                {
                    VisaBase2ExtractLog.reportWarning("Unable to unpack retention data in the message with post_tran_id %1 ", new String[] {
                            Long.toString(post_tran_id)
                    });
                }
            } else
            if(prev_retention_data_string != null && prev_retention_data_string != "")
            {
                try
                {
                    retention_data.fromMsg(prev_retention_data_string);
                }
                catch(XStreamBase x)
                {
                    VisaBase2ExtractLog.reportWarning("Unable to unpack retention data in the message with post_tran_id %1 ", new String[] {
                            Long.toString(prev_post_tran_id)
                    });
                }
            }
            if(settle_amount_impact < 0L)
            {
                settle_amount_impact = -settle_amount_impact;
            }
            if(tran_tran_fee_rsp < 0L)
            {
                tran_tran_fee_rsp = -tran_tran_fee_rsp;
            }
            if(PrimeBase2Extract.debug)
            {
                PrimeBase2Extract.debugPrintln("\nExtracted transaction for post_tran_id " + post_tran_id + ":" + "\nacquiring_inst_id_code = " + acquiring_inst_id_code + "\nauth_id_rsp = " + auth_id_rsp + "\nbatch_nr = " + batch_nr + "\ndatetime_tran_local = " + datetime_tran_local + "\nmessage_type = " + message_type + "\npos_condition_code = " + pos_condition_code + "\npos_entry_mode = " + pos_entry_mode + "\nretention_data = " + retention_data.toString() + "\nsettle_amount_impact = " + settle_amount_impact + "\nsettle_entity_id = " + settle_entity_id + "\nsponsor_bank = " + sponsor_bank + "\nstructured_data_req = " + structured_data_req + "\nstructured_data_rsp = " + structured_data_rsp + "\ntran_amount_rsp = " + tran_amount_rsp + "\ntran_cash_rsp = " + tran_cash_rsp + "\ntran_tran_fee_rsp = " + tran_tran_fee_rsp + "\ntran_currency_code = " + tran_currency_code + "\ntran_nr = " + tran_nr + "\ntran_reversed = " + tran_reversed + "\ntran_type = " + tran_type + "\ncard_acceptor_id_code =" + card_acceptor_id_code + "\ncard_acceptor_name_loc = " + card_acceptor_name_loc + "\ncard_seq_nr = " + card_seq_nr + "\nmerchant_type = " + merchant_type + "\nexpiry_date = " + expiry_date + "\npos_operating_environment = " + pos_operating_environment + "\npos_card_data_input_ability = " + pos_card_data_input_ability + "\npos_cardholder_present = " + pos_cardholder_present + "\npos_terminal_type = " + pos_terminal_type + "\nterminal_id = " + terminal_id + "\nbatch_datetime_end = " + batch_datetime_end + "\nprev_post_tran_id = " + prev_post_tran_id + "\ncard_verification_result = " + card_verification_result + "\nservice_restriction_code = " + service_restriction_code + "\nprev_retention_data_string = " + prev_retention_data_string + "\nprev_structured_data_req = " + prev_structured_data_req + "\nprev_structured_data_rsp = " + prev_structured_data_rsp + "\nnode = " + node + "\nprev_prev_post_tran_id = " + prev_prev_post_tran_id + "\nprev_pos_entry_mode = " + prev_pos_entry_mode);
            }
        }
    }

    private static final class StructDataKeys
    {

        public static final String BASEI = "BaseI";
        public static final String BASEII = "BaseII";
        public static final String ADJUSTMENT_COMPONENT = "AdjustmentComponent";
        public static final String _004_TRANSACTION_AMOUNT = "004_TRANSACTION_AMOUNT";
        public static final String _049_CURRENCY_CODE_TRANS = "049_CURRENCY_CODE_TRANS";
        public static final String _051_CURRENCY_CODE_BILL = "051_CURRENCY_CODE_BILL";

        StructDataKeys()
        {
        }
    }

    private static final class AuthRspCode
    {

        public static final String _Y1_OFFLINE_APPROVAL = "Y1";
        public static final String _Z1_OFFLINE_DECLINE = "Z1";
        public static final String _Y3_UNABLE_TO_GO_ONLINE_APPROVAL = "Y3";
        public static final String _Z3_UNABLE_TO_GO_ONLINE_DECLINE = "Z3";

        AuthRspCode()
        {
        }
    }

    private static final class CountryCodes
    {

        public static final String _US_AMERICA = "US";

        CountryCodes()
        {
        }
    }

    public static final class AuthCharIndicator
    {

        public static final String _N_DOES_NOT_QUALIFY = "N";
        public static final String _T_NOT_ELIGIBLE_FOR_CPS_PARTICIPATION = "T";

        public AuthCharIndicator()
        {
        }
    }

    protected static class ExtendedTranType
    {

        protected static final String _4100_RETRIEVAL_REQUEST = "4100";
        protected static final String _4101_CHARGEBACK = "4101";
        protected static final String _4102_CHARGEBACK_REVERSAL = "4102";
        protected static final String _4103_REPRESENTMENT = "4103";
        protected static final String _4104_ARBITRATION_CHARGEBACK = "4104";
        protected static final String _4105_FEE_COLLECTION = "4105";
        protected static final String _4106_FUNDS_DISBURSEMENT = "4106";
        protected static final String _4107_TEXT_MSG = "4107";
        protected static final String _4108_RETRIEVAL_CONFIRMATION = "4108";
        protected static final String _4002_FRAUD_ADVICE = "4002";
        protected static final String _4111_REPRESENTMENT_REVERSAL = "4111";
        protected static final String _6500_CARD_ACTIVATION_WITH_DEPOSIT_VERIFIED_BY_OPERATOR = "6500";

        public ExtendedTranType()
        {
        }
    }

    protected static class TransactionCode
    {

        protected static final String _05_SALES_DRAFT = "05";
        protected static final String _06_CREDIT_VOUCHER = "06";
        protected static final String _07_CASH_DISBURSEMENT = "07";
        protected static final String _25_SALES_DRAFT_REV = "25";
        protected static final String _26_CREDIT_VOUCHER_REV = "26";

        public TransactionCode()
        {
        }
    }

    private static class ErrorMessages
    {

        public static final String ERROR_CHECKING_TRANSACTION_MSG = "Failed checking the transaction message with post_tran_id %1 ";
        public static final String ERROR_CHECKING_REVERSAL_MSG = "Failed checking the reversal message with post_tran_id %1 ";
        public static final String ERROR_CLOSING_OUTPUT = "Process unable to close output: %1";
        public static final String ERROR_CREATING_OUTPUT = "Process unable to create output: %1";
        public static final String ERROR_DB_CLEAN = "Failed cleaning up the database statement";
        public static final String ERROR_EXTRACT_TRAN = "Failed creating an ExtractedTransaction instance";
        public static final String ERROR_GETTING_ACQ_INST_NAME = "Failed retrieving the acquirer institution name for %1from the database";
        public static final String ERROR_GETTING_SETTLE_ENTITIES = "Failed retrieving the settlement entities for sink node %1 from the database";
        public static final String ERROR_GETTING_0100_DATA = "Failed retrieving the structured data of the original 0100 message pertaining to" +
                " fraud for %1 from the database"
                ;
        public static final String ERROR_INSERT_TRAN_EXCEPTION = "Failed inserting a transaction exception record for the transaction with post_tr" +
                "an_id %1"
                ;
        public static final String ERROR_PACKING_TRAN_EXCEPTION = "Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                "ntrecord associated with this is %3."
                ;
        public static final String ERROR_PROCESSING_TRANSACTION = "An error occured while writing a transaction to file";
        public static final String GENERATE_CONTROL_RECORD = "Failed generating a control record for transaction code %1";
        public static final String GENERATE_TCR_RECORD = "Failed generating a transaction code record %1";
        public static final String INVALID_NODE_NAME_METHOD = "Invalid Node Name Method. The extract entity must be configured with the Free Li" +
                "st Of Node Names method."
                ;
        public static final String INVALID_NODES_SPECIFIED = "Invalid nodes specified. Source nodes may not be specified.";
        public static final String INVALID_USER_PARAMETER = "Invalid User parameter: %1. Expected %2. Received %3";
        public static final String INVALID_STRUCTURED_DATA = "Incorrectly formatted structured data in the message with post_tran_id %1 ";
        public static final String TEXT_MESSAGE_TOO_LONG = "Text Message too long in the message with post_tran_cust_id %1 ";
        public static final String PREV_TRAN_ID_MISSING = "PreviousTranID is missing from structured data with post_tran_id %1 ";
        public static final String NO_AVS_RESP_CODE = "No AVS_RESP_CODE present in the message with post_tran_id %1 ";
        public static final String NO_TEXT_MESSAGE = "No Text Message present in the message with post_tran_cust_id %1 ";
        public static final String NO_REASON_CODE = "No Reason code provided for message with post_tran_id %1 ";
        public static final String NO_REQ_PAY_SERVICE = "No Request Payment Service provided for message with post_tran_id %1 ";
        public static final String NO_REIMBURSEMENT_ATTR = "No Reimbursement attribute provided for message with post_tran_id %1 ";
        public static final String NO_DOC_IND = "No DOC_IND in the adjustment component of the message with post_tran_id %1 ";
        public static final String NO_ACQ_REF_NR = "No acquirer reference number in baseII of the message with post_tran_id %1 ";
        public static final String NO_ISSUER_CTRL_NR = "No issuer cotrol number in the adjustment component of the message with post_tra" +
                "n_id %1 "
                ;
        public static final String MISSING_MESSAGE_REASON_CODE = "There is no message_reason_code for transaction with post_tran_id%1";
        public static final String MISSING_SOURCE_BIN = "There is no valid source BIN for transaction with post_tran_id%1";
        public static final String INVALID_CHARACTER_IN_AUTH_SRC_CODE_ALTERED = "An invalid character (%1) was detected in the Authorization Source Code field in" +
                " the retention data of the message with post_tran_id %2.The value '5' has been s" +
                "ubstituted for this character"
                ;
        public static final String MALFORMED_USER_PARAMETER = "The %1 user parameter was malformed. The value configured was %2.";
        public static final String MISSING_FLAG = "Not all the flags for option %1 has been supplied. The flags must be %2";
        public static final String MISSING_USER_PARAMETERS = "Missing user parameters. Expected %1. Received %2";
        public static final String NO_SUCH_DIRECTORY = "Output directory does not exist: %1";
        public static final String NSM_FILE_NOT_FOUND = "The Net Settlement Mapping File could not be found. The filename is %1";
        public static final String NSM_MALFORMED_RECORD = "A record in the Net Settlement Mapping File was malformed. The BASE II Settlemen" +
                "t File Extract will not take this record into account when generating the outgoi" +
                "ng CTF file. The malformed record was present in line %1 of the file %2 and cont" +
                "ained the following data: %3"
                ;
        public static final String NSM_UNKNOWN_FLAG_VALUE = "A record in the Net Settlement Mapping File contained an unknown Net Settlement " +
                "Flag value. The BASE II Settlement File Extract has set the Net Settlement Flag " +
                "value for this record to the default value '9'. The record was present in line %" +
                "1 of the file %2 and contained the following data: BIN=%3, Net Settlement Flag=%" +
                "4"
                ;
        public static final String OUTPUT_DIR_NOT_SPECIFIED = "Output directory not specified. The output was created in %1";
        public static final String RETENTION_DATA_STREAM = "Unable to unpack retention data in the message with post_tran_id %1 ";
        public static final String WARN_EXCEPTIONS = "There are transactions that failed during the extract. TCR components were not g" +
                "enerated for these transactions. Please check the visabase2_outgoing_exceptions " +
                "table for  the following post_tran_id(s): %1"
                ;
        public static final String LOG_REF_ERROR = "Updating the post_tran_extract table failed for the transaction with post_tran_i" +
                "d %1."
                ;
        public static final String ERROR_GETTING_SETTLE_AMOUNT_IMPACT = "Failed retrieving the settlement amount impact for %1from the database";
        public static final String ERROR_GETTING_PREV_TRANSACTION = "Failed retrieving the previous transaction for %1from the database";
        public static final String ERROR_LOADING_COLLECTION_ONLY_TABLE = "Failed loading the VISA collection-only table from the database";
        public static final String ERROR_GETTING_MIN_TRAN_ID = "Error retrieving the minimum post_tran id for this session";
        public static final String ACQUIRING_INST_ABSENT = "File Granularity set to one file per acquirer per sink node, but no Acquiring In" +
                "stitution is set for transaction withpost_tran_cust_id: %1"
                ;
        public static final String LINKED_0100_NOT_FOUND = "Linked 0100 message not found for post_tran_cust_id: %1";

        ErrorMessages()
        {
        }
    }


    private String node_names[];
    private Hashtable settle_entity_lookup;
    private int extract_session_id;
    private Hashtable currency_decimal;
    private static Hashtable tran_type_table;
    protected static Connection connection = null;
    private static Hashtable net_settle_flag = new Hashtable();
    private static String nns_curr_code = "";
    private static String nns_fee = "";
    protected static int tcr_record_number = 999;
    private Vector transaction_exceptions;
    private String file_format;
   private String source_node_name = "VISA";
    private String file_location;
    private String terminal_type;
    private String security_code;
    private String test_string;
    private String final_file_name;
    private String report_creation_date;
    private Object reimbursement_attribute;
    private boolean node_granularity;
    private boolean closed_batches_only;
    private boolean use_transaction_date;
    private boolean crb_exception_file_validation;
    private String processing_bin;
    private boolean test;
    protected boolean is_cps_participating_country;
    protected static int file_header_last_digit = 0;
    private boolean process_declined_reversals;
    protected String set_acquiring_institution_id;
    private String acq_ref_nr;
    private String calculated_ra;
    private boolean tcr5_for_icc;
    private boolean use_format_code;
    private String format_code;
    protected static boolean linked_rsp_code = false;
    protected static boolean increment_file_id = false;
    private int compliancyLevel;
    private boolean extract_empty_tcr5_for_TC05_TC06;
    private long min_post_tran_id_for_session;
    private CollectionOnlyFlagChecker collection_only_flag_checker;
    protected static boolean debug = false;
    protected static FileOutputStream dump_file_output_stream = null;
    private static String PARTIAL_REVERSAL = "1";
    private static String _6011_MERCHANT_TYPE = "6011";
    private static String _5399_MERCHANT_TYPE = "5399";
    private static String VISA_DEBIT_PRODUCT_CODE = "0002";
    private static String FORMAT_CODE_FLAG = "fc";
    private static final String CPS_PARTICIPATING_COUNTRY = "CPS";
    private static final String NON_CPS_TRAN_ID = Convert.resize("", 15, ' ', false);
    private static String STRUCTURED_DATA_FIELD_NAME = "TransactionCodeQualifier";
    private static String STRUCTURED_DATA_PROC_CODE = "ProcessingCode";
    private static String NV_NOT_A_VISA_CARD = "NV";
    private static char _0_ZERO_PAD_CHAR = '0';
    private static String RA_KEY = "RA";
    static Hashtable merchant_verification_value = new Hashtable();
    public static final String _SPACE_ = " ";
    public static final String _5_ISSUER_RESPONSE_PROVIDED = "5";
    public static final String _6_OFFLINE_APPROVAL = "6";
    private static final String COMPLIANCY_LEVEL_PREFIX = "-c";
    private static final int DEFAULT_COMPLIANCY_LEVEL = 0;
    protected String[] selected_source_nodes;

    protected String constructNewFileName(FileInformation file_info)
    {
        return file_info.full_file_name + ".dat";
    }

    private Enumeration dbGetSettleEntities(String sink_node_name)
    {
        String query_get_settle_entities = "{call visabase2_get_settlement_entities(?,?,?)}";
        java.sql.CallableStatement stmt = null;
        ResultSet rs = null;
        Vector settle_entities = null;
        SettleEntity settle_entity = null;
        int settle_entity_id = 0;
        int node_granularity_i = node_granularity ? 1 : 0;
        debugPrintln("Start: get settlement entities for node '" + sink_node_name + "' (granularity:" + node_granularity_i + ",min_tran_id:" + min_post_tran_id_for_session + ")", true);
        try
        {
            stmt = connection.prepareCall(query_get_settle_entities);
            stmt.setString(1, sink_node_name);
            if(node_granularity)
            {
                stmt.setInt(2, 1);
            } else
            {
                stmt.setInt(2, 0);
            }
            stmt.setLong(3, min_post_tran_id_for_session);
            rs = stmt.executeQuery();
            settle_entities = new Vector();
            for(; rs.next(); settle_entities.addElement(settle_entity))
            {
                String acquiring_inst_id_code = rs.getString(1);
                settle_entity_id = rs.getInt(2);
                if(acquiring_inst_id_code == null)
                {
                    acquiring_inst_id_code = "";
                }
                settle_entity = new SettleEntity(sink_node_name, settle_entity_id, acquiring_inst_id_code);
            }

        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Failed retrieving the settlement entities for sink node %1 from the database", new String[] {
                    sink_node_name
            }, e);
        }
        finally
        {
            try
            {
                OfficeLib.cleanupStatement(connection, stmt, rs);
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportError("Failed cleaning up the database statement", e);
            }
        }
        debugPrintln("End: get settlement entities for node '" + sink_node_name + "'", true);
        return settle_entities.elements();
    }

    public boolean constructMVVToInterchange(ExtractedTransaction transaction, Tcr5 tcr5, boolean cps_enabled)
            throws Exception
    {
        String card_acceptor_id = constructCardAcceptorID(transaction.card_acceptor_id_code, cps_enabled);
        String tran_code = tcr5.getField(Tcr5.Field.TRANSACTION_CODE);
        boolean is_mvv_present = false;
        if(("05".equals(tran_code) || "06".equals(tran_code)) && card_acceptor_id != null && merchant_verification_value.containsKey(card_acceptor_id))
        {
            String mvv = (String)merchant_verification_value.get(card_acceptor_id);
            tcr5.putField(Tcr5.Field.MERCHANT_VERIFICATION_VALUE, mvv);
            is_mvv_present = true;
        }
        return is_mvv_present;
    }

    public boolean isAtmTransaction(ExtractedTransaction transaction)
    {
        if(transaction.pos_terminal_type != null && transaction.pos_terminal_type.equals("02"))
        {
            return true;
        }
        return transaction.merchant_type != null && transaction.merchant_type.equals(_6011_MERCHANT_TYPE);
    }

    public ExtractResult processExtractedTransaction(ResultSet rs, int first_plugin_column_index, boolean was_opened_in_earlier_session)
            throws SQLException
    {
        ExtractResult result = null;
        ExtractedTransaction transaction = null;
        int i = first_plugin_column_index;
        long post_tran_id = rs.getLong(i++);
        debugPrintln("processExtractedTransaction() [" + post_tran_id + "]");
        try
        {
            transaction = new ExtractedTransaction(rs, post_tran_id, i);
        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Failed creating an ExtractedTransaction instance");
        }
        if(closed_batches_only && transaction.batch_datetime_end == null)
        {
            if(transaction.message_type.equals("0220") && transaction.tran_amount_rsp > 0L || transaction.message_type.equals("0420") && transaction.settle_amount_impact > 0L || isStandaloneReversal(transaction) || isChargebackOrChargebackReversal(transaction) || transaction.message_type.equals("0620") && "4103".equals(transaction.ext_tran_type) || "4105".equals(transaction.ext_tran_type) || "4106".equals(transaction.ext_tran_type) || "4107".equals(transaction.ext_tran_type) || "4002".equals(transaction.ext_tran_type) || "4111".equals(transaction.ext_tran_type))
            {
                debugPrintln("closed....");
                result = new ExtractResult(post_tran_id, true);
                debugPrintln("processExtractedTransaction() - return [a]");
                return result;
            } else
            {
                debugPrintln("processExtractedTransaction() - return [b]");
                return result;
            }
        }
        debugPrintln("open....");
        if(transaction.message_type.equals("0220") && transaction.tran_amount_rsp > 0L && !isStandaloneReversal(transaction))
        {
            boolean include = dbCheckTransactionMsg(post_tran_id, transaction);
            if(process_declined_reversals && isFullyReversedSourceLeg(transaction.post_tran_cust_id))
            {
                include = false;
                debugPrintln("processExtractedTransaction() - return [c]");
                return null;
            }
            if(include)
            {
                setTransaction(transaction);
            }
            result = new ExtractResult(post_tran_id, transaction.is_open);
            debugPrintln("processExtractedTransaction() - return [d]");
            return result;
        }
        if(transaction.message_type.equals("0420") && transaction.settle_amount_impact > 0L || isStandaloneReversal(transaction))
        {
            boolean include = dbCheckReversalMsg(post_tran_id, transaction);
            if(include)
            {
                transaction.reversal = true;
                setTransaction(transaction);
            }
            result = new ExtractResult(post_tran_id, transaction.is_open);
            debugPrintln("processExtractedTransaction() - return [e]");
            return result;
        }
        if(transaction.message_type.equals("0420") && transaction.settle_amount_impact == 0L && !isStandaloneReversal(transaction))
        {
            boolean swapped_settl_amount_impact = dbSwapSettleAmountImpact(transaction);
            if(swapped_settl_amount_impact)
            {
                boolean include = dbCheckReversalMsg(post_tran_id, transaction);
                if(include)
                {
                    transaction.reversal = true;
                    setTransaction(transaction);
                }
                result = new ExtractResult(post_tran_id, transaction.is_open);
                debugPrintln("processExtractedTransaction() - return [f]");
                return result;
            }
        }
        if(transaction.message_type.equals("0620") && ("4100".equals(transaction.ext_tran_type) || isChargebackOrChargebackReversal(transaction) || "4103".equals(transaction.ext_tran_type) || "4105".equals(transaction.ext_tran_type) || "4106".equals(transaction.ext_tran_type) || "4107".equals(transaction.ext_tran_type) || "4002".equals(transaction.ext_tran_type) || "4111".equals(transaction.ext_tran_type)))
        {
            if("4103".equals(transaction.ext_tran_type) || "4111".equals(transaction.ext_tran_type))
            {
                transaction.representment = true;
            }
            setTransaction(transaction);
            result = new ExtractResult(post_tran_id, transaction.is_open);
            debugPrintln("processExtractedTransaction() - return [g]");
            return result;
        } else
        {
            debugPrintln("processExtractedTransaction() - return [h]");
            return result;
        }
    }

    private boolean dbSwapSettleAmountImpact(ExtractedTransaction transaction)
    {
        boolean result = false;
        String query_get_settle_amount = "select settle_amount_impact from post_tran WITH (NOLOCK) where post_tran_cust_id" +
                " = "
                + Long.toString(transaction.post_tran_cust_id) + " and tran_postilion_originated = 0 " + "and message_type = '0420'";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            stmt = connection.prepareStatement(query_get_settle_amount);
            for(rs = stmt.executeQuery(); rs.next();)
            {
                result = true;
                transaction.settle_amount_impact = rs.getLong(1);
                if(transaction.settle_amount_impact < 0L)
                {
                    transaction.settle_amount_impact = -transaction.settle_amount_impact;
                }
                if(transaction.settle_amount_impact == 0L)
                {
                    result = false;
                }
            }

        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Failed retrieving the settlement amount impact for %1from the database", new String[] {
                    Long.toString(transaction.post_tran_id)
            }, e);
        }
        finally
        {
            try
            {
                OfficeLib.cleanupStatement(connection, stmt, rs);
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportError("Failed cleaning up the database statement", e);
            }
        }
        return result;
    }

    private void transactionException(FileInformation file_info, ExtractedTransaction transaction, String fail_reason)
    {
        if(fail_reason == null)
        {
            fail_reason = "No reason for failure provided.";
        }
        dbTransactionException(file_info, transaction, fail_reason);
        transaction_exceptions.addElement(new String(String.valueOf(transaction.post_tran_id)));
        file_info.batch_info.exception_counter++;
    }

    private Hashtable dbLoadCollectionOnlyTable()
    {
        PreparedStatement stmt = null;
        Hashtable result = new Hashtable();
        debugPrintln("dbLoadCollectionOnlyTable()");
        try
        {
            stmt = connection.prepareStatement("SELECT name, bin_list FROM visabase2_collection_only");
            String name;
            String bin_list;
            for(ResultSet result_set = stmt.executeQuery(); result_set.next(); result.put(name, bin_list))
            {
                name = result_set.getString(1);
                bin_list = result_set.getString(2);
                debugPrintln("  name = " + name + ", bin_list = " + bin_list);
            }

        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Failed loading the VISA collection-only table from the database", new String[0], e);
        }
        finally
        {
            try
            {
                OfficeLib.cleanupStatement(connection, stmt, null);
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportError("Failed loading the VISA collection-only table from the database", e);
            }
        }
        return result;
    }

    private static String constructCardAcceptorID(String card_acceptor_id, boolean cps_enabled)
    {
        if(card_acceptor_id == null)
        {
            return null;
        }
        String acq_inst_name = null;
        if(cps_enabled)
        {
            acq_inst_name = dbGetAcquirerInstitutionName(card_acceptor_id);
        }
        if(acq_inst_name == null)
        {
            acq_inst_name = card_acceptor_id;
        }
        return acq_inst_name;
    }

    protected void getTcr1ChargebackFields(Tcr1 tcr1, ExtractedTransaction transaction)
            throws Exception
    {
        String avs_rsp_code = null;
        String terminal_type = transaction.pos_terminal_type;
        String auth_source_code = null;
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        BaseII base2 = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            base2 = getBaseII(struct_data);
            adj_comp = getAdjustmentComponent(struct_data);
        }
        String proc_code_string = adj_comp.getProcessingCode();
        ProcessingCode proc_code = new ProcessingCode(proc_code_string);
        String tran_type = proc_code.getTranType();
        if(base2 != null)
        {
            avs_rsp_code = base2.getAVSRspCode();
            auth_source_code = base2.getAuthSourceCode();
        }
        if(avs_rsp_code != null)
        {
            tcr1.putField(Tcr1.Field.AVS_RESP_CODE, avs_rsp_code);
        } else
        {
            tcr1.putField(Tcr1.Field.AVS_RESP_CODE, " ");
        }
        if(auth_source_code != null)
        {
            if(!" ".equals(auth_source_code) && !Validator.isValidAn(auth_source_code))
            {
                VisaBase2ExtractLog.reportWarning("An invalid character (%1) was detected in the Authorization Source Code field in" +
                                " the retention data of the message with post_tran_id %2.The value '5' has been s" +
                                "ubstituted for this character"
                        , new String[] {
                                auth_source_code, Long.toString(transaction.post_tran_id)
                        });
                auth_source_code = "5";
            }
        } else
        {
            auth_source_code = "6";
        }
        tcr1.putField(Tcr1.Field.AUTH_SOURCE_CODE, auth_source_code);
        String doc_ind = null;
        String message_text = null;
        String chargeback_ref_nr = null;
        String from_acc = null;
        if(adj_comp != null)
        {
            doc_ind = adj_comp.getDocumentationIndicator();
            message_text = adj_comp.getMessageText();
            chargeback_ref_nr = adj_comp.getIssuerRefNr();
        }
        if(chargeback_ref_nr != null)
        {
            tcr1.putField(Tcr1.Field.CHARGE_BACK_REF, chargeback_ref_nr);
        }
        if(doc_ind != null)
        {
            tcr1.putField(Tcr1.Field.DOC_IND, doc_ind);
        } else
        {
            VisaBase2ExtractLog.reportWarning("No DOC_IND in the adjustment component of the message with post_tran_id %1 ", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
        }
        if(message_text != null)
        {
            message_text = Convert.resize(message_text, 50, ' ', true);
            tcr1.putField(Tcr1.Field.MEMBER_MSG, message_text);
        }
        if("09".equals(tran_type))
        {
            tcr1.putField(Tcr1.Field.CASH_BACK, Convert.resize(Long.toString(transaction.tran_cash_rsp), 9, '0', false));
        } else
        {
            tcr1.putField(Tcr1.Field.CASH_BACK, Convert.resize("", 9, '0', false));
        }
        from_acc = proc_code.getFromAccount();
        String atm_account_selection = null;
        if(terminal_type.equals("02"))
        {
            if(proc_code_string != null)
            {
                if(postilion.core.message.bitmap.Iso8583.AccountType.isSavings(from_acc))
                {
                    atm_account_selection = "1";
                } else
                if(postilion.core.message.bitmap.Iso8583.AccountType.isCheck(from_acc))
                {
                    atm_account_selection = "2";
                } else
                if(postilion.core.message.bitmap.Iso8583.AccountType.isCredit(from_acc))
                {
                    atm_account_selection = "3";
                } else
                {
                    atm_account_selection = "0";
                }
            }
        } else
        {
            atm_account_selection = "0";
        }
        tcr1.putField(Tcr1.Field.ATM_ACCOUNT_SELECTION, atm_account_selection);
        String sp_chargeback_ind = null;
        if(adj_comp.getOriginalAmounts() != null && adj_comp.getOriginalAmounts().getTranAmount() != null)
        {
            String orig_tran_amount = adj_comp.getOriginalAmounts().getTranAmount();
            if(transaction.tran_amount_rsp != Long.parseLong(orig_tran_amount))
            {
                sp_chargeback_ind = "P";
            }
        }
        if(sp_chargeback_ind == null)
        {
            sp_chargeback_ind = " ";
        }
        tcr1.putField(Tcr1.Field.SP_CHARGE_BACK_IND, sp_chargeback_ind);
    }

    protected Tcr5 getTcr5ForChargeback(ExtractedTransaction transaction, IccRequest icc_request, BatchInformation batch_info, boolean cps_enabled)
            throws Exception
    {
        Tcr5 tcr5 = new Tcr5();
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        BaseII base2 = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            base2 = getBaseII(struct_data);
            adj_comp = getAdjustmentComponent(struct_data);
        }
        String base_i_xml = null;
        if(transaction.structured_data_rsp != null)
        {
            StructuredData struct_data_rsp = new StructuredData();
            try
            {
                struct_data_rsp.fromMsgString(transaction.structured_data_rsp);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_id)
                });
            }
            base_i_xml = struct_data_rsp.get("BaseI");
        }
        String tran_id = null;
        if(base2 != null)
        {
            tran_id = base2.getTranId();
        }
        if(tran_id != null)
        {
            cps_enabled = true;
        } else
        {
            cps_enabled = false;
        }
        BaseI base_i = null;
        String card_level_results = null;
        if(base_i_xml != null)
        {
            base_i = (BaseI)ObjectFactory.createObjectFromString(base_i_xml);
            card_level_results = base_i.getCardLevelResults();
        }
        if(card_level_results != null)
        {
            tcr5.putField(Tcr5.Field.PRODUCT_ID, card_level_results);
        } else
        {
            tcr5.putField(Tcr5.Field.PRODUCT_ID, "  ");
        }
        String tran_type = transaction.tran_type;
        if(adj_comp != null)
        {
            String proc_code_string = adj_comp.getProcessingCode();
            ProcessingCode proc_code = new ProcessingCode(proc_code_string);
            tran_type = proc_code.getTranType();
        }
        String transaction_code = (String)tran_type_table.get(tran_type);
        transaction_code = getTranType(transaction_code, transaction.ext_tran_type);
        String pan_entry_mode = transaction.pos_entry_mode.substring(0, 2);
        boolean is_icc_transaction = false;
        if(pan_entry_mode.equals("05") || pan_entry_mode.equals("07") || pan_entry_mode.equals("95"))
        {
            is_icc_transaction = true;
        }
        if(!tcr5_for_icc)
        {
            if(!cps_enabled && !"07".equals(transaction_code))
            {
                return null;
            }
        } else
        if(!is_icc_transaction && transaction.card_verification_result == null)
        {
            return null;
        }
        tcr5.putField(Tcr5.Field.TRANSACTION_CODE, transaction_code);
        if(struct_data != null)
        {
            String code_qualifier_str = struct_data.get(STRUCTURED_DATA_FIELD_NAME);
            if(code_qualifier_str != null)
            {
                tcr5.putField(Tcr5.Field.CODE_QUALIFIER, code_qualifier_str);
            }
        }
        String auth_resp_code = null;
        String validation_code = "    ";
        String market_auth_data = " ";
        if(base2 != null)
        {
            if(base2.getMarketSpecAuth() != null)
            {
                market_auth_data = base2.getMarketSpecAuth();
            }
            if(base2.getMultiClearingSeqNr() != null)
            {
                tcr5.putField(Tcr5.Field.MULTIPLE_CLEARING_SEQ, base2.getMultiClearingSeqNr());
            }
        }
        if(icc_request != null)
        {
            String resp_code = constructAuthRspCodeICC(transaction, icc_request);
            if(resp_code != null && resp_code.length() != 0)
            {
                auth_resp_code = resp_code;
            }
        }
        if(auth_resp_code == null && base2 != null && base2.getAuthRspCode() != null)
        {
            auth_resp_code = base2.getAuthRspCode();
        }
        if(auth_resp_code == null)
        {
            auth_resp_code = "  ";
        }
        if(cps_enabled && transaction_code.substring(1).equals("6"))
        {
            auth_resp_code = "  ";
            validation_code = "    ";
        }
        if(tran_id == null)
        {
            tran_id = Convert.resize("", 15, ' ', true);
        }
        if("6".equals(transaction_code.substring(1)))
        {
            tcr5.putField(Tcr5.Field.AUTHORIZATION_AMOUNT, "000000000000");
        } else
        {
            tcr5.putField(Tcr5.Field.AUTHORIZATION_AMOUNT, Long.toString(transaction.tran_amount_rsp));
        }
        tcr5.putField(Tcr5.Field.TRANSACTION_ID, tran_id);
        tcr5.putField(Tcr5.Field.AUTH_RESP_CODE, auth_resp_code);
        tcr5.putField(Tcr5.Field.VALIDATION_CODE, validation_code);
        tcr5.putField(Tcr5.Field.MARKET_AUTH_DATA, market_auth_data);
        tcr5.putField(Tcr5.Field.TOTAL_AUTH_AMOUNT, "000000000000");
        tcr5.putField(Tcr5.Field.AUTH_CURRENCY_CODE, "   ");
        debugPrintln("TCR 5 CHARGEBACK");
        debugPrintln(transaction_code);
        debugPrintln(transaction.card_verification_result);
        if(transaction.card_verification_result != null && !transaction.card_verification_result.equalsIgnoreCase("Y") && !isDisbursementTransactionCode(transaction_code))
        {
            debugPrintln("TCR 5");
            tcr5.putField(Tcr5.Field.CVV2_RESULT_VALUE, transaction.card_verification_result);
            debugPrintln("Value: " + tcr5.getField(Tcr5.Field.CVV2_RESULT_VALUE));
        } else
        {
            tcr5.putField(Tcr5.Field.CVV2_RESULT_VALUE, " ");
        }
        return tcr5;
    }

    protected String generateTc91(BatchInformation batch_info, int batch_nr, FileInformation file_info)
            throws Exception
    {
        batch_info.batch_tcr_counter++;
        batch_info.file_tcr_counter++;
        return "9100" + Convert.resize("", 6, '0', true) + Convert.resize("", 5, '0', true) + Convert.resize("", 15, '0', true) + Convert.resize(String.valueOf(batch_info.batch_monetary_tc_counter), 12, '0', false) + Convert.resize(String.valueOf(batch_info.batch_no_counter), 6, '0', false) + Convert.resize(String.valueOf(batch_info.batch_tcr_counter), 12, '0', false) + Convert.resize("", 6, '0', true) + Convert.resize("" + batch_nr, 8, ' ', true) + Convert.resize(String.valueOf(batch_info.batch_tc_counter + 1L), 9, '0', false) + Convert.resize("", 18, '0', true) + Convert.resize(String.valueOf(batch_info.batch_source_amount), 15, '0', false) + Convert.resize("", 15, '0', true) + Convert.resize("", 15, '0', true) + Convert.resize("", 15, '0', true);
    }

    protected Tcr0ForTc52 getTcr0ForTc52(FileInformation file_info, ExtractedTransaction transaction, IccRequest icc_request, boolean cps_enabled)
            throws Exception
    {
        Tcr0ForTc52 tcr0fortc52 = new Tcr0ForTc52();
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        BaseII base2 = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            base2 = getBaseII(struct_data);
            adj_comp = getAdjustmentComponent(struct_data);
        }
        String proc_code_string = null;
        String issuer_ref_nr = null;
        String acq_ref_nr = null;
        if(adj_comp != null)
        {
            if(adj_comp.getAcquirerRefNr() != null)
            {
                acq_ref_nr = adj_comp.getAcquirerRefNr();
            }
            proc_code_string = adj_comp.getProcessingCode();
            issuer_ref_nr = adj_comp.getIssuerRefNr();
        }
        if(acq_ref_nr != null)
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.ACQ_REFERENCE_NO, acq_ref_nr);
        } else
        {
            VisaBase2ExtractLog.reportWarning("No acquirer reference number in baseII of the message with post_tran_id %1 ", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
        }
        String pan = transaction.pan;
        tcr0fortc52.putField(Tcr0ForTc52.Field.ACCOUNT_NUMBER, Convert.resize(pan, 19, '0', true));
        if(transaction.sponsor_bank != null)
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.ACQ_BUSINESS_ID, transaction.sponsor_bank);
        } else
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.ACQ_BUSINESS_ID, Convert.resize("", 8, '0', true));
        }
        String purchase_date = transaction.datetime_tran_local.get("MMdd");
        tcr0fortc52.putField(Tcr0ForTc52.Field.PURCHASE_DATE, purchase_date);
        tcr0fortc52.putField(Tcr0ForTc52.Field.TRAN_AMOUNT, Long.toString(transaction.tran_amount_rsp));
        tcr0fortc52.putField(Tcr0ForTc52.Field.TRAN_CURRENCY_CODE, transaction.tran_currency_code);
        String name_location = transaction.card_acceptor_name_loc;
        tcr0fortc52.putField(Tcr0ForTc52.Field.MERCHANT_NAME, name_location.substring(0, 23) + "  ");
        tcr0fortc52.putField(Tcr0ForTc52.Field.MERCHANT_CITY, name_location.substring(23, 36));
        tcr0fortc52.putField(Tcr0ForTc52.Field.MERCHANT_COUNTRY_CODE, name_location.substring(38, 40) + " ");
        if(issuer_ref_nr != null)
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.ISSUER_CONTROL_NO, Convert.resize(issuer_ref_nr, 9, '0', false));
        } else
        if("US ".equals(tcr0fortc52.getField(Tcr0ForTc52.Field.MERCHANT_COUNTRY_CODE)))
        {
            VisaBase2ExtractLog.reportWarning("No issuer cotrol number in the adjustment component of the message with post_tra" +
                            "n_id %1 "
                    , new String[] {
                            Long.toString(transaction.post_tran_cust_id)
                    });
            transaction.is_open = true;
        } else
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.ISSUER_CONTROL_NO, Convert.resize("", 9, ' ', true));
        }
        String merchant_type = transaction.merchant_type;
        if(merchant_type == null)
        {
            if("02".equals(transaction.pos_terminal_type))
            {
                merchant_type = _6011_MERCHANT_TYPE;
            } else
            {
                merchant_type = _5399_MERCHANT_TYPE;
            }
        }
        tcr0fortc52.putField(Tcr0ForTc52.Field.MERCHANT_CATEGORY_CODE, merchant_type);
        String state = "   ";
        String postal = Convert.resize("", 5, '0', false);
        if(base2 != null && base2.getPOSGeoData() != null)
        {
            String pos_geographic_data = base2.getPOSGeoData();
            String post_temp = pos_geographic_data.substring(3);
            int post = post_temp.length() - 5;
            if(post >= 0)
            {
                postal = post_temp.substring(post, post_temp.length());
            } else
            {
                postal = post_temp.substring(0, post_temp.length());
                postal = Convert.resize(postal, 5, '0', false);
            }
            state = pos_geographic_data.substring(0, 3);
        }
        if("US ".equals(tcr0fortc52.getField(Tcr0ForTc52.Field.MERCHANT_COUNTRY_CODE)) || "CA ".equals(tcr0fortc52.getField(Tcr0ForTc52.Field.MERCHANT_COUNTRY_CODE)))
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.MERCHANT_STATE_CODE, state);
        } else
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.MERCHANT_STATE_CODE, Convert.resize("", 3, ' ', false));
        }
        if("US ".equals(tcr0fortc52.getField(Tcr0ForTc52.Field.MERCHANT_COUNTRY_CODE)))
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.MERCHANT_ZIP_CODE, Convert.resize(postal, 5, '0', true));
        } else
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.MERCHANT_ZIP_CODE, Convert.resize(postal, 5, '0', true));
        }
        String reason_code = transaction.message_reason_code;
        if(reason_code != null && reason_code.length() == 4)
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.REQUEST_REASON_CODE, reason_code.substring(2));
        } else
        {
            VisaBase2ExtractLog.reportWarning("No Reason code provided for message with post_tran_id %1 ", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
        }
        tcr0fortc52.putField(Tcr0ForTc52.Field.SETTLEMENT_FLAG, getNetSettlementFlag(tcr0fortc52.getField(Tcr0ForTc52.Field.ACCOUNT_NUMBER), tcr0fortc52.getField(Tcr0ForTc52.Field.TRAN_CURRENCY_CODE)));
        if(!tcr0fortc52.getField(Tcr0ForTc52.Field.SETTLEMENT_FLAG).equals(Tcr0.Settlement._9_BASE2_SELECTS))
        {
            tcr0fortc52.putField(Tcr0ForTc52.Field.NATIONAL_REIMBURSEMENT_FEE, nns_fee);
        }
        String atm_account_selection = "0";
        if("6011".equals(tcr0fortc52.getField(Tcr0ForTc52.Field.MERCHANT_CATEGORY_CODE)) && proc_code_string != null)
        {
            ProcessingCode proc_code = new ProcessingCode(proc_code_string);
            String from_acc = proc_code.getFromAccount();
            if(postilion.core.message.bitmap.Iso8583.AccountType.isSavings(from_acc))
            {
                atm_account_selection = "1";
            } else
            if(postilion.core.message.bitmap.Iso8583.AccountType.isCheck(from_acc))
            {
                atm_account_selection = "2";
            } else
            if(postilion.core.message.bitmap.Iso8583.AccountType.isCredit(from_acc))
            {
                atm_account_selection = "3";
            } else
            {
                atm_account_selection = "0";
            }
        }
        tcr0fortc52.putField(Tcr0ForTc52.Field.ATM_ACCOUNT_SELECTION, atm_account_selection);
        return tcr0fortc52;
    }

    private static String getTimeString()
    {
        return (new DateTime()).get("HH:mm:ss.SSS");
    }

    private String getTranType(String transaction_code, String extended_tran_type)
    {
        if("4101".equals(extended_tran_type))
        {
            transaction_code = "1" + transaction_code.substring(1);
        } else
        if("4102".equals(extended_tran_type))
        {
            transaction_code = "3" + transaction_code.substring(1);
        }
        return transaction_code;
    }

    protected Tcr0 getTcr0(FileInformation file_info, ExtractedTransaction transaction, Object reimbursement_attribute, IccRequest icc_request, boolean cps_enabled)
            throws Exception
    {
        Tcr0 tcr0 = new Tcr0();
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        BaseII base2 = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            base2 = getBaseII(struct_data);
            adj_comp = getAdjustmentComponent(struct_data);
        }
        String tran_type = transaction.tran_type;
        if((transaction.representment || isStandaloneReversal(transaction)) && adj_comp != null && adj_comp.getProcessingCode() != null)
        {
            String proc_code_string = adj_comp.getProcessingCode();
            ProcessingCode proc_code = new ProcessingCode(proc_code_string);
            tran_type = proc_code.getTranType();
        }
        if(transaction.representment)
        {
            String reason_code = transaction.message_reason_code;
            if(reason_code != null && reason_code.length() == 4)
            {
                tcr0.putField(Tcr0.Field.REASON_CODE, reason_code.substring(2));
            }
        }
        String transaction_code = (String)tran_type_table.get(tran_type);
        if(isChargebackOrChargebackReversal(transaction))
        {
            getTcr0ChargebackFields(file_info, tcr0, transaction);
            if(transaction.is_open)
            {
                return null;
            }
            String tran_id = null;
            if(base2 != null)
            {
                tran_id = base2.getTranId();
            }
            if(tran_id != null)
            {
                cps_enabled = true;
            } else
            {
                cps_enabled = false;
            }
        } else
        {
            if(transaction.reversal || transaction.ext_tran_type != null && transaction.ext_tran_type.equals("4111"))
            {
                int tran_code_int = Integer.parseInt(transaction_code);
                transaction_code = Convert.resize(Integer.toString(tran_code_int += 20), 2, '0', false);
            }
            if(transaction_code == null)
            {
                transactionException(file_info, transaction, "transaction_code is null");
                return null;
            }
            tcr0.putField(Tcr0.Field.TRANSACTION_CODE, transaction_code);
            if(isPrepaidLoadTransaction(transaction))
            {
                tcr0.putField(Tcr0.Field.CODE_QUALIFIER, "2");
            }
            if(crb_exception_file_validation)
            {
                tcr0.putField(Tcr0.Field.CRB_EXCEPTION, "N");
            }
            String auth_id_resp = transaction.auth_id_rsp;
            if(auth_id_resp == null)
            {
                tcr0.putField(Tcr0.Field.AUTH_CODE, "      ");
            } else
            {
                tcr0.putField(Tcr0.Field.AUTH_CODE, auth_id_resp);
            }
            String pos_condition_code = transaction.pos_condition_code;
            if(pos_condition_code != null)
            {
                if(pos_condition_code.equals("13") && !transaction.reversal || transaction.representment)
                {
                    tcr0.putField(Tcr0.Field.USAGE_CODE, "2");
                } else
                {
                    tcr0.putField(Tcr0.Field.USAGE_CODE, "1");
                }
            } else
            {
                tcr0.putField(Tcr0.Field.USAGE_CODE, "1");
            }
            if(cps_enabled)
            {
                tcr0.putField(Tcr0.Field.REQ_PAYMENT_SERVICE, "9");
            } else
            {
                tcr0.putField(Tcr0.Field.REQ_PAYMENT_SERVICE, " ");
            }
            String acq_ref_nr_string = null;
            if(adj_comp != null)
            {
                acq_ref_nr_string = adj_comp.getAcquirerRefNr();
            }
            if(acq_ref_nr_string != null && acq_ref_nr_string.length() == 23)
            {
                String format_code = acq_ref_nr_string.substring(0, 1);
                tcr0.putField(Tcr0.Field.FORMAT_CODE, format_code);
                String sink_node_name = file_info.sink_node_name;
                String acquirer_id = transaction.acquiring_inst_id_code;
                boolean collection_only = collection_only_flag_checker.isCollectionOnlyTransaction(sink_node_name, acquirer_id, transaction.pan);
                if(collection_only && !processing_bin.equals("      "))
                {
                    tcr0.putField(Tcr0.Field.BIN, Convert.resize(processing_bin, 6, '0', true));
                } else
                {
                    String acq_inst = acq_ref_nr_string.substring(1, 7);
                    tcr0.putField(Tcr0.Field.BIN, acq_inst);
                }
                String date = acq_ref_nr_string.substring(7, 11);
                tcr0.putField(Tcr0.Field.DATE, date);
                String film_locator = acq_ref_nr_string.substring(11, 22);
                tcr0.putField(Tcr0.Field.FILM_LOCATOR, film_locator);
            } else
            {
                String usage_code = tcr0.getField(Tcr0.Field.USAGE_CODE);
                if(use_format_code)
                {
                    tcr0.putField(Tcr0.Field.FORMAT_CODE, this.format_code);
                } else
                if(is_cps_participating_country && transaction_code.equals("05") && usage_code.equals("1"))
                {
                    tcr0.putField(Tcr0.Field.FORMAT_CODE, "2");
                } else
                {
                    tcr0.putField(Tcr0.Field.FORMAT_CODE, "7");
                }
                String acq_inst = transaction.acquiring_inst_id_code;
                if(acq_inst == null)
                {
                    transactionException(file_info, transaction, "acquiring_inst_id_code is null");
                    return null;
                }
                String sink_node_name = file_info.sink_node_name;
                String acquirer_id = transaction.acquiring_inst_id_code;
                boolean collection_only = collection_only_flag_checker.isCollectionOnlyTransaction(sink_node_name, acquirer_id, transaction.pan);
                if(collection_only && !processing_bin.equals("      "))
                {
                    tcr0.putField(Tcr0.Field.BIN, Convert.resize(processing_bin, 6, '0', true));
                } else
                {
                    tcr0.putField(Tcr0.Field.BIN, Convert.resize(acq_inst, 6, '0', true));
                }
                if(use_transaction_date)
                {
                    tcr0.putField(Tcr0.Field.DATE, transaction.datetime_tran_local.get("yyDDD").substring(1));
                } else
                {
                    tcr0.putField(Tcr0.Field.DATE, (new DateTime()).get("yyDDD").substring(1));
                }
                String tran_nr = String.valueOf(transaction.tran_nr);
                tcr0.putField(Tcr0.Field.FILM_LOCATOR, Convert.resize(tran_nr, 11, '0', false));
            }
        }
        if(struct_data != null)
        {
            String code_qualifier_str = struct_data.get(STRUCTURED_DATA_FIELD_NAME);
            if(code_qualifier_str != null)
            {
                tcr0.putField(Tcr0.Field.CODE_QUALIFIER, code_qualifier_str);
            }
        }
        String pan = transaction.pan;
        tcr0.putField(Tcr0.Field.ACCOUNT_NUMBER, Convert.resize(pan, 19, '0', true));
        String check_str = tcr0.getField(Tcr0.Field.FORMAT_CODE) + tcr0.getField(Tcr0.Field.BIN) + tcr0.getField(Tcr0.Field.DATE) + tcr0.getField(Tcr0.Field.FILM_LOCATOR);
        String check_digit = calcCheckDigit(check_str);
        tcr0.putField(Tcr0.Field.CHECK_DIGIT, check_digit);
        acq_ref_nr = check_str + check_digit;
        String acq_bus_id = transaction.sponsor_bank;
        if(acq_bus_id != null && Validator.isValidN(acq_bus_id))
        {
            tcr0.putField(Tcr0.Field.ACQUIRER_BUSS_ID, acq_bus_id);
        } else
        {
            tcr0.putField(Tcr0.Field.ACQUIRER_BUSS_ID, "0");
        }
        String purchase_date = transaction.datetime_tran_local.get("MMdd");
        tcr0.putField(Tcr0.Field.PURCHASE_DATE, purchase_date);
        long amount = 0L;
        if(transaction.reversal)
        {
            if(isStandaloneReversal(transaction) && adj_comp != null && adj_comp.getOriginalAmounts() != null && adj_comp.getOriginalAmounts().getTranAmount() != null)
            {
                amount = Long.parseLong(adj_comp.getOriginalAmounts().getTranAmount());
            } else
            {
                amount = transaction.settle_amount_impact;
            }
        } else
        if(transaction.tran_reversed.equals(PARTIAL_REVERSAL))
        {
            try
            {
                amount = transaction.new_tran_amount_rsp;
            }
            catch(Exception e)
            {
                debugPrintln("Transaction failed, post_tran_id = " + transaction.post_tran_id);
            }
        } else
        {
            amount = transaction.tran_amount_rsp;
        }
        String curr_code = transaction.tran_currency_code;
        String str_amount = Long.toString(amount);
        if(curr_code != null && !curr_code.equals("") && currency_decimal.containsKey(curr_code))
        {
            String decimal = (String)currency_decimal.get(curr_code);
            if(decimal.equals("0"))
            {
                str_amount = str_amount + "00";
                amount = Long.parseLong(str_amount);
            } else
            if(decimal.equals("3"))
            {
                str_amount = str_amount.substring(0, str_amount.length() - 1);
                amount = Long.parseLong(str_amount);
            }
        }
        tcr0.putField(Tcr0.Field.SOURCE_AMOUNT, str_amount);
        tcr0.putField(Tcr0.Field.SOURCE_CURRENCY_CODE, transaction.tran_currency_code);
        String name_location = transaction.card_acceptor_name_loc;
        tcr0.putField(Tcr0.Field.MERCHANT_NAME, name_location.substring(0, 23) + "  ");
        tcr0.putField(Tcr0.Field.MERCHANT_CITY, name_location.substring(23, 36));
        tcr0.putField(Tcr0.Field.MERCHANT_COUNTRY_CODE, name_location.substring(38, 40) + " ");
        String merchant_type = transaction.merchant_type;
        if(merchant_type == null)
        {
            if("02".equals(transaction.pos_terminal_type))
            {
                merchant_type = _6011_MERCHANT_TYPE;
            } else
            {
                merchant_type = _5399_MERCHANT_TYPE;
            }
        }
        tcr0.putField(Tcr0.Field.MERCHANT_CATEGORY_CODE, merchant_type);
        String zip = "00000";
        String state = "   ";
        if(transaction.retention_data != null && transaction.retention_data.isFieldSet("POS Geographic data"))
        {
            String pos_geographic_data = transaction.retention_data.getField("POS Geographic data");
            String zip_temp = pos_geographic_data.substring(3);
            int zip_pos = zip_temp.length() - 5;
            if(zip_pos >= 0)
            {
                zip = zip_temp.substring(zip_pos, zip_temp.length());
            } else
            {
                zip = zip_temp.substring(0, zip_temp.length());
                zip = Convert.resize(zip, 5, '0', false);
            }
            state = pos_geographic_data.substring(0, 3);
        }
        tcr0.putField(Tcr0.Field.MERCHANT_ZIP_CODE, zip);
        tcr0.putField(Tcr0.Field.MERCHANT_STATE_CODE, state);
        String pan_entry_mode = null;
        long prev_tran_prev_post_tran_nr = transaction.prev_prev_post_tran_id;
        String prev_pos_entry_mode = null;
        if(prev_tran_prev_post_tran_nr == 0L)
        {
            prev_pos_entry_mode = transaction.prev_pos_entry_mode;
        } else
        {
            prev_pos_entry_mode = getOriginal0100PosEntryMode(transaction);
        }
        if(prev_pos_entry_mode != null)
        {
            pan_entry_mode = prev_pos_entry_mode.substring(0, 2);
        } else
        if(transaction.pos_entry_mode != null)
        {
            pan_entry_mode = transaction.pos_entry_mode.substring(0, 2);
        }
        tcr0.putField(Tcr0.Field.SETTLEMENT_FLAG, getNetSettlementFlag(tcr0.getField(Tcr0.Field.ACCOUNT_NUMBER), tcr0.getField(Tcr0.Field.SOURCE_CURRENCY_CODE)));
        if(cps_enabled)
        {
            if(tcr0.getField(Tcr0.Field.REQ_PAYMENT_SERVICE) != null && tcr0.getField(Tcr0.Field.REQ_PAYMENT_SERVICE).equals("9"))
            {
                tcr0.putField(Tcr0.Field.AUTH_CHARACTERISTICS, "E");
            } else
            if(pan_entry_mode.equals("02") || pan_entry_mode.equals("07") || pan_entry_mode.equals("90") || pan_entry_mode.equals("05") || pan_entry_mode.equals("91") || pan_entry_mode.equals("95") || icc_request != null)
            {
                String pos_operating_environment = transaction.pos_operating_environment;
                if("2".equals(pos_operating_environment) || "4".equals(pos_operating_environment) || "5".equals(pos_operating_environment))
                {
                    tcr0.putField(Tcr0.Field.AUTH_CHARACTERISTICS, "C");
                } else
                {
                    tcr0.putField(Tcr0.Field.AUTH_CHARACTERISTICS, "E");
                }
            } else
            {
                tcr0.putField(Tcr0.Field.AUTH_CHARACTERISTICS, "M");
            }
        } else
        {
            tcr0.putField(Tcr0.Field.AUTH_CHARACTERISTICS, "N");
        }
        String card_data_input_capability = transaction.pos_card_data_input_ability;
        if(card_data_input_capability.equals("6"))
        {
            card_data_input_capability = "9";
        } else
        if("7".equals(card_data_input_capability))
        {
            card_data_input_capability = "2";
        } else
        if("8".equals(card_data_input_capability))
        {
            card_data_input_capability = "5";
        } else
        if("9".equals(card_data_input_capability))
        {
            card_data_input_capability = "0";
        } else
        if(card_data_input_capability.equals("A") || card_data_input_capability.equals("B"))
        {
            card_data_input_capability = "5";
            if(icc_request != null)
            {
                String term_cap_binary = Convert.fromHexToBin(icc_request.getTerminalCapabilities());
                if((term_cap_binary.getBytes()[0] & 0x20) == 0)
                {
                    card_data_input_capability = "8";
                }
            }
        }
        tcr0.putField(Tcr0.Field.POS_TERM_CAPAB, card_data_input_capability);
        String cardholder_auth_method = transaction.pos_cardholder_auth_method;
        String cardholder_present = transaction.pos_cardholder_present;
        String pos_terminal_type = transaction.pos_terminal_type;
        if(cardholder_present.equals("2") || cardholder_present.equals("3") || cardholder_present.equals("5"))
        {
            tcr0.putField(Tcr0.Field.CARDHOLDER_ID_METHOD, "4");
        } else
        if(cardholder_auth_method.equals("1"))
        {
            tcr0.putField(Tcr0.Field.CARDHOLDER_ID_METHOD, "2");
        } else
        if(cardholder_auth_method.equals("5"))
        {
            if("2".equals(cardholder_present) || "3".equals(cardholder_present) || "90".equals(pos_terminal_type) || "91".equals(pos_terminal_type) || "92".equals(pos_terminal_type) || "93".equals(pos_terminal_type) || "94".equals(pos_terminal_type) || "95".equals(pos_terminal_type) || "96".equals(pos_terminal_type))
            {
                tcr0.putField(Tcr0.Field.CARDHOLDER_ID_METHOD, "4");
            } else
            {
                tcr0.putField(Tcr0.Field.CARDHOLDER_ID_METHOD, "1");
            }
        } else
        if(cardholder_auth_method.equals("0"))
        {
            tcr0.putField(Tcr0.Field.CARDHOLDER_ID_METHOD, "1");
        } else
        {
            tcr0.putField(Tcr0.Field.CARDHOLDER_ID_METHOD, " ");
        }
        tcr0.putField(Tcr0.Field.POS_ENTRY_MODE, pan_entry_mode);
        if(transaction.ext_tran_type != null && transaction.ext_tran_type.equals("4111") || isStandaloneReversal(transaction))
        {
            String yddd = getYDDD(transaction.datetime_tran_local.get("MMdd"));
            tcr0.putField(Tcr0.Field.CENTRAL_PROC_DATE, yddd);
        } else
        if(!transaction.reversal)
        {
            tcr0.putField(Tcr0.Field.CENTRAL_PROC_DATE, "0000");
        } else
        if(transaction.prev_datetime_tran_local != null)
        {
            String yddd = getYDDD(transaction.prev_datetime_tran_local.get("MMdd"));
            tcr0.putField(Tcr0.Field.CENTRAL_PROC_DATE, yddd);
        } else
        {
            transactionException(file_info, transaction, "prev_datetime_tran_local is null");
            return null;
        }
        if(!transaction.tran_reversed.equals(PARTIAL_REVERSAL))
        {
            file_info.batch_info.file_source_amount += amount;
            file_info.batch_info.batch_source_amount += amount;
        } else
        {
            file_info.batch_info.file_source_amount += transaction.new_tran_amount_rsp;
            file_info.batch_info.batch_source_amount += transaction.new_tran_amount_rsp;
        }
        if(isPrepaidLoadTransaction(transaction))
        {
            tcr0.putField(Tcr0.Field.REIMBURSEMENT_ATTR, "Z");
        } else
        if(!isChargebackOrChargebackReversal(transaction) && (reimbursement_attribute instanceof IReimbursementAttribute))
        {
            IReimbursementAttribute i_reimbursement_attribute = (IReimbursementAttribute)reimbursement_attribute;
            tcr0.putField(Tcr0.Field.REIMBURSEMENT_ATTR, i_reimbursement_attribute.getReimbursementAttribute(tcr0));
            calculated_ra = tcr0.getField(Tcr0.Field.REIMBURSEMENT_ATTR);
        }
        tcr0 = checkForCollectionOnlyFlag(transaction, transaction_code, file_info, tcr0);
        return tcr0;
    }

    protected Tcr4SmsData getTcr4ForSmsData(ExtractedTransaction transaction, Tcr0 tcr0)
            throws Exception
    {
        Tcr4SmsData tcr4forsmsdata = new Tcr4SmsData();
        tcr4forsmsdata.putField(Tcr4SmsData.Field.TRANSACTION_CODE, tcr0.getField(Tcr0.Field.TRANSACTION_CODE));
        tcr4forsmsdata.putField(Tcr4SmsData.Field.DEBIT_PRODUCT_CODE, VISA_DEBIT_PRODUCT_CODE);
        tcr4forsmsdata.putField(Tcr4SmsData.Field.ADJUSTMENT_PROCESSING_INDICATOR, " ");
        tcr4forsmsdata.putField(Tcr4SmsData.Field.MESSAGE_REASON_CODE, "    ");
        tcr4forsmsdata.putField(Tcr4SmsData.Field.SURCHARGE_AMOUNT, Long.toString(transaction.tran_tran_fee_rsp));
        tcr4forsmsdata.putField(Tcr4SmsData.Field.SURCHARGE_CARDHOLDER_BILLING, Long.toString(transaction.tran_tran_fee_rsp));
        return tcr4forsmsdata;
    }

    public boolean isDraftData(String trans_code)
    {
        return "05".equals(trans_code) || "06".equals(trans_code) || "25".equals(trans_code) || "26".equals(trans_code);
    }

    protected static String calcCheckDigit(String check_str)
    {
        int nb = check_str.length();
        byte check[] = Convert.getData(check_str);
        boolean even = false;
        int sum = 0;
        for(int i = nb - 1; i != -1; i--)
        {
            int digit = check[i] - 48;
            if(even)
            {
                sum += digit;
            } else
            {
                sum += (2 * digit) % 10 + digit / 5;
            }
            even = !even;
        }

        if(sum % 10 == 0)
        {
            return "0";
        } else
        {
            return Integer.toString(10 - sum % 10);
        }
    }

    private boolean dbCheckTransactionMsg(long post_tran_id, ExtractedTransaction transaction)
    {
        String query_msg = "{call visabase2_transaction_msg(?)}";
        java.sql.CallableStatement stmt = null;
        ResultSet rs = null;
        boolean include_transaction = false;
        try
        {
            stmt = connection.prepareCall(query_msg);
            stmt.setLong(1, post_tran_id);
            for(rs = stmt.executeQuery(); rs.next();)
            {
                int include = rs.getInt(1);
                transaction.tran_reversed = rs.getString(2);
                if(include == 1)
                {
                    include_transaction = true;
                    if(transaction.tran_reversed.equals(PARTIAL_REVERSAL))
                    {
                        transaction.new_tran_amount_rsp = rs.getLong(3);
                        transaction.new_tran_cash_rsp = rs.getLong(4);
                    }
                }
            }

        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Failed checking the transaction message with post_tran_id %1 ", new String[] {
                    Long.toString(post_tran_id)
            }, e);
        }
        finally
        {
            try
            {
                OfficeLib.cleanupStatement(connection, stmt, rs);
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportError("Failed cleaning up the database statement", e);
            }
        }
        return include_transaction;
    }

    protected Tcr5 getTcr5(ExtractedTransaction transaction, IccRequest icc_request, BatchInformation batch_info, boolean cps_enabled)
            throws Exception
    {
        Tcr5 tcr5 = new Tcr5();
        boolean return_tcr5 = true;
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        BaseII base2 = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            base2 = getBaseII(struct_data);
            adj_comp = getAdjustmentComponent(struct_data);
        }
        String base_i_xml = null;
        if(transaction.structured_data_rsp != null)
        {
            StructuredData struct_data_rsp = new StructuredData();
            try
            {
                struct_data_rsp.fromMsgString(transaction.structured_data_rsp);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_id)
                });
            }
            base_i_xml = struct_data_rsp.get("BaseI");
        }
        BaseI base_i = null;
        String card_level_results = null;
        if(base_i_xml != null)
        {
            base_i = (BaseI)ObjectFactory.createObjectFromString(base_i_xml);
            card_level_results = base_i.getCardLevelResults();
        }
        if(card_level_results != null)
        {
            tcr5.putField(Tcr5.Field.PRODUCT_ID, card_level_results);
        } else
        {
            tcr5.putField(Tcr5.Field.PRODUCT_ID, "  ");
        }
        String tran_type = transaction.tran_type;
        if((transaction.representment || isStandaloneReversal(transaction)) && adj_comp != null && adj_comp.getProcessingCode() != null)
        {
            String proc_code_string = adj_comp.getProcessingCode();
            ProcessingCode proc_code = new ProcessingCode(proc_code_string);
            tran_type = proc_code.getTranType();
        }
        String transaction_code = (String)tran_type_table.get(tran_type);
        if(transaction.reversal || transaction.ext_tran_type != null && transaction.ext_tran_type.equals("4111"))
        {
            int tran_code_int = Integer.parseInt(transaction_code);
            transaction_code = Convert.resize(Integer.toString(tran_code_int += 20), 2, '0', false);
        }
        String pan_entry_mode = transaction.pos_entry_mode.substring(0, 2);
        boolean is_icc_transaction = false;
        if(pan_entry_mode.equals("05") || pan_entry_mode.equals("07") || pan_entry_mode.equals("95"))
        {
            is_icc_transaction = true;
        }
        if(!tcr5_for_icc)
        {
            if(!cps_enabled && !"07".equals(transaction_code))
            {
                return_tcr5 = false;
            }
        } else
        if(!is_icc_transaction && transaction.card_verification_result == null)
        {
            return_tcr5 = false;
        }
        if(extract_empty_tcr5_for_TC05_TC06 && isDraftData(transaction_code))
        {
            return_tcr5 = true;
        }
        tcr5.putField(Tcr5.Field.TRANSACTION_CODE, transaction_code);
        if(struct_data != null)
        {
            String code_qualifier_str = struct_data.get(STRUCTURED_DATA_FIELD_NAME);
            if(code_qualifier_str != null)
            {
                tcr5.putField(Tcr5.Field.CODE_QUALIFIER, code_qualifier_str);
            }
        }
        String transaction_id = Convert.resize("", 15, _0_ZERO_PAD_CHAR, true);
        String auth_resp_code = "  ";
        String validation_code = "    ";
        String market_auth_data = " ";
        if(transaction.retention_data != null)
        {
            if(transaction.retention_data.isFieldSet("Transaction id"))
            {
                transaction_id = transaction.retention_data.getField("Transaction id");
            }
            if(transaction.retention_data.isFieldSet("Response code"))
            {
                auth_resp_code = transaction.retention_data.getField("Response code");
            }
            if(transaction.retention_data.isFieldSet("Validation code"))
            {
                validation_code = transaction.retention_data.getField("Validation code");
            }
            if(transaction.retention_data.isFieldSet("Market specific id"))
            {
                market_auth_data = transaction.retention_data.getField("Market specific id");
            }
        }
        if(transaction.representment && base2 != null)
        {
            if(base2.getTranId() != null)
            {
                transaction_id = base2.getTranId();
            }
            if(base2.getMultiClearingSeqNr() != null)
            {
                String multi_clearing_seq_nr = base2.getMultiClearingSeqNr();
                tcr5.putField(Tcr5.Field.MULTIPLE_CLEARING_SEQ, multi_clearing_seq_nr);
            }
            if(base2.getAddDataIndicator() != null)
            {
                String add_data_indicator = base2.getAddDataIndicator();
                tcr5.putField(Tcr5.Field.ADD_DATA_INDICATOR, add_data_indicator);
            }
        }
        if(icc_request != null)
        {
            String resp_code = constructAuthRspCodeICC(transaction, icc_request);
            if(resp_code != null && resp_code.length() != 0)
            {
                auth_resp_code = resp_code;
            }
        }
        if(cps_enabled && transaction_code.substring(1).equals("6"))
        {
            auth_resp_code = "  ";
            validation_code = "    ";
        }
        tcr5.putField(Tcr5.Field.TRANSACTION_ID, transaction_id);
        tcr5.putField(Tcr5.Field.AUTH_RESP_CODE, auth_resp_code);
        tcr5.putField(Tcr5.Field.VALIDATION_CODE, validation_code);
        tcr5.putField(Tcr5.Field.MARKET_AUTH_DATA, market_auth_data);
        GenericAmount approve_amount = constructApprovedAmount(transaction, adj_comp);
        if(cps_enabled && transaction_code.substring(1).equals("6"))
        {
            tcr5.putField(Tcr5.Field.AUTHORIZATION_AMOUNT, "000000000000");
        } else
        {
            tcr5.putField(Tcr5.Field.AUTHORIZATION_AMOUNT, Long.toString(approve_amount.getAmount()));
        }
        if(cps_enabled)
        {
            tcr5.putField(Tcr5.Field.TOTAL_AUTH_AMOUNT, "000000000000");
        } else
        {
            tcr5.putField(Tcr5.Field.TOTAL_AUTH_AMOUNT, Long.toString(approve_amount.getAmount()));
        }
        if(cps_enabled && transaction_code.substring(1).equals("6"))
        {
            tcr5.putField(Tcr5.Field.AUTH_CURRENCY_CODE, "   ");
        } else
        {
            tcr5.putField(Tcr5.Field.AUTH_CURRENCY_CODE, approve_amount.getCurrency());
        }
        if(transaction.card_verification_result != null && !transaction.card_verification_result.equalsIgnoreCase("Y") && !isDisbursementTransactionCode(transaction_code))
        {
            debugPrintln("TCR 5, CVV2 Value Insert " + transaction.card_verification_result);
            tcr5.putField(Tcr5.Field.CVV2_RESULT_VALUE, transaction.card_verification_result);
        } else
        {
            tcr5.putField(Tcr5.Field.CVV2_RESULT_VALUE, " ");
        }
        boolean is_MVV_set = false;
        boolean is_DCC_set = false;
        is_MVV_set = constructMVVToInterchange(transaction, tcr5, cps_enabled);
        is_DCC_set = constructDCCIndicator(transaction, tcr5, struct_data);
        if(is_MVV_set || is_DCC_set)
        {
            return_tcr5 = true;
        }
        if(!return_tcr5)
        {
            return null;
        } else
        {
            return tcr5;
        }
    }

    private void getFileSettleEntities(int session_id, String output_directory)
            throws Exception
    {
        FileInformation file_info = null;
        Enumeration settle_entities = null;
        SettleEntity settle_entity = null;
        String session_id_string = (new Integer(session_id)).toString();
        PrintWriter settle_file = null;
        PrintWriter log_file = null;
        String tc90_record = "";
        debugPrintln("getFileSettleEntities[" + session_id + "," + output_directory + "]");
        for(int i = 0; i < node_names.length; i++)
        {
            String sink_node_name = node_names[i];
            settle_entities = dbGetSettleEntities(sink_node_name);
            while(settle_entities.hasMoreElements())
            {
                settle_entity = (SettleEntity)settle_entities.nextElement();
                debugPrintln("\tEntity ID: " + settle_entity.settle_entity_id + "\tAcquiring Institution ID: " + settle_entity.acquiring_inst_id_code);
                String file_name;
                String settle_entity_str;
                if(node_granularity)
                {
                    file_name = "base2.Zanaco.imp."+ Extract.getProcessDateTime()+"."+Convert.resize(session_id_string,3,'0',false);
                    settle_entity_str = String.valueOf(settle_entity.settle_entity_id);
                } else
                {
                    if(settle_entity.acquiring_inst_id_code.length() == 0)
                    {
                        continue;
                    }
                    file_name = sink_node_name + "_" + settle_entity.acquiring_inst_id_code + "_" + session_id_string;
                    settle_entity_str = sink_node_name.toUpperCase() + "_" + settle_entity.acquiring_inst_id_code;
                }
                if(!settle_entity_lookup.containsKey(settle_entity_str))
                {
                    try
                    {
                        settle_file = new PrintWriter(new BufferedWriter(new FileWriter(output_directory + file_name + ".temp")));
                        log_file = new PrintWriter(new BufferedWriter(new FileWriter(output_directory + file_name + ".log")));
                    }
                    catch(FileNotFoundException f)
                    {
                        debugPrintln("Logging NO_SUCH_DIRECTORY: '" + output_directory + file_name + "'", true);
                        debugPrintln(" Causal exception: " + f, true);
                        VisaBase2ExtractLog.reportError("Output directory does not exist: %1", new String[] {
                                output_directory
                        });
                    }
                    catch(Throwable e)
                    {
                        debugPrintln("Logging ERROR_CREATING_OUTPUT: '" + output_directory + file_name + "'", true);
                        debugPrintln(" Causal exception: " + e, true);
                        VisaBase2ExtractLog.reportError("Process unable to create output: %1", new String[] {
                                output_directory + file_name
                        }, e);
                    }
                    file_info = newFileInformation(output_directory, file_name, session_id, settle_file, log_file, new BatchInformation());
                    file_info.sink_node_name = sink_node_name;
                    debugPrintln("\tFile name: " + file_name);
                    final_file_name = file_name + ".CTF";
                    log_file.println("Final File Name: " + file_location+"\\"+final_file_name);

                    log_file.println("Interface: VisaBase12");
                    log_file.println("\tSink node: " + sink_node_name);
                    int file_id = session_id;
                    if(increment_file_id)
                    {
                        if(file_header_last_digit > 9)
                        {
                            file_header_last_digit = 0;
                        }
                        String file_id_string = Convert.resize(String.valueOf(session_id), 2, '0', false) + file_header_last_digit++;
                        file_id = Integer.parseInt(file_id_string);
                    }
                    try
                    {
                        tc90_record = generateTc90(file_id, test, security_code, file_info);
                    }
                    catch(Exception e)
                    {
                        VisaBase2ExtractLog.reportError("Failed generating a control record for transaction code %1", new String[] {
                                "tc90"
                        }, e);
                    }
                    settle_file.print(tc90_record+getFileLineFeed(file_format));
                    settle_entity_lookup.put(settle_entity_str, file_info);
                    debugPrintln("\tsettle_entity_lookup inserted for settle entity: " + settle_entity_str);
                    log_file.println("\t\tReconciliation Entity: " + settle_entity.settle_entity_id);
                }
            }
        }
    }

    protected FileInformation newFileInformation(String output_dir, String filename, int session_id, PrintWriter settle_file, PrintWriter log_file, BatchInformation batch_info)
    {
        return new FileInformation(output_dir + filename, session_id, settle_file, log_file, batch_info);
    }

    private void dbTransactionException(FileInformation file_info, ExtractedTransaction transaction, String fail_reason)
    {
        String query_tran_exception = "{call visabase2_tran_exceptions (?,?,?,?,?,?)}";
        java.sql.CallableStatement stmt = null;
        try
        {
            stmt = connection.prepareCall(query_tran_exception);
            stmt.setInt(1, file_info.extract_session_id);
            stmt.setLong(2, transaction.post_tran_id);
            stmt.setString(3, file_info.full_file_name);
            stmt.setInt(4, transaction.settle_entity_id);
            stmt.setInt(5, transaction.batch_nr);
            stmt.setString(6, fail_reason);
            stmt.executeUpdate();
        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Failed inserting a transaction exception record for the transaction with post_tr" +
                            "an_id %1"
                    , new String[] {
                            Long.toString(transaction.post_tran_cust_id)
                    }, e);
        }
        finally
        {
            try
            {
                OfficeLib.cleanupStatement(connection, stmt, null);
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportError("Failed cleaning up the database statement", e);
            }
        }
    }

    protected Tcr0ForFees getTcr0ForFees(FileInformation file_info, ExtractedTransaction transaction, IccRequest icc_request, boolean cps_enabled)
            throws Exception
    {
        Tcr0ForFees tcr0forfees = new Tcr0ForFees();
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        BaseII base2 = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            base2 = getBaseII(struct_data);
            adj_comp = getAdjustmentComponent(struct_data);
        }
        if("4105".equals(transaction.ext_tran_type))
        {
            tcr0forfees.putField(Tcr0ForFees.Field.TRANSACTION_CODE, "10");
        }
        if("4106".equals(transaction.ext_tran_type))
        {
            tcr0forfees.putField(Tcr0ForFees.Field.TRANSACTION_CODE, "20");
        }
        String pan = transaction.pan;
        if(pan == null)
        {
            pan = "0";
            if(adj_comp != null)
            {
                String receiving_inst_id = adj_comp.getReceivingInstIdCode();
                if(receiving_inst_id != null)
                {
                    if(receiving_inst_id.length() > 6)
                    {
                        receiving_inst_id = receiving_inst_id.substring(receiving_inst_id.length() - 6, receiving_inst_id.length());
                    }
                    tcr0forfees.putField(Tcr0ForFees.Field.DESTINATION_BIN, Convert.resize(receiving_inst_id, 6, '0', true));
                }
            }
        }
        tcr0forfees.putField(Tcr0ForFees.Field.ACCOUNT_NUMBER, Convert.resize(pan, 19, '0', true));
        if(transaction.message_reason_code != null)
        {
            tcr0forfees.putField(Tcr0ForFees.Field.REASON_CODE, transaction.message_reason_code);
        } else
        {
            VisaBase2ExtractLog.reportWarning("There is no message_reason_code for transaction with post_tran_id%1", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
            return null;
        }
        if("0100".equals(tcr0forfees.getField(Tcr0ForFees.Field.REASON_CODE)) || "0190".equals(Tcr0ForFees.Field.REASON_CODE))
        {
            tcr0forfees.putField(Tcr0ForFees.Field.COUNTRY_CODE, transaction.card_acceptor_name_loc.substring(38) + ' ');
        } else
        {
            tcr0forfees.putField(Tcr0ForFees.Field.COUNTRY_CODE, Convert.resize("", 3, ' ', true));
        }
        String event_date = transaction.datetime_tran_local.get("MMdd");
        tcr0forfees.putField(Tcr0ForFees.Field.EVENT_DATE, event_date);
        tcr0forfees.putField(Tcr0ForFees.Field.SETTLEMENT_FLAG, getNetSettlementFlag(tcr0forfees.getField(Tcr0ForFees.Field.ACCOUNT_NUMBER), tcr0forfees.getField(Tcr0ForFees.Field.SOURCE_CURRENCY_CODE)));
        tcr0forfees.putField(Tcr0ForFees.Field.SOURCE_AMOUNT, Long.toString(transaction.tran_amount_rsp));
        tcr0forfees.putField(Tcr0.Field.SOURCE_CURRENCY_CODE, transaction.tran_currency_code);
        String transaction_id = Convert.resize("", 15, '0', true);
        if(base2 != null && base2.getTranId() != null)
        {
            transaction_id = base2.getTranId();
        }
        tcr0forfees.putField(Tcr0ForFees.Field.TRANSACTION_IDENTIFIER, transaction_id);
        if(adj_comp != null)
        {
            String message_text = adj_comp.getMessageText();
            if(message_text != null)
            {
                if(message_text.length() < 71)
                {
                    tcr0forfees.putField(Tcr0ForFees.Field.TEXT, message_text);
                } else
                {
                    VisaBase2ExtractLog.reportWarning("Text Message too long in the message with post_tran_cust_id %1 ", new String[] {
                            Long.toString(transaction.post_tran_cust_id)
                    });
                    transaction.is_open = true;
                    return null;
                }
            } else
            {
                VisaBase2ExtractLog.reportWarning("No Text Message present in the message with post_tran_cust_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
                transaction.is_open = true;
                return null;
            }
        } else
        {
            VisaBase2ExtractLog.reportWarning("No Text Message present in the message with post_tran_cust_id %1 ", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
            return null;
        }
        String source_bin = transaction.acquiring_inst_id_code;
        if(adj_comp != null)
        {
            String role = adj_comp.getRole();
            if(role != null && "0".equals(role) && adj_comp.getForwardingInstIdCode() != null)
            {
                source_bin = adj_comp.getForwardingInstIdCode();
            }
        }
        if(source_bin == null)
        {
            VisaBase2ExtractLog.reportWarning("There is no valid source BIN for transaction with post_tran_id%1", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
            return null;
        }
        file_info.batch_info.file_source_amount += transaction.tran_amount_rsp;
        file_info.batch_info.batch_source_amount += transaction.tran_amount_rsp;
        if(source_bin.length() > 6)
        {
            source_bin = source_bin.substring(source_bin.length() - 6, source_bin.length());
        }
        tcr0forfees.putField(Tcr0ForFees.Field.SOURCE_BIN, Convert.resize(source_bin, 6, '0', true));
        return tcr0forfees;
    }

    protected Tcr7 getTcr7(ExtractedTransaction transaction, IccRequest icc_request)
            throws Exception
    {
        if(icc_request == null)
        {
            return null;
        }
        Tcr7 tcr7 = new Tcr7();
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        BaseII base2 = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            base2 = getBaseII(struct_data);
            adj_comp = getAdjustmentComponent(struct_data);
        }
        String tran_type = transaction.tran_type;
        if((transaction.representment || isStandaloneReversal(transaction)) && adj_comp != null && adj_comp.getProcessingCode() != null)
        {
            String proc_code_string = adj_comp.getProcessingCode();
            ProcessingCode proc_code = new ProcessingCode(proc_code_string);
            tran_type = proc_code.getTranType();
        }
        String transaction_code = (String)tran_type_table.get(tran_type);
        if(transaction.reversal)
        {
            int tran_code_int = Integer.parseInt(transaction_code);
            transaction_code = Convert.resize(Integer.toString(tran_code_int += 20), 2, '0', false);
        }
        tcr7.putField(Tcr7.Field.TRANSACTION_CODE, transaction_code);
        String icc_tran_type = icc_request.getTransactionType();
        if(icc_tran_type == null || icc_tran_type.length() != 2)
        {
            icc_tran_type = "00";
        }
        tcr7.putField(Tcr7.Field.TRANSACTION_TYPE, icc_tran_type);
        String card_seq_nr = transaction.card_seq_nr;
        if(card_seq_nr == null || card_seq_nr.length() != 3)
        {
            card_seq_nr = "000";
        }
        tcr7.putField(Tcr7.Field.CARD_SEQUENCE_NUMBER, card_seq_nr);
        String terminal_tran_date = icc_request.getTransactionDate();
        if(terminal_tran_date == null || terminal_tran_date.length() != 6)
        {
            terminal_tran_date = Convert.resize("", 6, '0', true);
        }
        tcr7.putField(Tcr7.Field.TERMINAL_TRANSACTION_DATE, terminal_tran_date);
        String terminal_capability_profile = icc_request.getTerminalCapabilities();
        if(terminal_capability_profile == null || terminal_capability_profile.length() != 6)
        {
            terminal_capability_profile = Convert.resize("", 6, '0', true);
        }
        tcr7.putField(Tcr7.Field.TERMINAL_CAPABILITY_PROFILE, terminal_capability_profile);
        String terminal_country_code = icc_request.getTerminalCountryCode();
        if(terminal_country_code == null || terminal_country_code.length() != 3)
        {
            terminal_country_code = "000";
        }
        tcr7.putField(Tcr7.Field.TERMINAL_COUNTRY_CODE, terminal_country_code);
        String unpredictable_number = icc_request.getUnpredictableNumber();
        if(unpredictable_number == null || unpredictable_number.length() != 8)
        {
            unpredictable_number = Convert.resize("", 8, '0', true);
        }
        tcr7.putField(Tcr7.Field.UNPREDICTABLE_NUMBER, unpredictable_number);
        String application_transaction_counter = icc_request.getApplicationTransactionCounter();
        if(application_transaction_counter == null || application_transaction_counter.length() != 4)
        {
            application_transaction_counter = "0000";
        }
        tcr7.putField(Tcr7.Field.APPLICATION_TRANSACTION_COUNTER, application_transaction_counter);
        String application_interchange_profile = icc_request.getApplicationInterchangeProfile();
        if(application_interchange_profile == null || application_interchange_profile.length() != 4)
        {
            application_interchange_profile = "0000";
        }
        tcr7.putField(Tcr7.Field.APPLICATION_INTERCHANGE_PROFILE, application_interchange_profile);
        String cryptogram = icc_request.getCryptogram();
        if(cryptogram == null || cryptogram.length() != 16)
        {
            cryptogram = Convert.resize("", 16, '0', true);
        }
        tcr7.putField(Tcr7.Field.CRYPTOGRAM, cryptogram);
        String iad = icc_request.getIssuerApplicationData();
        int vdd_length = 0;
        if(iad != null)
        {
            if(iad.length() >= 4)
            {
                tcr7.putField(Tcr7.Field.DERIVATION_KEY_INDEX, iad.substring(2, 4));
            }
            if(iad.length() >= 6)
            {
                tcr7.putField(Tcr7.Field.CRYPTOGRAM_VERSION, iad.substring(4, 6));
            }
            if(iad.length() >= 8)
            {
                int card_verification_length_end_pos = 14;
                if(iad.length() < 14)
                {
                    card_verification_length_end_pos = iad.length();
                }
                tcr7.putField(Tcr7.Field.CARD_VERIFICATION_RESULTS, Convert.resize(iad.substring(6, card_verification_length_end_pos), 8, '0', true));
            }
            if(iad.length() >= 16)
            {
                tcr7.putField(Tcr7.Field.IAD_BYTE_8, iad.substring(14, 16));
            }
            if(iad.length() >= 32)
            {
                tcr7.putField(Tcr7.Field.IAD_BYTE_9_TO_16, iad.substring(16, 32));
            }
            if(iad.length() >= 2)
            {
                tcr7.putField(Tcr7.Field.IAD_BYTE_1, iad.substring(0, 2));
            }
            if(iad.length() >= 34)
            {
                tcr7.putField(Tcr7.Field.IAD_BYTE_17, iad.substring(32, 34));
            }
            if(iad.length() >= 64)
            {
                tcr7.putField(Tcr7.Field.IAD_BYTE_18_TO_32, iad.substring(34, 64));
            }
        }
        String term_verification_results = icc_request.getTerminalVerificationResult();
        if(term_verification_results == null || term_verification_results.length() != 10)
        {
            term_verification_results = Convert.resize("", 10, '0', true);
        }
        tcr7.putField(Tcr7.Field.TERMINAL_VERIFICATION_RESULTS, term_verification_results);
        String cryptogram_amount = icc_request.getAmountAuthorized();
        if(cryptogram_amount == null)
        {
            cryptogram_amount = Convert.resize("", 12, '0', true);
        }
        tcr7.putField(Tcr7.Field.CRYPTOGRAM_AMOUNT, cryptogram_amount);
        String form_factor_indicator = icc_request.getFormFactorIndicator();
        if(form_factor_indicator != null && form_factor_indicator.length() <= 8)
        {
            if(form_factor_indicator.length() < 8)
            {
                form_factor_indicator = Convert.resize(form_factor_indicator, 8, '0', false);
            }
            tcr7.putField(Tcr7.Field.FORM_FACTOR_INDICATOR, form_factor_indicator);
        }
        String issuer_script_results = icc_request.getIssuerScriptResults();
        if(issuer_script_results != null && issuer_script_results.length() >= 10)
        {
            String base2_issuer_script1_res = issuer_script_results.substring(2, 10) + issuer_script_results.substring(0, 2);
            tcr7.putField(Tcr7.Field.ISSUER_SCRIPT_1_RESULTS, base2_issuer_script1_res);
        }
        return tcr7;
    }

    private static long retrieveMinPostTranIdForSession(int session_id)
            throws Exception
    {
        long max_tran_nr_from_prev_session = 0L;
        java.sql.CallableStatement stmt = null;
        ResultSet rs = null;
        try
        {
            stmt = connection.prepareCall("{call visabase2_get_session_min_post_tran_id(?)}");
            stmt.setInt(1, session_id);
            rs = stmt.executeQuery();
            if(rs.next())
            {
                max_tran_nr_from_prev_session = rs.getLong(1);
            }
        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Error retrieving the minimum post_tran id for this session", e);
        }
        finally
        {
            try
            {
                OfficeLib.cleanupStatement(connection, stmt, rs);
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportError("Failed cleaning up the database statement", e);
            }
        }
        debugPrintln("retrieveMinPostTranIdForSession = " + max_tran_nr_from_prev_session);
        return max_tran_nr_from_prev_session;
    }

    public void init(String user_parameters[], String output_directory, IExtractInfo info) throws Exception
    {
        char map_file_flag = 'S';
        char reimbursement_attribute_flag = 'R';
        char processing_bin_flag = 'B';
        char record_number_flag = 'T';
        char hidden_parameter_flag = 'H';
        char icc_tcr5_flag = 'I';
        String linked_resp_code_flag = "LRSP";
        String debug_flag = "debug";
        file_location = output_directory;
        String decline_reversals_flag = "dr";
        String increment_file_id_flag = "FID";
        String return_TCR5_flag = "E";
        extract_session_id = info.getSessionId();
        if (info.getNodeNameListMethod() != 30)
        {
            VisaBase2ExtractLog.reportError("Invalid Node Name Method. The extract entity must be configured with the" +
                    " Free Li" + "st Of Node Names method.");
        }
        if (info.getSourceNodeNames().length != 0)
        {
            VisaBase2ExtractLog.reportError("Invalid nodes specified. Source nodes may not be specified.");
        }
        node_names = info.getSinkNodeNames();
        connection = OfficeLib.getOfficeConnection();
        initCurrencyDecimalHashtable();
        if (user_parameters == null || user_parameters.length < 9)
        {
            int num_parameters = user_parameters != null ? user_parameters.length : 0;
            VisaBase2ExtractLog.reportError("Missing user parameters. Expected %1. Received %2", new String[]{"8",
                    Integer.toString(num_parameters)});
        } else
        {
            file_format = user_parameters[0];
            //source_node_name = ;
            // split comma separated source_node_name into selected source node names
            selected_source_nodes = splitParams(user_parameters[1]);
            terminal_type = getTerminalType(user_parameters[2]);
            security_code = user_parameters[3];
            test_string = user_parameters[4].trim();
            if (!Validator.isValidN(test_string) || Integer.parseInt(test_string) != 1 && Integer.parseInt
                    (test_string) != 0)
            {
                VisaBase2ExtractLog.reportError("Invalid User parameter: %1. Expected %2. Received %3", new
                        String[]{"<test>", "0 or 1", test_string});
            }
            if (Integer.parseInt(test_string) == 1)
            {
                test = true;
            }
            else
            {
                test = false;
            }
            String closed_batches_only_string = user_parameters[5].trim();
            if (!Validator.isValidN(closed_batches_only_string) || Integer.parseInt(closed_batches_only_string) != 1
                    && Integer.parseInt(closed_batches_only_string) != 0)
            {
                VisaBase2ExtractLog.reportError("Invalid User parameter: %1. Expected %2. Received %3", new
                        String[]{"<closed_batches_only>", "0 or 1", closed_batches_only_string});
            }
            if (Integer.parseInt(closed_batches_only_string) == 1)
            {
                closed_batches_only = true;
            } else
            {
                closed_batches_only = false;
            }
            String file_granularity = user_parameters[6];
            if (!Validator.isValidN(file_granularity) || Integer.parseInt(file_granularity) != 1 && Integer.parseInt
                    (file_granularity) != 0)
            {
                VisaBase2ExtractLog.reportError("Invalid User parameter: %1. Expected %2. Received %3", new
                        String[]{"<file_granularity>", "0 or 1", file_granularity.toString()});
            }
            if (Integer.parseInt(file_granularity) == 1)
            {
                node_granularity = true;
            } else
            {
                node_granularity = false;
            }
            String use_transaction_date_string = user_parameters[7];
            if (!Validator.isValidN(use_transaction_date_string) || Integer.parseInt(use_transaction_date_string) !=
                    1 && Integer.parseInt(use_transaction_date_string) != 0)
            {
                VisaBase2ExtractLog.reportError("Invalid User parameter: %1. Expected %2. Received %3", new
                        String[]{"<use_transaction_date>", "0 or 1", use_transaction_date_string});
            }
            if (Integer.parseInt(use_transaction_date_string) == 1)
            {
                use_transaction_date = true;
            } else
            {
                use_transaction_date = false;
            }
            String crb_exception_file_validation_string = user_parameters[8];
            if (!Validator.isValidN(crb_exception_file_validation_string) || Integer.parseInt
                    (crb_exception_file_validation_string) != 1 && Integer.parseInt(crb_exception_file_validation_string) != 0)
            {
                VisaBase2ExtractLog.reportError("Invalid User parameter: %1. Expected %2. Received %3", new
                        String[]{"<crb_exception_file_validation>", "0 or 1", crb_exception_file_validation_string});
            }
            if (Integer.parseInt(crb_exception_file_validation_string) == 1)
            {
                crb_exception_file_validation = true;
            } else
            {
                crb_exception_file_validation = false;
            }
        }
        if (output_directory.equals(null) || output_directory.equals(""))
        {
            output_directory = "c:\\temp\\";
            VisaBase2ExtractLog.reportWarning("Output directory not specified. The output was created in %1",
                    output_directory);
        } else
        {
            File file = null;
            if (output_directory.charAt(output_directory.length() - 1) != File.separatorChar)
            {
                output_directory = output_directory.concat(File.separator);
            }
        }
        for (int pos = 9; pos < user_parameters.length; pos++)
        {
            if (user_parameters[pos].equalsIgnoreCase(debug_flag))
            {
                debug = true;
            } else if (user_parameters[pos].equalsIgnoreCase(linked_resp_code_flag))
            {
                linked_rsp_code = true;
            } else if (user_parameters[pos].equalsIgnoreCase(decline_reversals_flag))
            {
                process_declined_reversals = true;
            } else if (user_parameters[pos].equalsIgnoreCase(increment_file_id_flag))
            {
                increment_file_id = true;
            } else if (user_parameters[pos].startsWith("-c") && user_parameters[pos].length() > "-c".length())
            {
                compliancyLevel = CompliancyChecker.getCompliancyLevel(user_parameters[pos].substring("-c".length()));
                if (compliancyLevel == -1)
                {
                    VisaBase2ExtractLog.reportError("Invalid User parameter: %1. Expected %2. Received %3", new
                            String[]{"Compliancy level was incorrectly specified", "format: -cYYYYMM (Refer to user guide)",
                            user_parameters[pos]});
                }
            } else if (user_parameters[pos].equalsIgnoreCase(return_TCR5_flag))
            {
                extract_empty_tcr5_for_TC05_TC06 = true;
            } else if (user_parameters[pos].length() > 0)
            {
                if (Character.toUpperCase(user_parameters[pos].charAt(0)) == map_file_flag)
                {
                    String flags_string = user_parameters[pos].substring(1);
                    StringTokenizer flags = new StringTokenizer(flags_string, ",");
                    String filename = "";
                    String currency = "";
                    String fee = "";
                    try
                    {
                        filename = flags.nextToken();
                        currency = flags.nextToken();
                        fee = flags.nextToken();
                    }
                    catch (NoSuchElementException e)
                    {
                        VisaBase2ExtractLog.reportError("Not all the flags for option %1 has been supplied. The flags" +
                                " must be %2", new String[]{"National Net Settlement (S)", "<filename>,<currency>,<fee>"});
                    }
                    if (filename.length() == 0 || currency.length() != 3 || !Validator.isValidN(currency) ||
                            !Validator.isValidN(fee))
                    {
                        VisaBase2ExtractLog.reportError("The %1 user parameter was malformed. The value configured " +
                                "was %2.", new String[]{"National Net Settlement (S)", filename + "," + currency + "," + fee});
                    } else
                    {
                        nns_curr_code = currency;
                        nns_fee = Convert.resize(fee, 12, '0', false);
                        loadNetSettlementFile(filename);
                    }
                }
                if (Character.toUpperCase(user_parameters[pos].charAt(0)) == reimbursement_attribute_flag)
                {
                    String reimburse_attribute = user_parameters[pos].substring(1);
                    try
                    {
                        reimbursement_attribute = Class.forName(reimburse_attribute).newInstance();
                    }
                    catch (ClassNotFoundException e)
                    {
                        VisaBase2ExtractLog.reportError("Invalid User parameter: %1. Expected %2. Received %3", new
                                String[]{"<reimburse_attribute>", "a valid class name", reimburse_attribute}, e);
                    }
                    if (!(reimbursement_attribute instanceof IReimbursementAttribute) && !(reimbursement_attribute
                            instanceof IReimbursementAttributeTcrs))
                    {
                        VisaBase2ExtractLog.reportError("Invalid User parameter: %1. Expected %2. Received %3", new
                                String[]{"<reimburse_attribute>", "a instance of either IReimbursementAttribute or " +
                                "IReimbursementAttributeTcrs", reimburse_attribute});
                    }
                }
                if (Character.toUpperCase(user_parameters[pos].charAt(0)) == record_number_flag)
                {
                    if (user_parameters[pos].substring(1).length() > 3 || !Validator.isValidN(user_parameters[pos]
                            .substring(1)))
                    {
                        VisaBase2ExtractLog.reportError("The %1 user parameter was malformed. The value configured " +
                                "was %2.", new String[]{"Max number Tcrs in a batch (T)", user_parameters[pos].substring(1)});
                    } else
                    {
                        tcr_record_number = Integer.parseInt(user_parameters[pos].substring(1));
                    }
                }
                if (Character.toUpperCase(user_parameters[pos].charAt(0)) == hidden_parameter_flag)
                {
                    String temp = user_parameters[pos].toUpperCase();
                    if (temp.length() > 2 && temp.substring(1).startsWith("A"))
                    {
                        temp = temp.substring(2, temp.length());
                        if (temp.length() <= 11 && Validator.isValidN(temp))
                        {
                            set_acquiring_institution_id = temp;
                        } else
                        {
                            VisaBase2ExtractLog.reportError("The %1 user parameter was malformed. The value " +
                                    "configured was %2.", new String[]{"Acquiring Instituion ID (HA)", user_parameters[pos]});
                        }
                    }
                }
                if (Character.toUpperCase(user_parameters[pos].charAt(0)) == processing_bin_flag)
                {
                    if (user_parameters[pos].substring(1).length() != 6 || !Validator.isValidN(user_parameters[pos]
                            .substring(1)))
                    {
                        VisaBase2ExtractLog.reportError("The %1 user parameter was malformed. The value configured " +
                                "was %2.", new String[]{"Processing BIN (B)", user_parameters[pos].substring(1)});
                    } else
                    {
                        processing_bin = user_parameters[pos].substring(1);
                    }
                }
                if (Character.toUpperCase(user_parameters[pos].charAt(0)) == icc_tcr5_flag)
                {
                    tcr5_for_icc = true;
                }
                if (user_parameters[pos].length() >= 2 && user_parameters[pos].substring(0, 2).equalsIgnoreCase
                        (FORMAT_CODE_FLAG))
                {
                    if (user_parameters[pos].substring(2).length() != 1 || !Validator.isValidN(user_parameters[pos]
                            .substring(2)))
                    {
                        VisaBase2ExtractLog.reportError("The %1 user parameter was malformed. The value configured " +
                                "was %2.", new String[]{"Format Code (FC)", user_parameters[pos].substring(2)});
                    } else
                    {
                        use_format_code = true;
                        format_code = user_parameters[pos].substring(2);
                    }
                }
                if (user_parameters[pos].length() >= 3 && user_parameters[pos].substring(0, 3).equalsIgnoreCase("CPS"))
                {
                    String value = user_parameters[pos].substring(3);
                    if (!"0".equals(value) && !"1".equals(value))
                    {
                        VisaBase2ExtractLog.reportError("Invalid User parameter: %1. Expected %2. Received %3", new
                                String[]{"<CPS>", "0 or 1", value});
                    }
                    is_cps_participating_country = "1".equals(value);
                }
            }
        }

        if (reimbursement_attribute == null)
        {
            reimbursement_attribute = new StandardRA();
        }
        if (processing_bin == null)
        {
            processing_bin = Convert.resize("", 6, ' ', true);
        }
        if (debug)
        {
            String dump_file_name = "";
            try
            {
                dump_file_name = output_directory + "VisaBase2Extract.log";
                dump_file_output_stream = new FileOutputStream(dump_file_name);
            }
            catch (FileNotFoundException f)
            {
                VisaBase2ExtractLog.reportError("Output directory does not exist: %1", new String[]{output_directory});
            }
            catch (Throwable e)
            {
                VisaBase2ExtractLog.reportError("Process unable to create output: %1", new String[]{dump_file_name}, e);
            }
            debugPrintln("Debugging Log File for VisaBase2 Extract\n");
        }
        min_post_tran_id_for_session = retrieveMinPostTranIdForSession(info.getSessionId());
        getFileSettleEntities(extract_session_id, output_directory);
        collection_only_flag_checker = new CollectionOnlyFlagChecker(dbLoadCollectionOnlyTable(), node_names);
        initMVVHashTable();

        //initialize the creation date for the file
        DateTime date_time = new DateTime();
        report_creation_date = date_time.get("dd-MMMM-yyyy");
    }

    protected GenericAmount constructApprovedAmount(ExtractedTransaction transaction, AdjustmentComponent adj_comp)
            throws SQLException
    {
        GenericAmount approved_amt = new GenericAmount();
        if(transaction.reversal && isStandaloneReversal(transaction) && adj_comp != null && adj_comp.getOriginalAmounts() != null && adj_comp.getOriginalAmounts().getTranAmount() != null)
        {
            approved_amt.setAmount(Long.parseLong(adj_comp.getOriginalAmounts().getTranAmount()));
            approved_amt.setCurrency(adj_comp.getOriginalAmounts().getTranCurrencyCode());
        } else
        {
            String query_msg = "{call visabase2_get_auth_amount(?)}";
            java.sql.CallableStatement stmt = null;
            ResultSet rs = null;
            try
            {
                stmt = connection.prepareCall(query_msg);
                stmt.setLong(1, transaction.post_tran_id);
                rs = stmt.executeQuery();
                if(rs.next())
                {
                    approved_amt.setAmount(Convert.fromStringToLong(rs.getString(1)));
                    approved_amt.setCurrency(rs.getString(2));
                } else
                {
                    approved_amt.setAmount(transaction.tran_amount_rsp);
                    approved_amt.setCurrency(transaction.tran_currency_code);
                }
            }
            finally
            {
                OfficeLib.cleanupStatement(connection, stmt, rs);
            }
        }
        return approved_amt;
    }

    public BaseII getBaseII(StructuredData struct_data)
    {
        BaseII base2 = null;
        String xml_string = struct_data.get("BaseII");
        if(xml_string != null)
        {
            base2 = (BaseII)postilion.office.extract.visabase2.xml.BaseII.ObjectFactory.createObjectFromString(xml_string);
        }
        return base2;
    }

    protected String generateTc92(BatchInformation batch_information, FileInformation file_info)
            throws Exception
    {
        batch_information.file_tcr_counter++;
        String tc92 = "9200" + Convert.resize("", 6, '0', true) + Convert.resize("", 5, '0', true) + Convert.resize("", 15, '0', true) + Convert.resize(String.valueOf(batch_information.file_monetary_tc_counter), 12, '0', false) + Convert.resize(String.valueOf(batch_information.batch_no_counter - 1L), 6, '0', false) + Convert.resize(String.valueOf(batch_information.file_tcr_counter), 12, '0', false) + Convert.resize("", 6, '0', true) + Convert.resize("", 8, ' ', true) + Convert.resize(String.valueOf(batch_information.file_tc_counter + batch_information.batch_no_counter), 9, '0', false) + Convert.resize("", 18, '0', true) + Convert.resize(String.valueOf(batch_information.file_source_amount), 15, '0', false) + Convert.resize("", 15, '0', true) + Convert.resize("", 15, '0', true) + Convert.resize("", 15, '0', true);
        return tc92;
    }

    protected void printTransaction(FileInformation file_info, ExtractedTransaction transaction)
            throws IOException, XPostilion
    {
        boolean cps_enabled = false;
        IccRequest icc_request = null;
        String tc91_record = "";
        Tcr7 tcr7 = null;
        Tcr0 tcr0 = null;
        Tcr0ForTc50 tcr0fortc50 = null;
        Tcr0ForTc52 tcr0fortc52 = null;
        Tcr0ForFees tcr0forfees = null;
        Tcr0ForFraud tcr0forfraud = null;
        Tcr2ForFraud tcr2forfraud = null;
        Tcr4SmsData tcr4forsmsdata = null;
        Tcr1 tcr1 = null;
        Tcr5 tcr5 = null;
        String sink_node_name = file_info.sink_node_name;
        String acquirer_id = transaction.acquiring_inst_id_code;
        Hashtable sink_node_bin_table = collection_only_flag_checker.getSinkNodeToBinTable();
        Vector sink_node_keys = toVector(sink_node_bin_table.keys());
        Hashtable acquirer_bin_table = collection_only_flag_checker.getAcquirerToBinTable();
        Vector acquirer_keys = toVector(acquirer_bin_table.keys());
        debugPrintln("printTransaction()");
        if(acquirer_id != null)
        {
            boolean collection_only = collection_only_flag_checker.isCollectionOnlyTransaction(sink_node_name, acquirer_id, transaction.pan);
            if((sink_node_keys.contains(sink_node_name.trim().toLowerCase()) || acquirer_keys.contains(acquirer_id.trim().toLowerCase())) && !collection_only)
            {
                debugPrintln("printTransaction() - return [a]");
                return;
            }
        }
        if(transaction.retention_data != null && transaction.retention_data.isFieldSet("Auth char indicator"))
        {
            String ind = transaction.retention_data.getField("Auth char indicator");
            if(!"N".equals(ind) && !"T".equals(ind))
            {
                cps_enabled = true;
            }
        }
        if(file_info.batch_info.batch_tcr_counter >= (long)(tcr_record_number - 4) || transaction.batch_nr > file_info.batch_info.last_transaction_batch_nr && file_info.batch_info.batch_tcr_counter > 0L)
        {
            try
            {
                tc91_record = generateTc91(file_info.batch_info, transaction.batch_nr, file_info);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed generating a control record for transaction code %1", new String[] {
                        "tc91"
                }, e);
            }
            file_info.settle_file.print(tc91_record+getFileLineFeed(file_format));
            closeBatch(file_info, transaction.batch_nr);
        }
        if(transaction.icc_data_req != null)
        {
            IccData icc_data = (IccData)postilion.core.xml.iccdata.ObjectFactory.createObjectFromString(transaction.icc_data_req);
            if(icc_data != null)
            {
                icc_request = icc_data.getIccRequest();
            }
        }
        if(transaction.message_type.equals("0620") && "4107".equals(transaction.ext_tran_type))
        {
            try
            {
                tcr0fortc50 = getTcr0ForTc50(file_info, transaction, icc_request, cps_enabled);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed generating a transaction code record %1", new String[] {
                        "TCR0FORTC50"
                }, e);
            }
            if(tcr0fortc50 == null || transaction.is_open)
            {
                debugPrintln("printTransaction() - return [b]");
                return;
            }
        } else
        if(transaction.message_type.equals("0620") && "4100".equals(transaction.ext_tran_type))
        {
            try
            {
                tcr0fortc52 = getTcr0ForTc52(file_info, transaction, icc_request, cps_enabled);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed generating a transaction code record %1", new String[] {
                        "TCR0FORTC52"
                }, e);
            }
            if(tcr0fortc52 == null || transaction.is_open)
            {
                debugPrintln("printTransaction() - return [c]");
                return;
            }
        } else
        if(transaction.message_type.equals("0620") && ("4105".equals(transaction.ext_tran_type) || "4106".equals(transaction.ext_tran_type)))
        {
            try
            {
                tcr0forfees = getTcr0ForFees(file_info, transaction, icc_request, cps_enabled);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed generating a transaction code record %1", new String[] {
                        "TCR0FORFEES"
                }, e);
            }
            if(tcr0forfees == null || transaction.is_open)
            {
                debugPrintln("printTransaction() - return [d]");
                return;
            }
        } else
        if(transaction.message_type.equals("0620") && "4002".equals(transaction.ext_tran_type))
        {
            try
            {
                tcr0forfraud = getTcr0ForFraud(file_info, transaction, icc_request, cps_enabled);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed generating a transaction code record %1", new String[] {
                        "TCR0FORFRAUD"
                }, e);
            }
            if(tcr0forfraud == null || transaction.is_open)
            {
                debugPrintln("printTransaction() - return [e]");
                return;
            }
            try
            {
                tcr2forfraud = getTcr2ForFraud(file_info, transaction, cps_enabled, tcr0forfraud);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed generating a transaction code record %1", new String[] {
                        "TCR2"
                }, e);
            }
        } else
        {
            try
            {
                tcr0 = getTcr0(file_info, transaction, reimbursement_attribute, icc_request, cps_enabled);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed generating a transaction code record %1", new String[] {
                        "TCR0"
                }, e);
            }
            if(tcr0 == null)
            {
                debugPrintln("printTransaction() - return [f]");
                return;
            }
            try
            {
                tcr1 = getTcr1(transaction, cps_enabled, tcr0);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed generating a transaction code record %1", new String[] {
                        "TCR1"
                }, e);
            }
            try
            {
                if(isAtmTransaction(transaction))
                {
                    tcr4forsmsdata = getTcr4ForSmsData(transaction, tcr0);
                }
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed generating a transaction code record %1", new String[] {
                        "TCR4"
                }, e);
            }
            try
            {
                if(isChargebackOrChargebackReversal(transaction))
                {
                    tcr5 = getTcr5ForChargeback(transaction, icc_request, file_info.batch_info, cps_enabled);
                } else
                {
                    tcr5 = getTcr5(transaction, icc_request, file_info.batch_info, cps_enabled);
                }
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed generating a transaction code record %1", new String[] {
                        "TCR5"
                }, e);
            }
            tcr7 = null;
            if(icc_request != null)
            {
                try
                {
                    tcr7 = getTcr7(transaction, icc_request);
                }
                catch(Exception e)
                {
                    VisaBase2ExtractLog.reportError("Failed generating a transaction code record %1", new String[] {
                            "TCR7"
                    }, e);
                }
            }
            if(!isChargebackOrChargebackReversal(transaction) && (reimbursement_attribute instanceof IReimbursementAttributeTcrs))
            {
                IReimbursementAttributeTcrs i_reimbursement_attribute = (IReimbursementAttributeTcrs)reimbursement_attribute;
                tcr0.putField(Tcr0.Field.REIMBURSEMENT_ATTR, i_reimbursement_attribute.getReimbursementAttribute(tcr0, tcr1, tcr5, tcr7));
                calculated_ra = tcr0.getField(Tcr0.Field.REIMBURSEMENT_ATTR);
            }
        }
        byte tcr0_bytes[] = null;
        byte tcr0fortc50_bytes[] = null;
        byte tcr0fortc52_bytes[] = null;
        byte tcr0forfees_bytes[] = null;
        byte tcr0forfraud_bytes[] = null;
        byte tcr2forfraud_bytes[] = null;
        byte tcr1_bytes[] = null;
        byte tcr5_bytes[] = null;
        byte tcr7_bytes[] = null;
        byte tcr4forsmsdata_bytes[] = null;
        debugPrintln("Printing transaction to file");
        if(tcr0fortc50 != null)
        {
            try
            {
                tcr0fortc50_bytes = tcr0fortc50.toMsg();
            }
            catch(XPostilion x)
            {
                VisaBase2ExtractLog.reportError("Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                                "ntrecord associated with this is %3."
                        , new String[] {
                                "TCR0_FOR_TC50", "" + transaction.post_tran_id, x.getEventRecord().describe()
                        });
                debugPrintln("printTransaction() - return [g]");
                return;
            }
            file_info.batch_info.batch_tc_counter++;
            file_info.batch_info.file_tc_counter++;
            file_info.batch_info.batch_tcr_counter++;
            file_info.batch_info.file_tcr_counter++;
            file_info.settle_file.print(Convert.getString(tcr0fortc50_bytes)+getFileLineFeed(file_format));
        } else
        if(tcr0forfees != null)
        {
            try
            {
                tcr0forfees_bytes = tcr0forfees.toMsg();
            }
            catch(XPostilion x)
            {
                VisaBase2ExtractLog.reportError("Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                                "ntrecord associated with this is %3."
                        , new String[] {
                                "TCR0_FOR_TCFEES", "" + transaction.post_tran_id, x.getEventRecord().describe()
                        });
                debugPrintln("printTransaction() - return [h]");
                return;
            }
            file_info.batch_info.batch_monetary_tc_counter++;
            file_info.batch_info.file_monetary_tc_counter++;
            file_info.batch_info.batch_tc_counter++;
            file_info.batch_info.file_tc_counter++;
            file_info.batch_info.batch_tcr_counter++;
            file_info.batch_info.file_tcr_counter++;
            file_info.settle_file.print(Convert.getString(tcr0forfees_bytes)+getFileLineFeed(file_format));
        } else
        if(tcr0forfraud != null)
        {
            try
            {
                tcr0forfraud_bytes = tcr0forfraud.toMsg();
            }
            catch(XPostilion x)
            {
                VisaBase2ExtractLog.reportError("Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                                "ntrecord associated with this is %3."
                        , new String[] {
                                "TCR0_FOR_TCFRAUD", "" + transaction.post_tran_id, x.getEventRecord().describe()
                        });
                debugPrintln("printTransaction() - return [i]");
                return;
            }
            file_info.batch_info.batch_tc_counter++;
            file_info.batch_info.file_tc_counter++;
            file_info.batch_info.batch_tcr_counter++;
            file_info.batch_info.file_tcr_counter++;
            file_info.settle_file.print(Convert.getString(tcr0forfraud_bytes)+getFileLineFeed(file_format));
            if(tcr2forfraud != null)
            {
                try
                {
                    tcr2forfraud_bytes = tcr2forfraud.toMsg();
                    file_info.batch_info.batch_tcr_counter++;
                    file_info.batch_info.file_tcr_counter++;
                    file_info.settle_file.print(Convert.getString(tcr2forfraud_bytes)+getFileLineFeed(file_format));
                }
                catch(XPostilion x)
                {
                    VisaBase2ExtractLog.reportError("Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                                    "ntrecord associated with this is %3."
                            , new String[] {
                                    "TCR2", "" + transaction.post_tran_id, x.getEventRecord().describe()
                            });
                }
            }
        } else
        if(tcr0fortc52 != null)
        {
            try
            {
                tcr0fortc52_bytes = tcr0fortc52.toMsg();
            }
            catch(XPostilion x)
            {
                VisaBase2ExtractLog.reportError("Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                                "ntrecord associated with this is %3."
                        , new String[] {
                                "TCR0_FOR_TC52", "" + transaction.post_tran_id, x.getEventRecord().describe()
                        });
                debugPrintln("printTransaction() - return [j]");
                return;
            }
            file_info.batch_info.batch_tc_counter++;
            file_info.batch_info.file_tc_counter++;
            file_info.batch_info.batch_tcr_counter++;
            file_info.batch_info.file_tcr_counter++;
            file_info.settle_file.print(Convert.getString(tcr0fortc52_bytes)+getFileLineFeed(file_format));
        } else
        {
            try
            {
                tcr0_bytes = tcr0.toMsg();
            }
            catch(XPostilion x)
            {
                VisaBase2ExtractLog.reportError("Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                                "ntrecord associated with this is %3."
                        , new String[] {
                                "TCR0", "" + transaction.post_tran_id, x.getEventRecord().describe()
                        });
                debugPrintln("printTransaction() - return [k]");
                return;
            }
            try
            {
                tcr1_bytes = tcr1.toMsg();
            }
            catch(XPostilion x)
            {
                VisaBase2ExtractLog.reportError("Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                                "ntrecord associated with this is %3."
                        , new String[] {
                                "TCR1", "" + transaction.post_tran_id, x.getEventRecord().describe()
                        });
                debugPrintln("printTransaction() - return [l]");
                return;
            }
            file_info.batch_info.batch_monetary_tc_counter++;
            file_info.batch_info.file_monetary_tc_counter++;
            file_info.batch_info.batch_tc_counter++;
            file_info.batch_info.file_tc_counter++;
            file_info.batch_info.batch_tcr_counter += 2L;
            file_info.batch_info.file_tcr_counter += 2L;
            file_info.settle_file.print(Convert.getString(tcr0_bytes)+getFileLineFeed(file_format));
            file_info.settle_file.print(Convert.getString(tcr1_bytes)+getFileLineFeed(file_format));
        }
        if(tcr4forsmsdata != null)
        {
            try
            {
                tcr4forsmsdata_bytes = tcr4forsmsdata.toMsg();
                file_info.batch_info.batch_tcr_counter++;
                file_info.batch_info.file_tcr_counter++;
                file_info.settle_file.print(Convert.getString(tcr4forsmsdata_bytes)+getFileLineFeed(file_format));
            }
            catch(XPostilion x)
            {
                VisaBase2ExtractLog.reportError("Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                                "ntrecord associated with this is %3."
                        , new String[] {
                                "TCR4", "" + transaction.post_tran_id, x.getEventRecord().describe()
                        });
                debugPrintln("printTransaction() - return [m]");
                return;
            }
        }
        if(tcr5 != null)
        {
            try
            {
                tcr5_bytes = tcr5.toMsg();
                file_info.batch_info.batch_tcr_counter++;
                file_info.batch_info.file_tcr_counter++;
                file_info.settle_file.print(Convert.getString(tcr5_bytes)+getFileLineFeed(file_format));
            }
            catch(XPostilion x)
            {
                VisaBase2ExtractLog.reportError("Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                                "ntrecord associated with this is %3."
                        , new String[] {
                                "TCR5", "" + transaction.post_tran_id, x.getEventRecord().describe()
                        });
                debugPrintln("printTransaction() - return [n]");
                return;
            }
        }
        if(tcr7 != null)
        {
            try
            {
                tcr7_bytes = tcr7.toMsg();
                file_info.batch_info.batch_tcr_counter++;
                file_info.batch_info.file_tcr_counter++;
                file_info.settle_file.print(Convert.getString(tcr7_bytes)+getFileLineFeed(file_format));
            }
            catch(XPostilion x)
            {
                VisaBase2ExtractLog.reportError("Failed packing a TCR record %1 for the transaction with post_tran_id %2. The eve" +
                                "ntrecord associated with this is %3."
                        , new String[] {
                                "TCR7", "" + transaction.post_tran_id, x.getEventRecord().describe()
                        });
            }
        }
        file_info.batch_info.last_transaction_batch_nr = transaction.batch_nr;
        debugPrintln("printTransaction() - done()");
    }

    protected AdjustmentComponent getAdjustmentComponent(StructuredData struct_data)
    {
        AdjustmentComponent adj_comp = null;
        String xml_string = struct_data.get("AdjustmentComponent");
        if(xml_string != null)
        {
            adj_comp = (AdjustmentComponent)postilion.office.extract.visabase2.xml.AdjustmentComponent.ObjectFactory.createObjectFromString(xml_string);
        }
        return adj_comp;
    }

    private boolean isFullyReversedSourceLeg(long post_tran_cust_id)
            throws SQLException
    {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            String select = "select post_tran_cust_id from post_tran WITH (NOLOCK) where post_tran_cust_id = " + post_tran_cust_id + " and message_type in ('0200','0220') " + " and tran_postilion_originated = 0 " + " and tran_reversed = 2";
            debugPrintln("Checking if the source node leg of a transaction has been fully reversed, using " +
                    "the following query: "
            );
            debugPrintln(select);
            stmt = connection.prepareStatement(select);
            rs = stmt.executeQuery();
            if(rs.next())
            {
                boolean flag = true;
                return flag;
            }
            boolean flag1 = false;
            return flag1;
        }
        finally
        {
            OfficeLib.cleanupStatement(connection, stmt, rs);
        }
    }

    public boolean isDisbursementTransactionCode(String transaction_code)
    {
        return transaction_code.equals("07") || transaction_code.equals("17") || transaction_code.equals("27") || transaction_code.equals("37");
    }

    public static String getYDDD(String settlement_date)
    {
        DateTime date = new DateTime();
        int current_yyyy = Integer.parseInt(date.get("yyyy"));
        int current_mm = Integer.parseInt(date.get("MM"));
        int current_dd = Integer.parseInt(date.get("dd"));
        int settlement_mm = Integer.parseInt(settlement_date.substring(0, 2));
        int settlement_dd = Integer.parseInt(settlement_date.substring(2));
        int settlement_yyyy;
        if(settlement_mm > current_mm || settlement_mm == current_mm && settlement_dd > current_dd)
        {
            settlement_yyyy = current_yyyy - 1;
        } else
        {
            settlement_yyyy = current_yyyy;
        }
        Calendar calendar = new GregorianCalendar(settlement_yyyy, settlement_mm - 1, settlement_dd);
        java.util.Date settle_date = calendar.getTime();
        DateTime settlement_date_time = new DateTime(0, settle_date);
        String settlement_days = settlement_date_time.get("DDD");
        return Integer.toString(settlement_yyyy % 10) + settlement_days;
    }

    public int getOpenTranProcessingStrategy()
    {
        return 30;
    }

    protected void dbLogTranReference(long post_tran_id)
    {
        StructuredData extr_extended_data = new StructuredData();
        extr_extended_data.put(RA_KEY, calculated_ra);
        PreparedStatement stmt = null;
        boolean resultset_produced = false;
        ResultSet rs = null;
        debugPrintln("dbLogTranReference(" + post_tran_id + ")");
        try
        {
            stmt = connection.prepareCall("{call visabase2_update_post_tran_extract(?,?,?)}");
            stmt.setLong(1, post_tran_id);
            stmt.setString(2, acq_ref_nr);
            stmt.setString(3, extr_extended_data.toMsgString());
            resultset_produced = stmt.execute();
            JdbcUtils.commitAndClose(connection, stmt);
        }
        catch(SQLException sqle)
        {
            VisaBase2ExtractLog.reportError("Updating the post_tran_extract table failed for the transaction with post_tran_i" +
                            "d %1."
                    , sqle);
        }
        acq_ref_nr = null;
        calculated_ra = null;
    }

    protected Tcr0ForFraud getTcr0ForFraud(FileInformation file_info, ExtractedTransaction transaction, IccRequest icc_request, boolean cps_enabled)
            throws Exception
    {
        Tcr0ForFraud tcr0forfraud = new Tcr0ForFraud();
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        String prev_tran_nr = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_id)
                });
            }
            adj_comp = getAdjustmentComponent(struct_data);
        }
        if(adj_comp != null)
        {
            prev_tran_nr = adj_comp.getPreviousPostTranID();
        }
        String film_locator;
        if(prev_tran_nr != null)
        {
            film_locator = Convert.resize(prev_tran_nr, 11, '0', false);
        } else
        {
            VisaBase2ExtractLog.reportWarning("PreviousTranID is missing from structured data with post_tran_id %1 ", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
            return null;
        }
        String source_bin = transaction.acquiring_inst_id_code;
        if(source_bin == null)
        {
            VisaBase2ExtractLog.reportWarning("There is no valid source BIN for transaction with post_tran_id%1", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
            return null;
        }
        if(source_bin.length() > 6)
        {
            source_bin = source_bin.substring(source_bin.length() - 6, source_bin.length());
        }
        tcr0forfraud.putField(Tcr0ForFees.Field.SOURCE_BIN, Convert.resize(source_bin, 6, '0', true));
        String pan = transaction.pan;
        tcr0forfraud.putField(Tcr0ForFees.Field.ACCOUNT_NUMBER, Convert.resize(pan, 23, '0', true));
        if(transaction.sponsor_bank != null)
        {
            tcr0forfraud.putField(Tcr0ForFraud.Field.ACQUIRER_BUSINESS_ID, transaction.sponsor_bank);
        } else
        {
            tcr0forfraud.putField(Tcr0ForFraud.Field.ACQUIRER_BUSINESS_ID, Convert.resize("", 8, '0', true));
        }
        String purchase_date = transaction.datetime_tran_local.get("MMdd");
        tcr0forfraud.putField(Tcr0ForFraud.Field.PURCHASE_DATE, purchase_date);
        String name_location = transaction.card_acceptor_name_loc;
        tcr0forfraud.putField(Tcr0ForFraud.Field.MERCHANT_NAME, name_location.substring(0, 23) + "  ");
        tcr0forfraud.putField(Tcr0ForFraud.Field.MERCHANT_CITY, name_location.substring(23, 36));
        tcr0forfraud.putField(Tcr0ForFraud.Field.MERCHANT_COUNTRY_CODE, name_location.substring(38, 40) + " ");
        String merchant_type = transaction.merchant_type;
        if(merchant_type == null)
        {
            if("02".equals(transaction.pos_terminal_type))
            {
                merchant_type = _6011_MERCHANT_TYPE;
            } else
            {
                merchant_type = _5399_MERCHANT_TYPE;
            }
        }
        tcr0forfraud.putField(Tcr0ForFraud.Field.MERCHANT_CATEGORY_CODE, merchant_type);
        BaseII base2_0100 = null;
        dbGet0100data(transaction, prev_tran_nr);
        if(transaction.structured_data_0100 != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_0100);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Failed retrieving the structured data of the original 0100 message pertaining to" +
                                " fraud for %1 from the database"
                        , new String[] {
                                Long.toString(transaction.post_tran_id)
                        });
                transaction.is_open = true;
            }
            base2_0100 = getBaseII(struct_data);
        }
        cps_enabled = false;
        if(base2_0100 != null && base2_0100.getTranId() != null)
        {
            cps_enabled = true;
        }
        String state = "   ";
        String postal = Convert.resize("", 10, ' ', false);
        if(base2_0100 != null && base2_0100.getPOSGeoData() != null)
        {
            String pos_geographic_data = base2_0100.getPOSGeoData();
            String post_temp = pos_geographic_data.substring(3);
            int post = post_temp.length() - 5;
            if(post >= 0)
            {
                postal = post_temp.substring(post, post_temp.length());
            } else
            {
                postal = post_temp.substring(0, post_temp.length());
                postal = Convert.resize(postal, 5, '0', false);
            }
            state = pos_geographic_data.substring(0, 3);
        }
        if("US ".equals(tcr0forfraud.getField(Tcr0ForFraud.Field.MERCHANT_COUNTRY_CODE)) || "CA ".equals(tcr0forfraud.getField(Tcr0ForFraud.Field.MERCHANT_COUNTRY_CODE)))
        {
            tcr0forfraud.putField(Tcr0ForFraud.Field.MERCHANT_STATE, state);
        } else
        {
            tcr0forfraud.putField(Tcr0ForFraud.Field.MERCHANT_STATE, Convert.resize("", 3, ' ', false));
        }
        if("US ".equals(tcr0forfraud.getField(Tcr0ForFraud.Field.MERCHANT_COUNTRY_CODE)))
        {
            tcr0forfraud.putField(Tcr0ForFraud.Field.MERCHANT_POSTAL_CODE, Convert.resize(postal, 10, '0', true));
        } else
        {
            tcr0forfraud.putField(Tcr0ForFraud.Field.MERCHANT_POSTAL_CODE, Convert.resize(postal, 10, ' ', true));
        }
        tcr0forfraud.putField(Tcr0ForFraud.Field.FRAUD_AMOUNT, Long.toString(transaction.tran_amount_rsp));
        tcr0forfraud.putField(Tcr0ForFraud.Field.FRAUD_CURRENCY_CODE, transaction.tran_currency_code);
        if(transaction.expiry_date != null)
        {
            tcr0forfraud.putField(Tcr0ForFraud.Field.CARD_EXPIRATION_DATE, transaction.expiry_date);
        } else
        {
            tcr0forfraud.putField(Tcr0ForFraud.Field.CARD_EXPIRATION_DATE, "0000");
        }
        String yddd = getYDDD(transaction.datetime_tran_local.get("MMdd"));
        String format_code;
        if(cps_enabled)
        {
            format_code = "2";
        } else
        {
            format_code = "7";
        }
        String check_str = format_code + source_bin + yddd + film_locator;
        String check_digit = calcCheckDigit(check_str);
        tcr0forfraud.putField(Tcr0ForFraud.Field.ACQUIRER_REFERENCE_NUMBER, format_code + source_bin + yddd + film_locator + check_digit);
        return tcr0forfraud;
    }

    protected Tcr1 getTcr1(ExtractedTransaction transaction, boolean cps_enabled, Tcr0 tcr0)
            throws Exception
    {
        Tcr1 tcr1 = new Tcr1();
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        BaseII base2 = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            base2 = getBaseII(struct_data);
            adj_comp = getAdjustmentComponent(struct_data);
        }
        if(isChargebackOrChargebackReversal(transaction))
        {
            String tran_id = null;
            if(base2 != null)
            {
                tran_id = base2.getTranId();
            }
            if(tran_id != null)
            {
                cps_enabled = true;
            } else
            {
                cps_enabled = false;
            }
        }
        String tran_type = transaction.tran_type;
        if((isChargebackOrChargebackReversal(transaction) || transaction.representment || isStandaloneReversal(transaction)) && adj_comp != null && adj_comp.getProcessingCode() != null)
        {
            String proc_code_string = adj_comp.getProcessingCode();
            ProcessingCode proc_code = new ProcessingCode(proc_code_string);
            tran_type = proc_code.getTranType();
        }
        String transaction_code = (String)tran_type_table.get(tran_type);
        transaction_code = getTranType(transaction_code, transaction.ext_tran_type);
        if(transaction.reversal || transaction.ext_tran_type != null && transaction.ext_tran_type.equals("4111"))
        {
            int tran_code_int = Integer.parseInt(transaction_code);
            transaction_code = Convert.resize(Long.toString(tran_code_int += 20), 2, '0', false);
        }
        tcr1.putField(Tcr1.Field.TRANSACTION_CODE, transaction_code);
        if(isPrepaidLoadTransaction(transaction))
        {
            tcr1.putField(Tcr1.Field.CODE_QUALIFIER, "2");
        }
        if(struct_data != null)
        {
            String code_qualifier_str = struct_data.get(STRUCTURED_DATA_FIELD_NAME);
            if(code_qualifier_str != null)
            {
                tcr1.putField(Tcr1.Field.CODE_QUALIFIER, code_qualifier_str);
            }
        }
        if(transaction.representment && base2 != null)
        {
            String chargeback_ref_nr = base2.getChargebackRefNr();
            if(chargeback_ref_nr != null)
            {
                tcr1.putField(Tcr1.Field.CHARGE_BACK_REF, chargeback_ref_nr);
            }
        }
        String pan_entry_mode = transaction.pos_entry_mode.substring(0, 2);
        if(pan_entry_mode.equals("02"))
        {
            tcr1.putField(Tcr1.Field.SPECIAL_COND_IND_1, "2");
        } else
        {
            tcr1.putField(Tcr1.Field.SPECIAL_COND_IND_1, " ");
        }
        String card_acceptor_id = constructCardAcceptorID(transaction.card_acceptor_id_code, cps_enabled);
        if(card_acceptor_id == null)
        {
            card_acceptor_id = Convert.resize("", 15, ' ', true);
        }
        tcr1.putField(Tcr1.Field.CARD_ACCEPTOR_ID_CODE, card_acceptor_id);
        tcr1.putField(Tcr1.Field.CARD_ACCEPTOR_TERM_ID, transaction.terminal_id);
        if(!tcr0.getField(Tcr0.Field.SETTLEMENT_FLAG).equals(Tcr0.Settlement._9_BASE2_SELECTS))
        {
            tcr1.putField(Tcr1.Field.NATIONAL_REIMBURSEMENT_FEE, nns_fee);
        }
        String pdc_term_type = transaction.pos_terminal_type;
        if(pdc_term_type.equals("90"))
        {
            tcr1.putField(Tcr1.Field.MAIL_TEL_IND, "8");
        } else
        if(pdc_term_type.equals("91") || pdc_term_type.equals("93"))
        {
            tcr1.putField(Tcr1.Field.MAIL_TEL_IND, "6");
        } else
        if(pdc_term_type.equals("92") || pdc_term_type.equals("94"))
        {
            tcr1.putField(Tcr1.Field.MAIL_TEL_IND, "5");
        } else
        if(pdc_term_type.equals("95") || pdc_term_type.equals("96"))
        {
            tcr1.putField(Tcr1.Field.MAIL_TEL_IND, "7");
        } else
        {
            String ch_is_present = transaction.pos_cardholder_present;
            if(ch_is_present.equals("2") || ch_is_present.equals("3"))
            {
                tcr1.putField(Tcr1.Field.MAIL_TEL_IND, "1");
            } else
            {
                tcr1.putField(Tcr1.Field.MAIL_TEL_IND, " ");
            }
        }
        if(isPrepaidLoadTransaction(transaction))
        {
            tcr1.putField(Tcr1.Field.PREPAID_CARD_IND, "L");
        }
        String atm_account_selection = null;
        String terminal_type = transaction.pos_terminal_type;
        String cardholder_auth_method = transaction.pos_cardholder_auth_method;
        if(terminal_type.equals("02"))
        {
            if(tran_type.equals("01") && transaction.card_acceptor_name_loc != null && transaction.card_acceptor_name_loc.endsWith("GB") && !transaction.pos_operating_environment.equals("1") && !transaction.pos_operating_environment.equals("2") && !transaction.pos_operating_environment.equals("5"))
            {
                tcr1.putField(Tcr1.Field.CARDHOLDER_ACTIVATED_TERM, "4");
            } else
            {
                tcr1.putField(Tcr1.Field.CARDHOLDER_ACTIVATED_TERM, "2");
            }
        } else
        if(terminal_type.equals("07"))
        {
            if("1".equals(cardholder_auth_method))
            {
                tcr1.putField(Tcr1.Field.CARDHOLDER_ACTIVATED_TERM, "2");
            } else
            {
                tcr1.putField(Tcr1.Field.CARDHOLDER_ACTIVATED_TERM, "3");
            }
        } else
        {
            tcr1.putField(Tcr1.Field.CARDHOLDER_ACTIVATED_TERM, " ");
        }
        if(isChargebackOrChargebackReversal(transaction))
        {
            getTcr1ChargebackFields(tcr1, transaction);
        } else
        {
            String avs_resp_code = " ";
            String auth_source_code = "6";
            String message_text = null;
            String doc_ind = null;
            if(transaction.ext_tran_type != null && (transaction.ext_tran_type.equals("4103") || transaction.ext_tran_type.equals("4111")) && adj_comp != null)
            {
                doc_ind = adj_comp.getDocumentationIndicator();
                if(doc_ind != null)
                {
                    tcr1.putField(Tcr1.Field.DOC_IND, doc_ind);
                } else
                {
                    VisaBase2ExtractLog.reportWarning("No DOC_IND in the adjustment component of the message with post_tran_id %1 ", new String[] {
                            Long.toString(transaction.post_tran_cust_id)
                    });
                    transaction.is_open = true;
                }
            }
            if(transaction.representment && adj_comp != null)
            {
                message_text = adj_comp.getMessageText();
                if(message_text != null)
                {
                    tcr1.putField(Tcr1.Field.MEMBER_MSG, message_text);
                }
                String sp_chargeback_ind = null;
                if(adj_comp.getOriginalAmounts() != null && adj_comp.getOriginalAmounts().getTranAmount() != null)
                {
                    String orig_tran_amount = adj_comp.getOriginalAmounts().getTranAmount();
                    if(transaction.tran_amount_rsp != Long.parseLong(orig_tran_amount) && transaction.card_acceptor_name_loc != null)
                    {
                        CardAcceptorNameLocation card_acceptor_name_location = new CardAcceptorNameLocation(transaction.card_acceptor_name_loc);
                        if(card_acceptor_name_location.getCountry().equals("US"))
                        {
                            sp_chargeback_ind = "P";
                            tcr1.putField(Tcr1.Field.SP_CHARGE_BACK_IND, sp_chargeback_ind);
                        }
                    }
                }
            }
            if(transaction.retention_data != null)
            {
                if(transaction.retention_data.isFieldSet("Address verification result code"))
                {
                    avs_resp_code = transaction.retention_data.getField("Address verification result code");
                } else
                {
                    debugPrintln("Addr ver res code not set for " + transaction.tran_nr);
                }
                if(transaction.retention_data.isFieldSet("Authorisation source code"))
                {
                    auth_source_code = transaction.retention_data.getField("Authorisation source code");
                    if(!" ".equals(auth_source_code) && !Validator.isValidAn(auth_source_code))
                    {
                        VisaBase2ExtractLog.reportWarning("An invalid character (%1) was detected in the Authorization Source Code field in" +
                                        " the retention data of the message with post_tran_id %2.The value '5' has been s" +
                                        "ubstituted for this character"
                                , new String[] {
                                        auth_source_code, Long.toString(transaction.post_tran_id)
                                });
                        auth_source_code = "5";
                    }
                } else
                {
                    debugPrintln("Auth source code not set for " + transaction.tran_nr);
                }
            } else
            {
                debugPrintln("transaction.retention_data is null");
            }
            tcr1.putField(Tcr1.Field.AVS_RESP_CODE, avs_resp_code);
            tcr1.putField(Tcr1.Field.AUTH_SOURCE_CODE, auth_source_code);
            if(terminal_type.equals("02"))
            {
                if(transaction.structured_data_rsp != null)
                {
                    StructuredData struct_data_rsp = new StructuredData();
                    try
                    {
                        struct_data_rsp.fromMsgString(transaction.structured_data_rsp);
                    }
                    catch(Exception e)
                    {
                        VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                                Long.toString(transaction.post_tran_id)
                        });
                    }
                    String proc_code_xml = struct_data_rsp.get(STRUCTURED_DATA_PROC_CODE);
                    String base_i_xml = struct_data_rsp.get("BaseI");
                    ProcessingCodeData processing_code_data = null;
                    BaseI base_i = null;
                    if(base_i_xml != null)
                    {
                        base_i = (BaseI)ObjectFactory.createObjectFromString(base_i_xml);
                        processing_code_data = base_i.getProcessingCodeData();
                    }
                    if(proc_code_xml != null)
                    {
                        processing_code_data = (ProcessingCodeData)ObjectFactory.createObjectFromString(proc_code_xml);
                    }
                    if(processing_code_data != null)
                    {
                        String from_acc = processing_code_data.getFromAccount();
                        if(postilion.core.message.bitmap.Iso8583.AccountType.isSavings(from_acc))
                        {
                            atm_account_selection = "1";
                        } else
                        if(postilion.core.message.bitmap.Iso8583.AccountType.isCheck(from_acc))
                        {
                            atm_account_selection = "2";
                        } else
                        if(postilion.core.message.bitmap.Iso8583.AccountType.isCredit(from_acc))
                        {
                            atm_account_selection = "3";
                        } else
                        {
                            atm_account_selection = "0";
                        }
                    }
                }
            } else
            {
                atm_account_selection = "0";
            }
            if(atm_account_selection == null && terminal_type.equals("02"))
            {
                String from_acc = transaction.from_account_type;
                if(postilion.core.message.bitmap.Iso8583.AccountType.isSavings(from_acc))
                {
                    atm_account_selection = "1";
                } else
                if(postilion.core.message.bitmap.Iso8583.AccountType.isCheck(from_acc))
                {
                    atm_account_selection = "2";
                } else
                if(postilion.core.message.bitmap.Iso8583.AccountType.isCredit(from_acc))
                {
                    atm_account_selection = "3";
                } else
                {
                    atm_account_selection = "0";
                }
            }
            tcr1.putField(Tcr1.Field.ATM_ACCOUNT_SELECTION, atm_account_selection);
            if("09".equals(tran_type))
            {
                if(transaction.tran_reversed.equals(PARTIAL_REVERSAL))
                {
                    tcr1.putField(Tcr1.Field.CASH_BACK, String.valueOf(transaction.new_tran_cash_rsp));
                } else
                {
                    tcr1.putField(Tcr1.Field.CASH_BACK, Long.toString(transaction.tran_cash_rsp));
                }
            } else
            {
                tcr1.putField(Tcr1.Field.CASH_BACK, Convert.resize("", 9, '0', true));
            }
        }
        IccRequest icc_request = null;
        if(transaction.icc_data_req != null)
        {
            IccData icc_data = (IccData)postilion.core.xml.iccdata.ObjectFactory.createObjectFromString(transaction.icc_data_req);
            if(icc_data != null)
            {
                icc_request = icc_data.getIccRequest();
            }
        }
        if(icc_request != null)
        {
            tcr1.putField(Tcr1.Field.CHIP_COND_CODE, icc_request.getChipConditionCode());
        } else
        {
            String chip_condition_code = null;
            ServiceRestrictionCode src = null;
            if(transaction.service_restriction_code != null)
            {
                src = new ServiceRestrictionCode(transaction.service_restriction_code);
            }
            if(src != null && "5".equals(transaction.pos_card_data_input_ability) && "90".equals(pan_entry_mode) && src.isChipOnCard())
            {
                tcr1.putField(Tcr1.Field.CHIP_COND_CODE, "2");
            }
        }
        tcr1.putField(Tcr1.Field.INSTALLMENT_PMT_COUNT, "  ");
        return tcr1;
    }

    public boolean constructDCCIndicator(ExtractedTransaction transaction, Tcr5 tcr5, StructuredData struct_data)
    {
        boolean is_DCC_present = false;
        String tran_code = tcr5.getField(Tcr5.Field.TRANSACTION_CODE);
        if(isDraftData(tran_code) && struct_data != null)
        {
            String currency_code_bill = struct_data.get("051_CURRENCY_CODE_BILL");
            String tran_cur_code = struct_data.get("049_CURRENCY_CODE_TRANS");
            String tran_amount = struct_data.get("004_TRANSACTION_AMOUNT");
            if(currency_code_bill != null && tran_cur_code != null && tran_amount != null)
            {
                tcr5.putField(Tcr5.Field.DCC_INDICATOR, "1");
                is_DCC_present = true;
            }
        }
        return is_DCC_present;
    }

    private static String dbGetAcquirerInstitutionName(String card_acceptor_id)
    {
        String query_get_cps_acq_inst = "{call visabase2_get_cps_acq_inst_mapping(?)}";
        java.sql.CallableStatement stmt = null;
        ResultSet rs = null;
        String acq_inst_name = null;
        try
        {
            stmt = connection.prepareCall(query_get_cps_acq_inst);
            stmt.setString(1, card_acceptor_id);
            for(rs = stmt.executeQuery(); rs.next();)
            {
                acq_inst_name = rs.getString(1);
            }

        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Failed retrieving the acquirer institution name for %1from the database", new String[] {
                    card_acceptor_id
            }, e);
        }
        finally
        {
            try
            {
                OfficeLib.cleanupStatement(connection, stmt, rs);
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportError("Failed cleaning up the database statement", e);
            }
        }
        return acq_inst_name;
    }

    private boolean dbCheckReversalMsg(long post_tran_id, ExtractedTransaction transaction)
    {
        String query_msg = "{call visabase2_reversal_msg(?, ?)}";
        java.sql.CallableStatement stmt = null;
        ResultSet rs = null;
        boolean include_transaction = false;
        try
        {
            stmt = connection.prepareCall(query_msg);
            stmt.setLong(1, post_tran_id);
            stmt.setInt(2, extract_session_id);
            for(rs = stmt.executeQuery(); rs.next();)
            {
                int include = rs.getInt(1);
                if(include == 1)
                {
                    include_transaction = true;
                    java.sql.Timestamp prev_datetime = rs.getTimestamp(2);
                    if(!rs.wasNull())
                    {
                        transaction.prev_datetime_tran_local = new DateTime(prev_datetime);
                    }
                }
            }

        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Failed checking the transaction message with post_tran_id %1 ", new String[] {
                    Long.toString(post_tran_id)
            }, e);
        }
        finally
        {
            try
            {
                OfficeLib.cleanupStatement(connection, stmt, rs);
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportError("Failed cleaning up the database statement", e);
            }
        }
        return include_transaction;
    }

    private static String constructAuthRspCodeICC(ExtractedTransaction transaction, IccRequest icc_request)
            throws Exception
    {
        String resp_code = icc_request.getAuthorizationResponseCode();
        String cryptogram_information_data = icc_request.getCryptogramInformationData();
        byte hex_value[] = null;
        if(cryptogram_information_data != null)
        {
            hex_value = Convert.fromHexStringToHexData(cryptogram_information_data);
        }
        IssuerActionCode issuer_action_code = null;
        issuer_action_code = icc_request.getIssuerActionCode();
        String iac_value = null;
        if(issuer_action_code != null)
        {
            iac_value = issuer_action_code.getDefault();
        }
        if(resp_code != null && resp_code.length() != 0 && transaction.prev_post_tran_id == 0L)
        {
            return resp_code;
        }
        if(transaction.message_type.equals("0220") && hex_value != null && hex_value[0] >= 0 && hex_value[0] <= 63 && transaction.prev_post_tran_id == 0L)
        {
            if(iac_value != null)
            {
                return "Z3";
            } else
            {
                return "Z1";
            }
        }
        if(transaction.message_type.equals("0220") && hex_value != null && hex_value[0] >= 64 && hex_value[0] <= 127 && transaction.prev_post_tran_id == 0L)
        {
            if(iac_value != null)
            {
                return "Y3";
            } else
            {
                return "Y1";
            }
        }
        String rsp_code = null;
        if(linked_rsp_code)
        {
            String get_prev_tran = "SELECT rsp_code_rsp FROM post_tran WITH (NOLOCK) WHERE post_tran_id = " + Long.toString(transaction.prev_post_tran_id) + " AND message_type = '0100'";
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try
            {
                stmt = connection.prepareStatement(get_prev_tran);
                for(rs = stmt.executeQuery(); rs.next();)
                {
                    String rsp_code_rsp = rs.getString(1);
                    if(rsp_code_rsp != null)
                    {
                        rsp_code = rsp_code_rsp;
                    }
                }

                if(rsp_code == null && transaction.rsp_code_req != null && !transaction.rsp_code_req.equals(""))
                {
                    rsp_code = transaction.rsp_code_req;
                }
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportError("Failed retrieving the previous transaction for %1from the database", new String[] {
                        Long.toString(transaction.post_tran_id)
                }, e);
            }
            finally
            {
                try
                {
                    OfficeLib.cleanupStatement(connection, stmt, rs);
                }
                catch(SQLException e)
                {
                    VisaBase2ExtractLog.reportError("Failed cleaning up the database statement", e);
                }
            }
        }
        return rsp_code;
    }

    protected void setTransaction(ExtractedTransaction transaction)
    {
        debugPrintln("setTransaction()");
        try
        {
            String settle_entity_str;
            if(node_granularity)
            {
                settle_entity_str = String.valueOf(transaction.settle_entity_id);
            } else
            {
                settle_entity_str = transaction.node.toUpperCase() + "_" + transaction.acquiring_inst_id_code;
            }
            FileInformation file_info = (FileInformation)settle_entity_lookup.get(settle_entity_str);
            if(file_info != null)
            {
                printTransaction(file_info, transaction);
            } else
            {
                debugPrintln("setTransaction() file_info == null");
                if(!node_granularity && (transaction.acquiring_inst_id_code == null || transaction.acquiring_inst_id_code.trim().length() == 0))
                {
                    VisaBase2ExtractLog.reportWarning("File Granularity set to one file per acquirer per sink node, but no Acquiring In" +
                                    "stitution is set for transaction withpost_tran_cust_id: %1"
                            , new String[] {
                                    Long.toString(transaction.post_tran_cust_id)
                            });
                }
            }
        }
        catch(Throwable e)
        {
            VisaBase2ExtractLog.reportError("An error occured while writing a transaction to file", e);
        }
        dbLogTranReference(transaction.post_tran_id);
    }

    private boolean isPrepaidLoadTransaction(ExtractedTransaction transaction)
    {
        return "21".equals(transaction.tran_type) && "6500".equals(transaction.ext_tran_type);
    }

    public String getSelectStatement(String component_select_prefix, String component_from_prefix, String
            component_where_prefix, String component_where_postfix)
    {
        String select_clause = "select " + component_select_prefix + " post_tran.post_tran_id," + " post_tran" +
                ".acquiring_inst_id_code," + " post_tran.auth_id_rsp," + " post_tran.batch_nr," + " post_tran" +
                ".datetime_tran_local," + " post_tran.from_account_type," + " post_tran.icc_data_req," + " post_tran" +
                ".message_type," + " post_tran.pos_condition_code," + " post_tran.pos_entry_mode," + " post_tran" +
                ".retention_data," + " post_tran.rsp_code_req," + " post_tran.settle_amount_impact," + " post_tran" +
                ".settle_entity_id," + " post_tran.sponsor_bank," + " post_tran.structured_data_req," + " post_tran" +
                ".structured_data_rsp," + " post_tran.tran_amount_rsp," + " post_tran.tran_cash_rsp," + " post_tran" +
                ".tran_tran_fee_rsp," + " post_tran.tran_currency_code," + " post_tran.tran_nr," + " post_tran" +
                ".tran_reversed," + " post_tran.tran_type," + " post_tran_cust.card_acceptor_id_code," + " " +
                "post_tran_cust.card_acceptor_name_loc," + " post_tran_cust.card_seq_nr," + " post_tran_cust" +
                ".merchant_type," + " post_tran_cust.pan," + " post_tran_cust.expiry_date," + " post_tran_cust" +
                ".pos_operating_environment," + " post_tran_cust.pos_card_data_input_ability," + " post_tran_cust" +
                ".pos_cardholder_auth_method," + " post_tran_cust.pos_cardholder_present," + " post_tran_cust" +
                ".pos_terminal_type," + " post_tran_cust.post_tran_cust_id," + " post_tran_cust.terminal_id," +
                " post_batch.datetime_end," + " post_tran.extended_tran_type," + " post_tran" +
                ".message_reason_code," + " post_tran.card_verification_result," + " post_tran" +
                ".prev_post_tran_id," + " post_tran_cust.service_restriction_code," + " post_tran_prev" +
                ".retention_data," + " post_tran_prev.structured_data_req," + " post_tran_prev" +
                ".structured_data_rsp," + " post_tran.sink_node_name," + " post_tran_prev.prev_post_tran_id," +
                "" + " post_tran_prev.pos_entry_mode";
        String from_clause = " from " + component_from_prefix + " post_tran\t" + "INNER JOIN post_tran_cust ON " +
                "post_tran_cust.post_tran_cust_id = post_tran.post_tran_cust_id " + "LEFT JOIN post_tran AS post_tran_prev ON" +
                " " + "post_tran.prev_post_tran_id = post_tran_prev.post_tran_id " + "INNER JOIN post_batch ON " +
                "post_tran.settle_entity_id = post_batch.settle_entity_id AND " + "post_tran.batch_nr = post_batch.batch_nr ";
        String where_clause = " where " + component_where_prefix + " post_tran.message_type in" + " (0220,0420,0620)"
                + " AND post_tran_cust.pos_terminal_type = '"+ terminal_type + buildIncludedSourceNodeList()+ "'"
                + component_where_postfix;
        String order_by_clause = " order by post_tran.batch_nr, post_tran.tran_nr";
        String query = select_clause + from_clause + where_clause + order_by_clause;
        debugPrintln("\nExtract Select Query:\n" + query);
        return query;
    }

    private String getOriginal0100PosEntryMode(ExtractedTransaction transaction)
            throws SQLException
    {
        java.sql.CallableStatement stmt = null;
        ResultSet rs = null;
        try
        {
            try
            {
                stmt = connection.prepareCall("{call visabase2extract_get_0100_pos_entry_mode(" + transaction.post_tran_cust_id + ") }");
                rs = stmt.executeQuery();
                String s1 = null;
                try
                {
                    rs.next();
                    String pos_entry_mode = rs.getString(1);
                    String s = pos_entry_mode;
                    return s;
                }
                catch(SQLException e)
                {
                    s1 = null;
                }
                return s1;
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportWarning("Linked 0100 message not found for post_tran_cust_id: %1", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            String s2 = null;
            return s2;
        }
        finally
        {
            OfficeLib.cleanupStatement(connection, stmt, rs);
        }
    }

    protected Tcr0ForTc50 getTcr0ForTc50(FileInformation file_info, ExtractedTransaction transaction, IccRequest icc_request, boolean cps_enabled)
            throws Exception
    {
        Tcr0ForTc50 tcr0fortc50 = new Tcr0ForTc50();
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            adj_comp = getAdjustmentComponent(struct_data);
        }
        if(adj_comp != null)
        {
            String message_text = adj_comp.getMessageText();
            if(message_text != null)
            {
                if(message_text.length() < 108)
                {
                    tcr0fortc50.putField(Tcr0ForTc50.Field.TEXT, message_text);
                } else
                {
                    VisaBase2ExtractLog.reportWarning("Text Message too long in the message with post_tran_cust_id %1 ", new String[] {
                            Long.toString(transaction.post_tran_cust_id)
                    });
                    transaction.is_open = true;
                }
            } else
            {
                VisaBase2ExtractLog.reportWarning("No Text Message present in the message with post_tran_cust_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
                tcr0fortc50.putField(Tcr0ForTc50.Field.TEXT, Convert.resize("", 107, ' ', true));
                transaction.is_open = true;
            }
        } else
        {
            VisaBase2ExtractLog.reportWarning("No Text Message present in the message with post_tran_cust_id %1 ", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            tcr0fortc50.putField(Tcr0ForTc50.Field.TEXT, Convert.resize("", 107, ' ', true));
            transaction.is_open = true;
        }
        return tcr0fortc50;
    }

    public PrimeBase2Extract()
    {
        node_names = new String[0];
        settle_entity_lookup = new Hashtable();
        currency_decimal = new Hashtable();
        transaction_exceptions = new Vector();
        reimbursement_attribute = null;
        is_cps_participating_country = true;
        process_declined_reversals = false;
        set_acquiring_institution_id = null;
        acq_ref_nr = null;
        calculated_ra = null;
        tcr5_for_icc = false;
        use_format_code = false;
        format_code = null;
        compliancyLevel = 0;
        extract_empty_tcr5_for_TC05_TC06 = false;
        min_post_tran_id_for_session = -1L;
        collection_only_flag_checker = new CollectionOnlyFlagChecker();
    }

    private void initCurrencyDecimalHashtable()
            throws SQLException
    {
        currency_decimal.clear();
        String query = "select currency_code, nr_decimals from post_currencies where nr_decimals in (0,3" +
                ")"
                ;
        String decimals = "0";
        String currency_code = "000";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            debugPrintln(query);
            stmt = connection.prepareStatement(query);
            for(rs = stmt.executeQuery(); rs.next(); currency_decimal.put(currency_code, decimals))
            {
                currency_code = rs.getString(1);
                decimals = rs.getString(2);
            }

        }
        finally
        {
            try
            {
                OfficeLib.cleanupStatement(connection, stmt, rs);
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportError("Failed cleaning up the database statement", e);
            }
        }
    }

    protected static void debugPrintln(String line)
    {
        if(debug)
        {
            try
            {
                dump_file_output_stream.write(Convert.getData(line));
                dump_file_output_stream.write(10);
                dump_file_output_stream.flush();
            }
            catch(Throwable e) { }
        }
    }

    private static void debugPrintln(String line, boolean include_time_prefix)
    {
        if(include_time_prefix)
        {
            debugPrintln("[" + getTimeString() + "] " + line);
        } else
        {
            debugPrintln(line);
        }
    }

    private static void initMVVHashTable()
            throws SQLException
    {
        String sql_query = "select card_acceptor_id,mvv_assigned_id from visabase2_merch_cfg";
        PreparedStatement statement = null;
        ResultSet rs = null;
        merchant_verification_value.clear();
        try
        {
            statement = connection.prepareStatement(sql_query);
            for(rs = statement.executeQuery(); rs.next();)
            {
                String card_acceptor_id = rs.getString(1);
                String mvv_assigned_id = rs.getString(2);
                if(mvv_assigned_id != null)
                {
                    merchant_verification_value.put(card_acceptor_id, mvv_assigned_id);
                }
            }

        }
        finally
        {
            OfficeLib.cleanupStatement(connection, statement, rs);
        }
    }

    protected void getTcr0ChargebackFields(FileInformation file_info, Tcr0 tcr0, ExtractedTransaction transaction)
            throws Exception
    {
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        BaseII base2 = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            base2 = getBaseII(struct_data);
            adj_comp = getAdjustmentComponent(struct_data);
        }
        String proc_code_string = null;
        String transaction_code = null;
        String tran_type = null;
        String acq_ref_nr_string = null;
        if(adj_comp != null)
        {
            proc_code_string = adj_comp.getProcessingCode();
            acq_ref_nr_string = adj_comp.getAcquirerRefNr();
        }
        if(acq_ref_nr_string != null && acq_ref_nr_string.length() == 23)
        {
            String format_code = acq_ref_nr_string.substring(0, 1);
            tcr0.putField(Tcr0.Field.FORMAT_CODE, format_code);
            String acq_inst = acq_ref_nr_string.substring(1, 7);
            tcr0.putField(Tcr0.Field.BIN, acq_inst);
            String date = acq_ref_nr_string.substring(7, 11);
            tcr0.putField(Tcr0.Field.DATE, date);
            String film_locator = acq_ref_nr_string.substring(11, 22);
            tcr0.putField(Tcr0.Field.FILM_LOCATOR, film_locator);
            acq_ref_nr = acq_ref_nr_string;
        } else
        {
            VisaBase2ExtractLog.reportWarning("No acquirer reference number in baseII of the message with post_tran_id %1 ", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
        }
        if(proc_code_string != null)
        {
            ProcessingCode proc_code = new ProcessingCode(proc_code_string);
            tran_type = proc_code.getTranType();
            transaction_code = (String)tran_type_table.get(tran_type);
            transaction_code = getTranType(transaction_code, transaction.ext_tran_type);
        }
        if(transaction_code == null)
        {
            transactionException(file_info, transaction, "transaction_code is null");
            return;
        }
        tcr0.putField(Tcr0.Field.TRANSACTION_CODE, transaction_code);
        String reason_code = transaction.message_reason_code;
        if(reason_code != null && reason_code.length() == 4)
        {
            tcr0.putField(Tcr0.Field.REASON_CODE, reason_code.substring(2));
        }
        String auth_id_resp = null;
        if(adj_comp != null)
        {
            auth_id_resp = adj_comp.getAuthIdRsp();
        }
        if(auth_id_resp == null)
        {
            auth_id_resp = Convert.resize("", 6, ' ', true);
        }
        tcr0.putField(Tcr0.Field.AUTH_CODE, auth_id_resp);
        String reimbursement_atr = null;
        String request_payment_service = null;
        String intl_fee_ind = null;
        if(base2 != null)
        {
            reimbursement_atr = base2.getReimbursementAttr();
            request_payment_service = base2.getReqPaymentService();
            intl_fee_ind = base2.getIntFeeInd();
        }
        if(reimbursement_atr != null)
        {
            tcr0.putField(Tcr0.Field.REIMBURSEMENT_ATTR, reimbursement_atr);
            calculated_ra = reimbursement_atr;
        } else
        {
            VisaBase2ExtractLog.reportWarning("No Reimbursement attribute provided for message with post_tran_id %1 ", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
        }
        if(request_payment_service != null)
        {
            tcr0.putField(Tcr0.Field.REQ_PAYMENT_SERVICE, request_payment_service);
        } else
        {
            VisaBase2ExtractLog.reportWarning("No Request Payment Service provided for message with post_tran_id %1 ", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
            return;
        }
        if(intl_fee_ind != null)
        {
            tcr0.putField(Tcr0.Field.INTL_FEE_IND, intl_fee_ind);
        }
        if("28".equals(tcr0.getField(Tcr0.Field.REASON_CODE)) || "70".equals(tcr0.getField(Tcr0.Field.REASON_CODE)))
        {
            tcr0.putField(Tcr0.Field.CRB_EXCEPTION, "Y");
        }
        tcr0.putField(Tcr0.Field.USAGE_CODE, "1");
    }

    public void done()
            throws SQLException
    {
        String file_name = "";
        FileInformation file_info = null;
        try
        {
            for(Enumeration file_infos = settle_entity_lookup.elements(); file_infos.hasMoreElements();)
            {
                file_info = (FileInformation)file_infos.nextElement();
                PrintWriter settle_file = file_info.settle_file;
                debugPrintln("done() [" + file_info.full_file_name + "]");
                settle_file.print(generateTc91(file_info.batch_info, file_info.batch_info.last_transaction_batch_nr, file_info)+getFileLineFeed(file_format));
                closeBatch(file_info, file_info.batch_info.last_transaction_batch_nr);
                settle_file.print(generateTc92(file_info.batch_info, file_info)+getFileLineFeed(file_format));
                settle_file.close();
                file_name = file_info.full_file_name;
                (new File(file_name + ".temp")).renameTo(new File(constructNewFileName(file_info)));
                file_info.log_file.close();
                if(!transaction_exceptions.isEmpty())
                {
                    String post_tran_ids_string = "";
                    for(Enumeration post_tran_ids_enum = transaction_exceptions.elements(); post_tran_ids_enum.hasMoreElements();)
                    {
                        post_tran_ids_string = post_tran_ids_string + (String)post_tran_ids_enum.nextElement() + ",";
                    }

                    post_tran_ids_string = post_tran_ids_string.substring(0, post_tran_ids_string.length() - 1);
                    VisaBase2ExtractLog.reportWarning("There are transactions that failed during the extract. TCR components were not g" +
                                    "enerated for these transactions. Please check the visabase2_outgoing_exceptions " +
                                    "table for  the following post_tran_id(s): %1"
                            , new String[] {
                                    post_tran_ids_string
                            });
                }
            }

            if(debug)
            {
                dump_file_output_stream.close();
            }


            //logWarnings();
            file_info.log_file.println("Loggin extract jar details");
            file_info.log_file.println("FileName: " + file_info.full_file_name);
            file_info.log_file.println("SourceNodeName: " + this.source_node_name);
            file_info.log_file.println("Run Date: " + this.report_creation_date);

            // Creating the detail batch and summary excel reports
            Process process = null; // for debug
            BufferedReader inStream = null; // for debug
            BufferedReader errorStream = null; // for debug
            String s = null;
            process = Runtime
                    .getRuntime()
                    .exec("java -jar C:\\WINDOWS\\java\\classes\\postilion\\office\\extract\\visabase2\\eftcPrimeBase2_Excel_Reports.jar "
                            + file_location+"\\"+final_file_name
                            + " "
                            + this.source_node_name
                            + " "
                            + report_creation_date);

            inStream = new BufferedReader(new InputStreamReader(
                    process.getInputStream())); // for debug
            errorStream = new BufferedReader(new InputStreamReader(
                    process.getErrorStream())); // for debug

            // read the output from the command
            while ((s = inStream.readLine()) != null) {
                System.out.println(s); // for debug
            }

            // read any error from the attempted command
            while ((s = errorStream.readLine()) != null) {
                System.out.println(s); // for debug
            }

            //System.out.println(inStream.readLine()); // for debug
            System.out.println(final_file_name); // for debug
            System.out.println(report_creation_date); // for debug
            System.out.println(source_node_name); // for debug
        }
        catch(IOException e)
        {
            VisaBase2ExtractLog.reportError("Process unable to close output: %1", e);
        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Process unable to close output: %1", e);
        }
        OfficeLib.cleanupStatement(connection, null, null);

        //start creating the excel reports
        VisaBase2ExtractLog.reportWarning("Creating the Detail and Summary excel reports");


    }









    public boolean isChargebackOrChargebackReversal(ExtractedTransaction transaction)
    {
        return transaction.message_type.equals("0620") && "4101".equals(transaction.ext_tran_type) || "4102".equals(transaction.ext_tran_type);
    }

    public boolean isStandaloneReversal(ExtractedTransaction transaction)
    {
        return transaction.message_type.equals("0220") && ("02".equals(transaction.tran_type) || "22".equals(transaction.tran_type)) && transaction.structured_data_req != null;
    }

    private static void dbGet0100data(ExtractedTransaction transaction, String tran_nr)
    {
        String query_get_0100_data = "{call visabase2_get_0100_data(?)}";
        java.sql.CallableStatement stmt = null;
        ResultSet rs = null;
        try
        {
            stmt = connection.prepareCall(query_get_0100_data);
            stmt.setString(1, tran_nr);
            for(rs = stmt.executeQuery(); rs.next();)
            {
                transaction.structured_data_0100 = rs.getString(1);
                transaction.cashback_0100 = rs.getLong(2);
            }

        }
        catch(Exception e)
        {
            VisaBase2ExtractLog.reportError("Failed retrieving the structured data of the original 0100 message pertaining to" +
                            " fraud for %1 from the database"
                    , new String[] {
                            tran_nr
                    }, e);
        }
        finally
        {
            try
            {
                OfficeLib.cleanupStatement(connection, stmt, rs);
            }
            catch(SQLException e)
            {
                VisaBase2ExtractLog.reportError("Failed cleaning up the database statement", e);
            }
        }
    }

    public static void closeBatch(FileInformation file_info, int batch_nr)
    {
        file_info.log_file.println("\t\t\tPostilion Batch Number: " + Convert.resize(String.valueOf(batch_nr), 8, ' ', true) + " CTF Batch Number: " + Convert.resize(String.valueOf(file_info.batch_info.batch_no_counter), 8, ' ', true) + " Transactions: " + Convert.resize(String.valueOf(file_info.batch_info.batch_tc_counter), 8, ' ', true) + " Exceptions: " + Convert.resize(String.valueOf(file_info.batch_info.exception_counter), 8, ' ', true) + " " + (new DateTime()).get("hh:mm:ss"));
        file_info.batch_info.batch_no_counter++;
        file_info.batch_info.batch_source_amount = 0L;
        file_info.batch_info.batch_tc_counter = 0L;
        file_info.batch_info.batch_monetary_tc_counter = 0L;
        file_info.batch_info.batch_tcr_counter = 0L;
        file_info.batch_info.exception_counter = 0L;
    }

    protected Tcr2ForFraud getTcr2ForFraud(FileInformation file_info, ExtractedTransaction transaction, boolean cps_enabled, Tcr0ForFraud tcr0forfraud)
            throws Exception
    {
        Tcr2ForFraud tcr2forfraud = new Tcr2ForFraud();
        tcr2forfraud.putField(Tcr2ForFraud.Field.TERMINAL_ID, transaction.terminal_id);
        StructuredData struct_data = null;
        AdjustmentComponent adj_comp = null;
        BaseII base2_0100 = null;
        if(transaction.structured_data_req != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_req);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_cust_id)
                });
            }
            adj_comp = getAdjustmentComponent(struct_data);
        }
        String proc_code_string = null;
        String prev_tran_nr = null;
        String auth = null;
        if(adj_comp != null)
        {
            proc_code_string = adj_comp.getProcessingCode();
            prev_tran_nr = adj_comp.getPreviousPostTranID();
            auth = adj_comp.getAuthIdRsp();
        }
        if(prev_tran_nr == null)
        {
            VisaBase2ExtractLog.reportWarning("PreviousTranID is missing from structured data with post_tran_id %1 ", new String[] {
                    Long.toString(transaction.post_tran_cust_id)
            });
            transaction.is_open = true;
            return null;
        }
        if(transaction.structured_data_0100 != null)
        {
            struct_data = new StructuredData();
            try
            {
                struct_data.fromMsgString(transaction.structured_data_0100);
            }
            catch(Exception e)
            {
                VisaBase2ExtractLog.reportWarning("Incorrectly formatted structured data in the message with post_tran_id %1 ", new String[] {
                        Long.toString(transaction.post_tran_id)
                });
            }
            base2_0100 = getBaseII(struct_data);
        }
        if(base2_0100 != null)
        {
            if(base2_0100.getTranId() != null)
            {
                tcr2forfraud.putField(Tcr2ForFraud.Field.TRANSACTION_IDENTIFIER, base2_0100.getTranId());
                tcr2forfraud.putField(Tcr2ForFraud.Field.EXCLUDED_TRAN_ID_REASON, " ");
            } else
            {
                tcr2forfraud.putField(Tcr2ForFraud.Field.TRANSACTION_IDENTIFIER, Convert.resize("", 15, ' ', true));
                tcr2forfraud.putField(Tcr2ForFraud.Field.EXCLUDED_TRAN_ID_REASON, "N");
            }
        } else
        {
            tcr2forfraud.putField(Tcr2ForFraud.Field.TRANSACTION_IDENTIFIER, Convert.resize("", 15, ' ', true));
            tcr2forfraud.putField(Tcr2ForFraud.Field.EXCLUDED_TRAN_ID_REASON, "N");
        }
        String tran_type;
        if(proc_code_string != null)
        {
            ProcessingCode proc_code = new ProcessingCode(proc_code_string);
            tran_type = proc_code.getTranType();
        } else
        {
            transactionException(file_info, transaction, "transaction_code is null");
            return null;
        }
        if("09".equals(tran_type))
        {
            String cashback = Long.toString(transaction.cashback_0100);
            tcr2forfraud.putField(Tcr2ForFraud.Field.CASHBACK_INDICATOR, "Y");
            tcr2forfraud.putField(Tcr2ForFraud.Field.CASHBACK_AMOUNT, Convert.resize(cashback, 9, '0', false));
        } else
        {
            tcr2forfraud.putField(Tcr2ForFraud.Field.CASHBACK_INDICATOR, "N");
            tcr2forfraud.putField(Tcr2ForFraud.Field.CASHBACK_AMOUNT, Convert.resize("", 9, '0', true));
        }
        if(auth != null)
        {
            tcr2forfraud.putField(Tcr2ForFraud.Field.AUTHORIZATION_CODE, Convert.resize(auth, 6, ' ', true));
        } else
        {
            tcr2forfraud.putField(Tcr2ForFraud.Field.AUTHORIZATION_CODE, Convert.resize("", 6, ' ', true));
        }
        String cardholder_id = transaction.pos_cardholder_present;
        String cardholder_auth = transaction.pos_cardholder_auth_method;
        if("2".equals(cardholder_id) || "3".equals(cardholder_id))
        {
            tcr2forfraud.putField(Tcr2ForFraud.Field.CARDHOLDER_ID_METHOD_USED, "4");
        } else
        if("1".equals(cardholder_auth))
        {
            tcr2forfraud.putField(Tcr2ForFraud.Field.CARDHOLDER_ID_METHOD_USED, "2");
        } else
        if("0".equals(cardholder_auth))
        {
            tcr2forfraud.putField(Tcr2ForFraud.Field.CARDHOLDER_ID_METHOD_USED, "1");
        } else
        {
            tcr2forfraud.putField(Tcr2ForFraud.Field.CARDHOLDER_ID_METHOD_USED, "0");
        }
        tcr2forfraud.putField(Tcr2ForFraud.Field.POS_ENTRY_MODE, transaction.pos_entry_mode.substring(0, 2));
        String card_data_input_capability = transaction.pos_card_data_input_ability;
        if(card_data_input_capability.equals("6"))
        {
            card_data_input_capability = "9";
        } else
        if(card_data_input_capability.equals("7") || card_data_input_capability.equals("8") || card_data_input_capability.equals("9"))
        {
            card_data_input_capability = "0";
        } else
        if(card_data_input_capability.equals("A") || card_data_input_capability.equals("B"))
        {
            card_data_input_capability = "8";
        }
        tcr2forfraud.putField(Tcr2ForFraud.Field.POS_TERM_CAPABILITY, card_data_input_capability);
        String terminal_type = transaction.pos_terminal_type;
        String cardholder_auth_method = transaction.pos_cardholder_auth_method;
        if(terminal_type.equals("02"))
        {
            if(tran_type.equals("01") && transaction.card_acceptor_name_loc != null && transaction.card_acceptor_name_loc.endsWith("GB") && !transaction.pos_operating_environment.equals("1") && !transaction.pos_operating_environment.equals("2") && !transaction.pos_operating_environment.equals("5"))
            {
                tcr2forfraud.putField(Tcr2ForFraud.Field.CARDHOLDER_ACTIVATED_TERM_IND, "4");
            } else
            {
                tcr2forfraud.putField(Tcr2ForFraud.Field.CARDHOLDER_ACTIVATED_TERM_IND, "2");
            }
        } else
        if(terminal_type.equals("07"))
        {
            if("1".equals(cardholder_auth_method))
            {
                tcr2forfraud.putField(Tcr2ForFraud.Field.CARDHOLDER_ACTIVATED_TERM_IND, "2");
            } else
            {
                tcr2forfraud.putField(Tcr2ForFraud.Field.CARDHOLDER_ACTIVATED_TERM_IND, "3");
            }
        } else
        {
            tcr2forfraud.putField(Tcr2ForFraud.Field.CARDHOLDER_ACTIVATED_TERM_IND, " ");
        }
        String card_acceptor_id = constructCardAcceptorID(transaction.card_acceptor_id_code, cps_enabled);
        if(card_acceptor_id == null)
        {
            card_acceptor_id = Convert.resize("", 15, ' ', true);
        }
        tcr2forfraud.putField(Tcr2ForFraud.Field.CARD_ACCEPTOR_ID, card_acceptor_id);
        return tcr2forfraud;
    }

    private Tcr0 checkForCollectionOnlyFlag(ExtractedTransaction transaction, String transaction_code, FileInformation file_info, Tcr0 tcr0)
    {
        String sink_node_name = file_info.sink_node_name;
        String acquirer_id = transaction.acquiring_inst_id_code;
        boolean collection_only = collection_only_flag_checker.isCollectionOnlyTransaction(sink_node_name, acquirer_id, transaction.pan);
        if(collection_only)
        {
            if(!Validator.isValidN(tcr0.getField(Tcr0.Field.SETTLEMENT_FLAG)))
            {
                transactionException(file_info, transaction, "settlement flag is not numeric");
                return null;
            }
            tcr0.putField(Tcr0.Field.COLLECTION_ONLY_FLAG, "C");
            tcr0.putField(Tcr0.Field.DESTINATION_AMOUNT, "000000000000");
            tcr0.putField(Tcr0.Field.DESTINATION_CURRENCY_CODE, "   ");
        } else
        {
            tcr0.putField(Tcr0.Field.COLLECTION_ONLY_FLAG, " ");
        }
        return tcr0;
    }

    protected static String getNetSettlementFlag(String pan, String currency_code)
    {
        if(net_settle_flag.size() == 0)
        {
            return Tcr0.Settlement._9_BASE2_SELECTS;
        }
        int bin_size = pan.length() >= 6 ? 6 : pan.length();
        for(String bin = pan.substring(0, bin_size); bin.length() > 0; bin = bin.substring(0, bin.length() - 1))
        {
            if(net_settle_flag.containsKey(bin))
            {
                if(currency_code.equals(nns_curr_code))
                {
                    return (String)net_settle_flag.get(bin);
                } else
                {
                    return Tcr0.Settlement._9_BASE2_SELECTS;
                }
            }
        }

        return Tcr0.Settlement._9_BASE2_SELECTS;
    }

    public static Vector toVector(Enumeration e)
    {
        Vector result = new Vector();
        for(; e.hasMoreElements(); result.addElement(e.nextElement())) { }
        return result;
    }

    protected String generateTc90(int file_id, boolean test, String security_code, FileInformation file_info)
            throws Exception
    {
        return "90" + processing_bin + (new DateTime()).get("yyDDD") + Convert.resize("", 16, ' ', true) + (test ? "TEST" : "    ") + Convert.resize("", 29, ' ', true) + Convert.resize(security_code, 8, ' ', true) + Convert.resize("", 6, ' ', true) + Convert.resize(String.valueOf(file_id), 3, '0', false) + Convert.resize("", 89, ' ', true);
    }

    public void loadNetSettlementFile(String file_name)
            throws Exception
    {
        File file = new File(file_name);
        if(!file.exists())
        {
            VisaBase2ExtractLog.reportError("The Net Settlement Mapping File could not be found. The filename is %1", new String[] {
                    file_name
            });
        }
        LineNumberReader line_reader = new LineNumberReader(new FileReader(file));
        for(String line = line_reader.readLine(); line != null; line = line_reader.readLine())
        {
            int pivot = line.indexOf("=");
            if(pivot <= 0 || pivot == line.length() - 1)
            {
                VisaBase2ExtractLog.reportError("A record in the Net Settlement Mapping File was malformed. The BASE II Settlemen" +
                                "t File Extract will not take this record into account when generating the outgoi" +
                                "ng CTF file. The malformed record was present in line %1 of the file %2 and cont" +
                                "ained the following data: %3"
                        , new String[] {
                                String.valueOf(line_reader.getLineNumber()), file_name, line
                        });
            } else
            {
                String bin = line.substring(0, pivot);
                String flag = line.substring(pivot + 1);
                if(flag.equals(Tcr0.Settlement._0_INTERNATIONAL) || flag.equals(Tcr0.Settlement._3_CLEARING_ONLY) || flag.equals(Tcr0.Settlement._8_NATIONAL) || flag.equals(Tcr0.Settlement._9_BASE2_SELECTS))
                {
                    net_settle_flag.put(bin, flag);
                } else
                {
                    VisaBase2ExtractLog.reportError("A record in the Net Settlement Mapping File contained an unknown Net Settlement " +
                                    "Flag value. The BASE II Settlement File Extract has set the Net Settlement Flag " +
                                    "value for this record to the default value '9'. The record was present in line %" +
                                    "1 of the file %2 and contained the following data: BIN=%3, Net Settlement Flag=%" +
                                    "4"
                            , new String[] {
                                    String.valueOf(line_reader.getLineNumber()), file_name, bin, flag
                            });
                }
            }
        }

    }

    static
    {
        tran_type_table = new Hashtable();
        tran_type_table.put("00", "05");
        tran_type_table.put("01", "07");
        tran_type_table.put("02", "05");
        tran_type_table.put("09", "05");
        tran_type_table.put("11", "05");
        tran_type_table.put("20", "06");
        tran_type_table.put("21", "06");
        tran_type_table.put("22", "06");
        tran_type_table.put("91", "48");
    }

    public String getTerminalType(String terminal_type)
    {
        if(terminal_type.equalsIgnoreCase("POS"))
        {
            return TerminalType._01_POS;
        }
        //else
        return TerminalType._02_ATM;
    }


    protected static class TerminalType
    {
        protected static final String _01_POS = "01";
        protected static final String _02_ATM = "02";
    }

    protected static class FileFormat
    {
        protected static final String _0_DOS_WINDOWS = "0";
        protected static final String _1_UNIX = "1";
        protected static final String _2_MAC = "2";
    }

    protected static class FileFormatChar
    {
        protected static final String _0_DOS_WINDOWS = "\r\n";
        protected static final String _1_UNIX = "\n";
        protected static final String _2_MAC = "\r";
    }

    public String getFileLineFeed(String param_value)
    {
        if(param_value.equals(FileFormat._0_DOS_WINDOWS))
        {
            return FileFormatChar._0_DOS_WINDOWS;
        }
        if(param_value.equals(FileFormat._1_UNIX))
        {
            return FileFormatChar._1_UNIX;
        }
        if(param_value.equals(FileFormat._2_MAC))
        {
            return FileFormatChar._2_MAC;
        }
        return "\n";
    }


    private String buildIncludedSourceNodeList()
    {
        StringBuffer list_buffer = new StringBuffer();
        if(selected_source_nodes == null || selected_source_nodes.length == 0)
        {
            return "";
        }
        list_buffer.append(" AND post_tran_cust.source_node_name IN (");
        int len = selected_source_nodes.length;
        for(int i = 0; i < len; i++)
        {
            list_buffer.append('\'');
            list_buffer.append(selected_source_nodes[i]);
            list_buffer.append('\'');
            if(i + 1 < len)
            {
                list_buffer.append(',');
            }
        }

        list_buffer.append(") ");
        return list_buffer.toString();
    }

    public static String[] splitParams(String params)
    {
        if (params == null)
        {
            return null;
        }
        else
        {
            int len = params.length();
            char[] sep = new char[] { ',', '\t' };
            char[] special_sep = new char[] { '"', "'".charAt(0) };
            Vector v = new Vector();
            int i = 0;

            int next_special_sep;
            while (i < len)
            {
                int next_sep = len;

                int j;
                for (next_special_sep = 0; next_special_sep < sep.length; ++next_special_sep)
                {
                    j = params.indexOf(sep[next_special_sep], i);
                    if (j != -1 && j < next_sep)
                    {
                        next_sep = j;
                    }
                }

                next_special_sep = len;

                int end;
                for (j = 0; j < special_sep.length; ++j)
                {
                    end = params.indexOf(special_sep[j], i);
                    if (end != -1 && end < next_special_sep)
                    {
                        next_special_sep = end;
                    }
                }

                if (next_special_sep == i)
                {
                    char c = params.charAt(i);
                    end = params.indexOf(c, i + 1);
                    if (end == -1)
                    {
                        end = len;
                    }

                    v.addElement(params.substring(i + 1, end));
                    i = end + 1;
                }
                else if (next_sep == i)
                {
                    ++i;
                }
                else
                {
                    v.addElement(params.substring(i, next_sep));
                    i = next_sep;
                }
            }

            String[] temp = new String[v.size()];

            for (next_special_sep = 0; next_special_sep < v.size(); ++next_special_sep)
            {
                temp[next_special_sep] = (String) v.elementAt(next_special_sep);
            }

            return temp;
        }
    }
}
