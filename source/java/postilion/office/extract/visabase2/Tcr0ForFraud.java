package postilion.office.extract.visabase2;


import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;

public class Tcr0ForFraud extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String DESTINATION_BIN = new String("destination bin");
        public static final String SOURCE_BIN = new String("source bin");
        public static final String ACCOUNT_NUMBER = new String("account number");
        public static final String ACQUIRER_REFERENCE_NUMBER = new String("acquirer reference number");
        public static final String ACQUIRER_BUSINESS_ID = new String("acquirer business id");
        public static final String RESPONSE_CODE = new String("response code");
        public static final String PURCHASE_DATE = new String("purchase date");
        public static final String MERCHANT_NAME = new String("merchant name");
        public static final String MERCHANT_CITY = new String("merchant city");
        public static final String MERCHANT_COUNTRY_CODE = new String("merchant country code");
        public static final String MERCHANT_CATEGORY_CODE = new String("merchant category code");
        public static final String MERCHANT_STATE = new String("merchant state");
        public static final String FRAUD_AMOUNT = new String("fraud amount");
        public static final String FRAUD_CURRENCY_CODE = new String("fraud currency code");
        public static final String VIC_PROCESSING_DATE = new String("VIC processing date");
        public static final String ISSUER_GENERATED_AUTH = new String("issuer generated authorization");
        public static final String NOTIFICATION_CODE = new String("notification code");
        public static final String ACCOUNT_SEQUENCE_NUMBER = new String("account sequence number");
        public static final String RESERVED = new String("reserved");
        public static final String FRAUD_TYPE = new String("fraud_type");
        public static final String CARD_EXPIRATION_DATE = new String("card expiration date");
        public static final String MERCHANT_POSTAL_CODE = new String("merchant postal code");
        public static final String FRAUD_INVEST_STATUS = new String("fraud investigation status");
        public static final String REIMBURSEMENT_ATTR = new String("reimbursement attribute");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr0ForFraud()
    {
        super(168, stream);
        putField(Field.TRANSACTION_CODE, "40");
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "0");
        putField(Field.DESTINATION_BIN, "400050");
        putField(Field.RESPONSE_CODE, "  ");
        putField(Field.VIC_PROCESSING_DATE, "0000");
        putField(Field.ISSUER_GENERATED_AUTH, " ");
        putField(Field.NOTIFICATION_CODE, "1");
        putField(Field.ACCOUNT_SEQUENCE_NUMBER, "9999");
        putField(Field.RESERVED, " ");
        putField(Field.FRAUD_TYPE, "9");
        putField(Field.FRAUD_INVEST_STATUS, "  ");
        putField(Field.REIMBURSEMENT_ATTR, "0");
    }

    public static void main(String args[])
    {
        Tcr0ForFraud tcr0 = new Tcr0ForFraud();
        System.out.println(tcr0.describeStructure());
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN(), 1);
        IStreamFormatter destination_bin = new StreamFormatterFieldFixed(Field.DESTINATION_BIN, Validator.getNs(), 6);
        IStreamFormatter source_bin = new StreamFormatterFieldFixed(Field.SOURCE_BIN, Validator.getNs(), 6);
        IStreamFormatter account_number = new StreamFormatterFieldFixed(Field.ACCOUNT_NUMBER, Validator.getN(), 23);
        IStreamFormatter acq_ref_nr = new StreamFormatterFieldFixed(Field.ACQUIRER_REFERENCE_NUMBER, Validator.getNs(), 23);
        IStreamFormatter acq_bus_id = new StreamFormatterFieldFixed(Field.ACQUIRER_BUSINESS_ID, Validator.getNs(), 8);
        IStreamFormatter rsp_code = new StreamFormatterFieldFixed(Field.RESPONSE_CODE, Validator.getAns(), 2);
        IStreamFormatter purchase_date = new StreamFormatterFieldFixed(Field.PURCHASE_DATE, Validator.getN(), 4);
        IStreamFormatter merchant_name = new StreamFormatterFieldFixed(Field.MERCHANT_NAME, Validator.getAns(), 25);
        IStreamFormatter merchant_city = new StreamFormatterFieldFixed(Field.MERCHANT_CITY, Validator.getAns(), 13);
        IStreamFormatter merchant_country_code = new StreamFormatterFieldFixed(Field.MERCHANT_COUNTRY_CODE, Validator.getAns(), 3);
        IStreamFormatter merchant_category_code = new StreamFormatterFieldFixed(Field.MERCHANT_CATEGORY_CODE, Validator.getNs(), 4);
        IStreamFormatter merchant_state = new StreamFormatterFieldFixed(Field.MERCHANT_STATE, Validator.getAns(), 3);
        IStreamFormatter fraud_amount = new StreamFormatterFieldFixed(Field.FRAUD_AMOUNT, Validator.getN(), 12, '0', false, false);
        IStreamFormatter fraud_currency_code = new StreamFormatterFieldFixed(Field.FRAUD_CURRENCY_CODE, Validator.getN(), 3);
        IStreamFormatter vic_processing_date = new StreamFormatterFieldFixed(Field.VIC_PROCESSING_DATE, Validator.getN(), 4);
        IStreamFormatter issuer_generated_auth = new StreamFormatterFieldFixed(Field.ISSUER_GENERATED_AUTH, Validator.getAns(), 1);
        IStreamFormatter notification_code = new StreamFormatterFieldFixed(Field.NOTIFICATION_CODE, Validator.getN(), 1);
        IStreamFormatter account_seq_number = new StreamFormatterFieldFixed(Field.ACCOUNT_SEQUENCE_NUMBER, Validator.getN(), 4);
        IStreamFormatter reserved = new StreamFormatterFieldFixed(Field.RESERVED, Validator.getAns(), 1);
        IStreamFormatter fraud_type = new StreamFormatterFieldFixed(Field.FRAUD_TYPE, Validator.getAns(), 1);
        IStreamFormatter card_expiration_date = new StreamFormatterFieldFixed(Field.CARD_EXPIRATION_DATE, Validator.getN(), 4);
        IStreamFormatter merchant_postal_code = new StreamFormatterFieldFixed(Field.MERCHANT_POSTAL_CODE, Validator.getAns(), 10);
        IStreamFormatter fraud_investigation_status = new StreamFormatterFieldFixed(Field.FRAUD_INVEST_STATUS, Validator.getAns(), 2);
        IStreamFormatter reimbursement_attr = new StreamFormatterFieldFixed(Field.REIMBURSEMENT_ATTR, Validator.getAns(), 1);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(destination_bin);
        msg.add(source_bin);
        msg.add(account_number);
        msg.add(acq_ref_nr);
        msg.add(acq_bus_id);
        msg.add(rsp_code);
        msg.add(purchase_date);
        msg.add(merchant_name);
        msg.add(merchant_city);
        msg.add(merchant_country_code);
        msg.add(merchant_category_code);
        msg.add(merchant_state);
        msg.add(fraud_amount);
        msg.add(fraud_currency_code);
        msg.add(vic_processing_date);
        msg.add(issuer_generated_auth);
        msg.add(notification_code);
        msg.add(account_seq_number);
        msg.add(reserved);
        msg.add(fraud_type);
        msg.add(card_expiration_date);
        msg.add(merchant_postal_code);
        msg.add(fraud_investigation_status);
        msg.add(reimbursement_attr);
        stream = msg;
    }
}

