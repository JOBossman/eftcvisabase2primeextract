package postilion.office.extract.visabase2;

//@author JBossman

public class Main
{
    public static void main(String[] args) throws Exception
    {
        String tcr0Data = "05004531880001638373000 N 74760766323002433392922000000001118000000000000   000000002000967Test_Merch               LUSAKA       ZM 701100000     1009N      5 2 050000B";
        String tcr1Data = "0501            000000                                                          MERCH_1285688_196629271000000000000  000000  0iS 0                           000000000  ";
        String tcr5Data = "050546726324302641100000007450096700        0101 000000074500                                                                                                          X";
        String tcr7Data = "050700000170920E0D8C8894        4DD69EE900973C006F5C1047C3187E0A010A008000800003A420020000000745000A02000000000031A40657                                                ";
        //----------------------
        //TEST FOR TCR 0
        //----------------------
        Tcr0 tcr0 = new Tcr0();
        tcr0.fromMsg(tcr0Data);
        System.out.println("MERCHANT_NAME: " + tcr0.getField(Tcr0.Field.MERCHANT_NAME));
        System.out.println("MERCHANT_COUNTRY_CODE: " + tcr0.getField(Tcr0.Field.MERCHANT_COUNTRY_CODE));
        System.out.println("MERCHANT_CITY: " + tcr0.getField(Tcr0.Field.MERCHANT_CITY));
        System.out.println("MERCHANT BUSINESS ID: " + tcr0.getField(Tcr0.Field.ACQUIRER_BUSS_ID));
        System.out.println("SOURCE CURRENCY CODE: " + tcr0.getField(Tcr0.Field.SOURCE_CURRENCY_CODE));
        System.out.println("DESTINATION CURRENCY CODE: " + tcr0.getField(Tcr0.Field.DESTINATION_CURRENCY_CODE));
        System.out.println("SOURCE_AMOUNT: " + tcr0.getField(Tcr0.Field.SOURCE_AMOUNT));
        System.out.println("DESINATION AMOUNT: " + tcr0.getField(Tcr0.Field.DESTINATION_AMOUNT));
        System.out.println("PURCHASE_DATE: " + tcr0.getField(Tcr0.Field.PURCHASE_DATE));
        System.out.println("REIMBURSEMENT_ATTR: " + tcr0.getField(Tcr0.Field.REIMBURSEMENT_ATTR));


        //----------------------
        //TEST FOR TCR 1
        //----------------------
        Tcr1 tcr1 = new Tcr1();
        tcr1.fromMsg(tcr1Data);
        System.out.println("CARD ACCEPTER ID CODE: " + tcr1.getField(Tcr1.Field.CARD_ACCEPTOR_ID_CODE));
        System.out.println("CARD ACCEPTER TERM ID: " + tcr1.getField(Tcr1.Field.CARD_ACCEPTOR_TERM_ID));
        System.out.println("IF_TRACE_NUMBER: " + tcr1.getField(Tcr1.Field.IF_TRACE_NUMBER));




        //----------------------
        //TEST FOR TCR 5
        //----------------------
        /*Tcr5 tcr5 = new Tcr5();
        tcr5.fromMsg(tcr5Data);
        System.out.println(tcr5.getField(Tcr5.Field.MULTIPLE_CLEARING_SEQ));
        System.out.println(tcr5.getField(Tcr5.Field.MULTIPLE_CLEARING_CNT));


        //----------------------
        //TEST FOR TCR 7
        //----------------------
        Tcr7 tcr7 = new Tcr7();
        tcr7.fromMsg(tcr7Data);
        System.out.println("COMPONENT_SEQUENCE: " + tcr7.getField(Tcr7.Field.COMPONENT_SEQUENCE));*/




    }
}
