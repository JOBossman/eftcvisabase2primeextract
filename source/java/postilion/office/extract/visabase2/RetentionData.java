package postilion.office.extract.visabase2;



import postilion.core.message.Validator;
import postilion.core.message.stream.*;

public final class RetentionData extends StreamMessage
{
    public static class Field
    {

        public static final String AUTH_SRC_CODE = "Authorisation source code";
        public static final String ADDR_VER_RES_CODE = "Address verification result code";
        public static final String TRANSACTION_ID = "Transaction id";
        public static final String VALIDATION_CODE = "Validation code";
        public static final String MARKET_ID = "Market specific id";
        public static final String RESPONSE_CODE = "Response code";
        public static final String POS_GEOGRAPHIC_DATA = "POS Geographic data";
        public static final String AUTH_CHAR_INDICATOR = "Auth char indicator";
        private static final String TERMINATOR = "Terminator";

        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    private static char FS;
    private static final int MAX_SIZE = 40;

    public RetentionData()
    {
        super(40, stream);
    }

    public static void main(String args1[])
    {
    }

    static
    {
        FS = '|';
        StreamFormatterFieldFixed auth_src_code = new StreamFormatterFieldFixed("Authorisation source code", Validator.getAns(), 1, ' ', true, true);
        StreamFormatterFieldFixed addr_ver_res_code = new StreamFormatterFieldFixed("Address verification result code", Validator.getAn(), 1, ' ', true, true);
        StreamFormatterFieldFixed trans_id = new StreamFormatterFieldFixed("Transaction id", Validator.getN(), 15, ' ', true, true);
        StreamFormatterFieldFixed val_code = new StreamFormatterFieldFixed("Validation code", Validator.getAns(), 4, ' ', true, true);
        StreamFormatterFieldFixed market_id = new StreamFormatterFieldFixed("Market specific id", Validator.getAns(), 1, ' ', true, true);
        StreamFormatterFieldFixed rsp_code = new StreamFormatterFieldFixed("Response code", Validator.getAns(), 2, ' ', true, true);
        StreamFormatterFieldVar pos_geographic_data = new StreamFormatterFieldVar("POS Geographic data", Validator.getAns(), 14, false, FS);
        StreamFormatterFieldFixed auth_char_indicator = new StreamFormatterFieldFixed("Auth char indicator", Validator.getAn(), 1, ' ', true, true);
        StreamFormatterPeek auth_char_indicator_peek = new StreamFormatterPeek(0, "0", null, auth_char_indicator, auth_char_indicator, "Auth char indicator", auth_char_indicator, null);
        StreamFormatterContainer retention_data = new StreamFormatterContainer();
        retention_data.add(auth_src_code);
        retention_data.add(addr_ver_res_code);
        retention_data.add(trans_id);
        retention_data.add(val_code);
        retention_data.add(market_id);
        retention_data.add(rsp_code);
        retention_data.add(pos_geographic_data);
        retention_data.add(auth_char_indicator_peek);
        stream = retention_data;
    }
}

