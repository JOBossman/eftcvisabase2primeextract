package postilion.office.extract.visabase2;

public interface IReimbursementAttribute
{

    public abstract String getReimbursementAttribute(Tcr0 tcr0);
}
