package postilion.office.extract.visabase2;


import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;
import postilion.core.util.Convert;

public class Tcr0ForTc50 extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String DESTINATION_BIN = new String("destination bin");
        public static final String SOURCE_BIN = new String("source bin");
        public static final String RESERVED1 = new String("reserved1");
        public static final String TEXT = new String("text");
        public static final String RESERVED2 = new String("reserved2");
        public static final String REIMBURSEMENT_ATTR = new String("reimbursement attribute");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr0ForTc50()
    {
        super(168, stream);
        putField(Field.TRANSACTION_CODE, "50");
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "0");
        putField(Field.DESTINATION_BIN, Convert.resize("", 6, '0', true));
        putField(Field.SOURCE_BIN, Convert.resize("", 6, '0', true));
        putField(Field.REIMBURSEMENT_ATTR, "0");
        putField(Field.RESERVED1, Convert.resize("", 6, ' ', true));
        putField(Field.RESERVED2, Convert.resize("", 38, ' ', true));
    }

    public static void main(String args[])
    {
        Tcr0ForTc50 tcr0 = new Tcr0ForTc50();
        System.out.println(tcr0.describeStructure());
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN(), 1);
        IStreamFormatter destination_bin = new StreamFormatterFieldFixed(Field.DESTINATION_BIN, Validator.getN(), 6);
        IStreamFormatter source_bin = new StreamFormatterFieldFixed(Field.SOURCE_BIN, Validator.getN(), 6);
        IStreamFormatter reserved1 = new StreamFormatterFieldFixed(Field.RESERVED1, Validator.getAns(), 6);
        IStreamFormatter text = new StreamFormatterFieldFixed(Field.TEXT, Validator.getAns(), 107, ' ', true, false);
        IStreamFormatter reserved2 = new StreamFormatterFieldFixed(Field.RESERVED2, Validator.getAns(), 38);
        IStreamFormatter reimbursement_attr = new StreamFormatterFieldFixed(Field.REIMBURSEMENT_ATTR, Validator.getAns(), 1);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(destination_bin);
        msg.add(source_bin);
        msg.add(reserved1);
        msg.add(text);
        msg.add(reserved2);
        msg.add(reimbursement_attr);
        stream = msg;
    }
}

