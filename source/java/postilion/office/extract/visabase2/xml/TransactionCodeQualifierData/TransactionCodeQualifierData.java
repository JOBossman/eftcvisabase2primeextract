package postilion.office.extract.visabase2.xml.TransactionCodeQualifierData;


import com.vsi.xml.*;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.Vector;

public class TransactionCodeQualifierData
        implements XmlObject, Validateable, Serializable
{

    protected String _TransactionCodeQualifier;

    public void fromXml(XmlElement xml)
    {
        if(xml == null)
        {
            return;
        }
        if(!"TransactionCodeQualifierData".equals(xml.getName()))
        {
            xml = xml.getSubElement("TransactionCodeQualifierData");
            if(xml == null)
            {
                return;
            }
        }
        setTransactionCodeQualifier(xml.getData("TransactionCodeQualifier"));
    }

    public String getTransactionCodeQualifier()
    {
        return _TransactionCodeQualifier;
    }

    public void setTransactionCodeQualifier(String newValue)
    {
        _TransactionCodeQualifier = newValue;
    }

    public void toXml(OutputStream stream, String indent, boolean embed_files)
    {
        if(stream instanceof XmlOutputStream)
        {
            toXml(stream, embed_files);
        } else
        {
            FormattedOutputStream out = new FormattedOutputStream(stream);
            out.setIndentString(indent);
            toXml(((OutputStream) (out)), embed_files);
        }
    }

    public void toXml(OutputStream stream, boolean embed_files)
    {
        XmlOutputStream out = null;
        if(stream instanceof XmlOutputStream)
        {
            out = (XmlOutputStream)stream;
        } else
        {
            out = new RawOutputStream(stream);
        }
        out.writeStartTag("TransactionCodeQualifierData", false);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).incrementIndent();
        }
        out.write("TransactionCodeQualifier", _TransactionCodeQualifier);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).decrementIndent();
        }
        out.writeEndTag("TransactionCodeQualifierData");
    }

    public boolean isValid()
    {
        if(!(this instanceof Validateable))
        {
            return true;
        }
        Vector errors = getValidationErrors(true);
        return errors == null || errors.size() < 1;
    }

    public String birthCertificate()
    {
        return "1hge8b9:gdvifd04:1jqfnci";
    }

    public TransactionCodeQualifierData()
    {
        _TransactionCodeQualifier = null;
    }

    public TransactionCodeQualifierData(XmlElement root)
    {
        _TransactionCodeQualifier = null;
        fromXml(root);
    }

    public String getXmlTagName()
    {
        return "TransactionCodeQualifierData";
    }

    public static String getClassXmlTagName()
    {
        return "TransactionCodeQualifierData";
    }

    public Vector getValidationErrors()
    {
        return getValidationErrors(false);
    }

    public Vector getValidationErrors(boolean return_on_error)
    {
        Vector errors = new Vector();
        if(errors.size() < 1)
        {
            return null;
        } else
        {
            return errors;
        }
    }
}
