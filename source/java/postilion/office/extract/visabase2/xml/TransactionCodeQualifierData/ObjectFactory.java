package postilion.office.extract.visabase2.xml.TransactionCodeQualifierData;


import com.vsi.xml.XmlDbObject;
import com.vsi.xml.XmlElement;
import com.vsi.xml.XmlObject;
import com.vsi.xml.XmlObjectFactory;

import java.io.File;
import java.io.InputStream;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;

public class ObjectFactory extends XmlObjectFactory
{

    protected static Hashtable _default_map;
    protected static Hashtable _default_dbmap = new Hashtable();
    protected static ObjectFactory _default_factory = getInstance();

    public static Exception getDefaultFactoryLastException()
    {
        return _default_factory.getLastException();
    }

    public static XmlObject createObject(String name, XmlElement xml)
    {
        XmlObjectFactory factory = xml.getObjectFactory();
        if(factory != null)
        {
            return factory.getInstance(name, xml);
        } else
        {
            return _default_factory.getInstance(name, xml);
        }
    }

    public static XmlObject createObject(InputStream in)
    {
        return _default_factory.getInstance(in);
    }

    public static XmlObject createObject(XmlElement xml)
    {
        XmlObjectFactory factory = xml.getObjectFactory();
        if(factory != null)
        {
            return factory.getInstance(xml);
        } else
        {
            return _default_factory.getInstance(xml);
        }
    }

    public static XmlObject createObject(File f)
    {
        return _default_factory.getInstance(f);
    }

    public static XmlObject createObject(String filename)
    {
        return _default_factory.getInstance(filename);
    }

    public static XmlDbObject createObject(ResultSet rs)
    {
        return _default_factory.getInstance(rs);
    }

    protected ObjectFactory()
    {
    }

    public static String getDefaultFactoryLastError()
    {
        return _default_factory.getLastError();
    }

    public static void setDefaultFactoryXmlBinding(String name, String class_name)
    {
        _default_factory.setXmlBinding(name, class_name);
    }

    public static XmlObject createObjectFromUrl(String url)
    {
        return _default_factory.getInstanceFromUrl(url);
    }

    public static Vector createList(XmlElement xml)
    {
        XmlObjectFactory factory = xml.getObjectFactory();
        if(factory != null)
        {
            return factory.getList(xml);
        } else
        {
            return _default_factory.getList(xml);
        }
    }

    public static Vector createList(ResultSet rs)
    {
        return _default_factory.getList(rs);
    }

    public static XmlObject createObjectFromString(String xml)
    {
        return _default_factory.getInstanceFromString(xml);
    }

    public static ObjectFactory getInstance()
    {
        ObjectFactory factory = new ObjectFactory();
        factory.setXmlMap(_default_map);
        factory.setDbMap(_default_dbmap);
        return factory;
    }

    static
    {
        _default_map = new Hashtable();
        _default_map.put("TransactionCodeQualifierData", "postilion.office.extract.primeBase2.xml.TransactionCodeQualifierData.TransactionC" +
                "odeQualifierData"
        );
    }
}
