package postilion.office.extract.visabase2.xml.BaseI;


import com.vsi.xml.*;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.Vector;

// Referenced classes of package postilion.office.extract.visabase2.xml.BaseI:
//            ObjectFactory, ProcessingCodeData

public class BaseI
        implements XmlObject, Validateable, Serializable
{

    protected ProcessingCodeData _ProcessingCodeData;
    protected String _CardLevelResults;

    public void fromXml(XmlElement xml)
    {
        if(xml == null)
        {
            return;
        }
        if(!"BaseI".equals(xml.getName()))
        {
            XmlObjectFactory factory = xml.getObjectFactory();
            xml = xml.getSubElement("BaseI");
            if(xml == null)
            {
                return;
            }
            xml.setObjectFactory(factory);
        }
        setProcessingCodeData(xml);
        setCardLevelResults(xml.getData("CardLevelResults"));
    }

    public void toXml(OutputStream stream, String indent, boolean embed_files)
    {
        if(stream instanceof XmlOutputStream)
        {
            toXml(stream, embed_files);
        } else
        {
            FormattedOutputStream out = new FormattedOutputStream(stream);
            out.setIndentString(indent);
            toXml(((OutputStream) (out)), embed_files);
        }
    }

    public void toXml(OutputStream stream, boolean embed_files)
    {
        XmlOutputStream out = null;
        if(stream instanceof XmlOutputStream)
        {
            out = (XmlOutputStream)stream;
        } else
        {
            out = new RawOutputStream(stream);
        }
        out.writeStartTag("BaseI", false);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).incrementIndent();
        }
        Object ProcessingCodeData_value = getProcessingCodeData();
        if(ProcessingCodeData_value instanceof XmlObject)
        {
            out.write(null, (XmlObject)ProcessingCodeData_value, embed_files);
        }
        out.write("CardLevelResults", _CardLevelResults);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).decrementIndent();
        }
        out.writeEndTag("BaseI");
    }

    public boolean isValid()
    {
        if(!(this instanceof Validateable))
        {
            return true;
        }
        Vector errors = getValidationErrors(true);
        return errors == null || errors.size() < 1;
    }

    public String birthCertificate()
    {
        return "1hge8b9:gdvifdpq:1gwis9t";
    }

    public BaseI()
    {
        _ProcessingCodeData = null;
        _CardLevelResults = null;
    }

    public BaseI(XmlElement root)
    {
        _ProcessingCodeData = null;
        _CardLevelResults = null;
        fromXml(root);
    }

    public String getXmlTagName()
    {
        return "BaseI";
    }

    public String getCardLevelResults()
    {
        return _CardLevelResults;
    }

    public void setCardLevelResults(String newValue)
    {
        _CardLevelResults = newValue;
    }

    public ProcessingCodeData getProcessingCodeData()
    {
        return _ProcessingCodeData;
    }

    public void setProcessingCodeData(ProcessingCodeData obj)
    {
        _ProcessingCodeData = obj;
    }

    public void setProcessingCodeData(XmlElement xml)
    {
        if(xml == null)
        {
            _ProcessingCodeData = null;
            return;
        }
        XmlObjectFactory factory = xml.getObjectFactory();
        if(!"ProcessingCodeData".equals(xml.getName()))
        {
            xml = xml.getSubElement("ProcessingCodeData");
            if(xml == null)
            {
                _ProcessingCodeData = null;
                return;
            }
        }
        _ProcessingCodeData = (ProcessingCodeData) ObjectFactory.createObject(xml);
    }

    public static String getClassXmlTagName()
    {
        return "BaseI";
    }

    public Vector getValidationErrors()
    {
        return getValidationErrors(false);
    }

    public Vector getValidationErrors(boolean return_on_error)
    {
        Vector errors = new Vector();
        if(!XmlValidator.isValid(_ProcessingCodeData, "ProcessingCodeData", "Element", "BaseI->ProcessingCodeData", true, errors, return_on_error) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_CardLevelResults, "CardLevelResults", "Element", "BaseI->CardLevelResults", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(errors.size() < 1)
        {
            return null;
        } else
        {
            return errors;
        }
    }
}
