package postilion.office.extract.visabase2.xml.BaseI;

import com.vsi.xml.*;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.Vector;

public class ProcessingCodeData
        implements XmlObject, Validateable, Serializable
{

    protected String _TransactionType;
    protected String _FromAccount;
    protected String _ToAccount;

    public void fromXml(XmlElement xml)
    {
        if(xml == null)
        {
            return;
        }
        if(!"ProcessingCodeData".equals(xml.getName()))
        {
            xml = xml.getSubElement("ProcessingCodeData");
            if(xml == null)
            {
                return;
            }
        }
        setTransactionType(xml.getData("TransactionType"));
        setFromAccount(xml.getData("FromAccount"));
        setToAccount(xml.getData("ToAccount"));
    }

    public void toXml(OutputStream stream, String indent, boolean embed_files)
    {
        if(stream instanceof XmlOutputStream)
        {
            toXml(stream, embed_files);
        } else
        {
            FormattedOutputStream out = new FormattedOutputStream(stream);
            out.setIndentString(indent);
            toXml(((OutputStream) (out)), embed_files);
        }
    }

    public void toXml(OutputStream stream, boolean embed_files)
    {
        XmlOutputStream out = null;
        if(stream instanceof XmlOutputStream)
        {
            out = (XmlOutputStream)stream;
        } else
        {
            out = new RawOutputStream(stream);
        }
        out.writeStartTag("ProcessingCodeData", false);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).incrementIndent();
        }
        out.write("TransactionType", _TransactionType);
        out.write("FromAccount", _FromAccount);
        out.write("ToAccount", _ToAccount);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).decrementIndent();
        }
        out.writeEndTag("ProcessingCodeData");
    }

    public String getToAccount()
    {
        return _ToAccount;
    }

    public boolean isValid()
    {
        if(!(this instanceof Validateable))
        {
            return true;
        }
        Vector errors = getValidationErrors(true);
        return errors == null || errors.size() < 1;
    }

    public void setToAccount(String newValue)
    {
        _ToAccount = newValue;
    }

    public String birthCertificate()
    {
        return "1hge8b9:gdvifdsc:sf5oes";
    }

    public ProcessingCodeData()
    {
        _TransactionType = null;
        _FromAccount = null;
        _ToAccount = null;
    }

    public ProcessingCodeData(XmlElement root)
    {
        _TransactionType = null;
        _FromAccount = null;
        _ToAccount = null;
        fromXml(root);
    }

    public String getXmlTagName()
    {
        return "ProcessingCodeData";
    }

    public String getTransactionType()
    {
        return _TransactionType;
    }

    public void setTransactionType(String newValue)
    {
        _TransactionType = newValue;
    }

    public static String getClassXmlTagName()
    {
        return "ProcessingCodeData";
    }

    public Vector getValidationErrors()
    {
        return getValidationErrors(false);
    }

    public Vector getValidationErrors(boolean return_on_error)
    {
        Vector errors = new Vector();
        if(!XmlValidator.isValid(_TransactionType, "TransactionType", "Element", "ProcessingCodeData->TransactionType", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_FromAccount, "FromAccount", "Element", "ProcessingCodeData->FromAccount", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_ToAccount, "ToAccount", "Element", "ProcessingCodeData->ToAccount", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(errors.size() < 1)
        {
            return null;
        } else
        {
            return errors;
        }
    }

    public String getFromAccount()
    {
        return _FromAccount;
    }

    public void setFromAccount(String newValue)
    {
        _FromAccount = newValue;
    }
}

