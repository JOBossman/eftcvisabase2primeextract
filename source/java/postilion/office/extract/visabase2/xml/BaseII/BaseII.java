package postilion.office.extract.visabase2.xml.BaseII;


import com.vsi.xml.*;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.Vector;

public class BaseII
        implements XmlObject, Validateable, Serializable
{

    protected String _ChargebackRefNr;
    protected String _TranId;
    protected String _MultiClearingSeqNr;
    protected String _AddDataIndicator;
    protected String _POSGeoData;
    protected String _ReqPaymentService;
    protected String _ReimbursementAttr;
    protected String _IntFeeInd;
    protected String _AuthSourceCode;
    protected String _AVSRspCode;
    protected String _MarketSpecAuth;
    protected String _AuthRspCode;
    protected String _AcquirerRefNr;

    public void fromXml(XmlElement xml)
    {
        if(xml == null)
        {
            return;
        }
        if(!"BaseII".equals(xml.getName()))
        {
            xml = xml.getSubElement("BaseII");
            if(xml == null)
            {
                return;
            }
        }
        setChargebackRefNr(xml.getData("ChargebackRefNr"));
        setTranId(xml.getData("TranId"));
        setMultiClearingSeqNr(xml.getData("MultiClearingSeqNr"));
        setAddDataIndicator(xml.getData("AddDataIndicator"));
        setPOSGeoData(xml.getData("POSGeoData"));
        setReqPaymentService(xml.getData("ReqPaymentService"));
        setReimbursementAttr(xml.getData("ReimbursementAttr"));
        setIntFeeInd(xml.getData("IntFeeInd"));
        setAuthSourceCode(xml.getData("AuthSourceCode"));
        setAVSRspCode(xml.getData("AVSRspCode"));
        setMarketSpecAuth(xml.getData("MarketSpecAuth"));
        setAuthRspCode(xml.getData("AuthRspCode"));
        setAcquirerRefNr(xml.getData("AcquirerRefNr"));
    }

    public String getReimbursementAttr()
    {
        return _ReimbursementAttr;
    }

    public void setReimbursementAttr(String newValue)
    {
        _ReimbursementAttr = newValue;
    }

    public void toXml(OutputStream stream, String indent, boolean embed_files)
    {
        if(stream instanceof XmlOutputStream)
        {
            toXml(stream, embed_files);
        } else
        {
            FormattedOutputStream out = new FormattedOutputStream(stream);
            out.setIndentString(indent);
            toXml(((OutputStream) (out)), embed_files);
        }
    }

    public void toXml(OutputStream stream, boolean embed_files)
    {
        XmlOutputStream out = null;
        if(stream instanceof XmlOutputStream)
        {
            out = (XmlOutputStream)stream;
        } else
        {
            out = new RawOutputStream(stream);
        }
        out.writeStartTag("BaseII", false);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).incrementIndent();
        }
        out.write("ChargebackRefNr", _ChargebackRefNr);
        out.write("TranId", _TranId);
        out.write("MultiClearingSeqNr", _MultiClearingSeqNr);
        out.write("AddDataIndicator", _AddDataIndicator);
        out.write("POSGeoData", _POSGeoData);
        out.write("ReqPaymentService", _ReqPaymentService);
        out.write("ReimbursementAttr", _ReimbursementAttr);
        out.write("IntFeeInd", _IntFeeInd);
        out.write("AuthSourceCode", _AuthSourceCode);
        out.write("AVSRspCode", _AVSRspCode);
        out.write("MarketSpecAuth", _MarketSpecAuth);
        out.write("AuthRspCode", _AuthRspCode);
        out.write("AcquirerRefNr", _AcquirerRefNr);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).decrementIndent();
        }
        out.writeEndTag("BaseII");
    }

    public String getAVSRspCode()
    {
        return _AVSRspCode;
    }

    public void setAVSRspCode(String newValue)
    {
        _AVSRspCode = newValue;
    }

    public String getMarketSpecAuth()
    {
        return _MarketSpecAuth;
    }

    public void setMarketSpecAuth(String newValue)
    {
        _MarketSpecAuth = newValue;
    }

    public static String getClassXmlTagName()
    {
        return "BaseII";
    }

    public String getAuthSourceCode()
    {
        return _AuthSourceCode;
    }

    public void setAuthSourceCode(String newValue)
    {
        _AuthSourceCode = newValue;
    }

    public String getChargebackRefNr()
    {
        return _ChargebackRefNr;
    }

    public void setChargebackRefNr(String newValue)
    {
        _ChargebackRefNr = newValue;
    }

    public String getAcquirerRefNr()
    {
        return _AcquirerRefNr;
    }

    public void setAcquirerRefNr(String newValue)
    {
        _AcquirerRefNr = newValue;
    }

    public boolean isValid()
    {
        if(!(this instanceof Validateable))
        {
            return true;
        }
        Vector errors = getValidationErrors(true);
        return errors == null || errors.size() < 1;
    }

    public String birthCertificate()
    {
        return "1hge8b9:gdviff7k:d24f88";
    }

    public BaseII()
    {
        _ChargebackRefNr = null;
        _TranId = null;
        _MultiClearingSeqNr = null;
        _AddDataIndicator = null;
        _POSGeoData = null;
        _ReqPaymentService = null;
        _ReimbursementAttr = null;
        _IntFeeInd = null;
        _AuthSourceCode = null;
        _AVSRspCode = null;
        _MarketSpecAuth = null;
        _AuthRspCode = null;
        _AcquirerRefNr = null;
    }

    public BaseII(XmlElement root)
    {
        _ChargebackRefNr = null;
        _TranId = null;
        _MultiClearingSeqNr = null;
        _AddDataIndicator = null;
        _POSGeoData = null;
        _ReqPaymentService = null;
        _ReimbursementAttr = null;
        _IntFeeInd = null;
        _AuthSourceCode = null;
        _AVSRspCode = null;
        _MarketSpecAuth = null;
        _AuthRspCode = null;
        _AcquirerRefNr = null;
        fromXml(root);
    }

    public String getAuthRspCode()
    {
        return _AuthRspCode;
    }

    public void setAuthRspCode(String newValue)
    {
        _AuthRspCode = newValue;
    }

    public String getXmlTagName()
    {
        return "BaseII";
    }

    public String getMultiClearingSeqNr()
    {
        return _MultiClearingSeqNr;
    }

    public void setMultiClearingSeqNr(String newValue)
    {
        _MultiClearingSeqNr = newValue;
    }

    public String getTranId()
    {
        return _TranId;
    }

    public void setTranId(String newValue)
    {
        _TranId = newValue;
    }

    public String getReqPaymentService()
    {
        return _ReqPaymentService;
    }

    public String getPOSGeoData()
    {
        return _POSGeoData;
    }

    public void setPOSGeoData(String newValue)
    {
        _POSGeoData = newValue;
    }

    public void setReqPaymentService(String newValue)
    {
        _ReqPaymentService = newValue;
    }

    public Vector getValidationErrors()
    {
        return getValidationErrors(false);
    }

    public Vector getValidationErrors(boolean return_on_error)
    {
        Vector errors = new Vector();
        if(!XmlValidator.isValid(_ChargebackRefNr, "ChargebackRefNr", "Element", "BaseII->ChargebackRefNr", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_TranId, "TranId", "Element", "BaseII->TranId", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_MultiClearingSeqNr, "MultiClearingSeqNr", "Element", "BaseII->MultiClearingSeqNr", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_AddDataIndicator, "AddDataIndicator", "Element", "BaseII->AddDataIndicator", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_POSGeoData, "POSGeoData", "Element", "BaseII->POSGeoData", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_ReqPaymentService, "ReqPaymentService", "Element", "BaseII->ReqPaymentService", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_ReimbursementAttr, "ReimbursementAttr", "Element", "BaseII->ReimbursementAttr", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_IntFeeInd, "IntFeeInd", "Element", "BaseII->IntFeeInd", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_AuthSourceCode, "AuthSourceCode", "Element", "BaseII->AuthSourceCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_AVSRspCode, "AVSRspCode", "Element", "BaseII->AVSRspCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_MarketSpecAuth, "MarketSpecAuth", "Element", "BaseII->MarketSpecAuth", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_AuthRspCode, "AuthRspCode", "Element", "BaseII->AuthRspCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_AcquirerRefNr, "AcquirerRefNr", "Element", "BaseII->AcquirerRefNr", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(errors.size() < 1)
        {
            return null;
        } else
        {
            return errors;
        }
    }

    public String getIntFeeInd()
    {
        return _IntFeeInd;
    }

    public void setIntFeeInd(String newValue)
    {
        _IntFeeInd = newValue;
    }

    public String getAddDataIndicator()
    {
        return _AddDataIndicator;
    }

    public void setAddDataIndicator(String newValue)
    {
        _AddDataIndicator = newValue;
    }
}
