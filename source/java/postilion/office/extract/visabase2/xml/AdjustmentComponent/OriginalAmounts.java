package postilion.office.extract.visabase2.xml.AdjustmentComponent;


import com.vsi.xml.*;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.Vector;

public class OriginalAmounts
        implements XmlObject, Validateable, Serializable
{

    protected String _TranAmount;
    protected String _SettleAmount;
    protected String _TranCurrencyCode;
    protected String _SettleCurrencyCode;

    public void fromXml(XmlElement xml)
    {
        if(xml == null)
        {
            return;
        }
        if(!"OriginalAmounts".equals(xml.getName()))
        {
            xml = xml.getSubElement("OriginalAmounts");
            if(xml == null)
            {
                return;
            }
        }
        setTranAmount(xml.getData("TranAmount"));
        setSettleAmount(xml.getData("SettleAmount"));
        setTranCurrencyCode(xml.getData("TranCurrencyCode"));
        setSettleCurrencyCode(xml.getData("SettleCurrencyCode"));
    }

    public void toXml(OutputStream stream, String indent, boolean embed_files)
    {
        if(stream instanceof XmlOutputStream)
        {
            toXml(stream, embed_files);
        } else
        {
            FormattedOutputStream out = new FormattedOutputStream(stream);
            out.setIndentString(indent);
            toXml(((OutputStream) (out)), embed_files);
        }
    }

    public void toXml(OutputStream stream, boolean embed_files)
    {
        XmlOutputStream out = null;
        if(stream instanceof XmlOutputStream)
        {
            out = (XmlOutputStream)stream;
        } else
        {
            out = new RawOutputStream(stream);
        }
        out.writeStartTag("OriginalAmounts", false);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).incrementIndent();
        }
        out.write("TranAmount", _TranAmount);
        out.write("SettleAmount", _SettleAmount);
        out.write("TranCurrencyCode", _TranCurrencyCode);
        out.write("SettleCurrencyCode", _SettleCurrencyCode);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).decrementIndent();
        }
        out.writeEndTag("OriginalAmounts");
    }

    public String getSettleAmount()
    {
        return _SettleAmount;
    }

    public boolean isValid()
    {
        if(!(this instanceof Validateable))
        {
            return true;
        }
        Vector errors = getValidationErrors(true);
        return errors == null || errors.size() < 1;
    }

    public void setSettleAmount(String newValue)
    {
        _SettleAmount = newValue;
    }

    public String birthCertificate()
    {
        return "1hge8b9:gdvifhnn:qm93j1";
    }

    public OriginalAmounts()
    {
        _TranAmount = null;
        _SettleAmount = null;
        _TranCurrencyCode = null;
        _SettleCurrencyCode = null;
    }

    public OriginalAmounts(XmlElement root)
    {
        _TranAmount = null;
        _SettleAmount = null;
        _TranCurrencyCode = null;
        _SettleCurrencyCode = null;
        fromXml(root);
    }

    public String getSettleCurrencyCode()
    {
        return _SettleCurrencyCode;
    }

    public void setSettleCurrencyCode(String newValue)
    {
        _SettleCurrencyCode = newValue;
    }

    public String getXmlTagName()
    {
        return "OriginalAmounts";
    }

    public String getTranAmount()
    {
        return _TranAmount;
    }

    public void setTranAmount(String newValue)
    {
        _TranAmount = newValue;
    }

    public static String getClassXmlTagName()
    {
        return "OriginalAmounts";
    }

    public Vector getValidationErrors()
    {
        return getValidationErrors(false);
    }

    public Vector getValidationErrors(boolean return_on_error)
    {
        Vector errors = new Vector();
        if(!XmlValidator.isValid(_TranAmount, "TranAmount", "Element", "OriginalAmounts->TranAmount", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_SettleAmount, "SettleAmount", "Element", "OriginalAmounts->SettleAmount", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_TranCurrencyCode, "TranCurrencyCode", "Element", "OriginalAmounts->TranCurrencyCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_SettleCurrencyCode, "SettleCurrencyCode", "Element", "OriginalAmounts->SettleCurrencyCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(errors.size() < 1)
        {
            return null;
        } else
        {
            return errors;
        }
    }

    public String getTranCurrencyCode()
    {
        return _TranCurrencyCode;
    }

    public void setTranCurrencyCode(String newValue)
    {
        _TranCurrencyCode = newValue;
    }
}
