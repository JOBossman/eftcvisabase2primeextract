package postilion.office.extract.visabase2.xml.AdjustmentComponent;

import com.vsi.xml.*;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.Vector;

// Referenced classes of package postilion.office.extract.visabase2.xml.AdjustmentComponent:
//            ObjectFactory, OriginalAmounts

public class AdjustmentComponent
        implements XmlObject, Validateable, Serializable
{

    protected OriginalAmounts _OriginalAmounts;
    protected String _ProcessingCode;
    protected String _RetrievalDocumentCode;
    protected String _AcquirerRefNr;
    protected String _IssuerRefNr;
    protected String _FeeCollectionControlNr;
    protected String _DocumentationIndicator;
    protected String _MessageText;
    protected String _FulfillmentDocumentCode;
    protected String _MasterCardFunctionCode;
    protected String _DestinationInstitutionId;
    protected String _OriginatorInstitutionId;
    protected String _AdditionalData;
    protected String _DateAction;
    protected String _OriginalRetrievalReason;
    protected String _AcquiringInstIdCode;
    protected String _AcquiringInstCountryCode;
    protected String _ForwardingInstIdCode;
    protected String _ForwardingInstCountryCode;
    protected String _ReceivingInstCountryCode;
    protected String _ReceivingInstIdCode;
    protected String _AuthIdRsp;
    protected String _Role;
    protected String _MsgId;
    protected String _MsgMatching;
    protected String _RejectFields;
    protected String _RejectCodes;
    protected String _Correction;
    protected String _PreviousPostTranID;

    public String getDestinationInstitutionId()
    {
        return _DestinationInstitutionId;
    }

    public void setDestinationInstitutionId(String newValue)
    {
        _DestinationInstitutionId = newValue;
    }

    public String getOriginalRetrievalReason()
    {
        return _OriginalRetrievalReason;
    }

    public void setOriginalRetrievalReason(String newValue)
    {
        _OriginalRetrievalReason = newValue;
    }

    public String getPreviousPostTranID()
    {
        return _PreviousPostTranID;
    }

    public void setPreviousPostTranID(String newValue)
    {
        _PreviousPostTranID = newValue;
    }

    public String getRetrievalDocumentCode()
    {
        return _RetrievalDocumentCode;
    }

    public void setRetrievalDocumentCode(String newValue)
    {
        _RetrievalDocumentCode = newValue;
    }

    public String getMasterCardFunctionCode()
    {
        return _MasterCardFunctionCode;
    }

    public void setMasterCardFunctionCode(String newValue)
    {
        _MasterCardFunctionCode = newValue;
    }

    public String getForwardingInstCountryCode()
    {
        return _ForwardingInstCountryCode;
    }

    public void setForwardingInstCountryCode(String newValue)
    {
        _ForwardingInstCountryCode = newValue;
    }

    public String getMsgId()
    {
        return _MsgId;
    }

    public void setMsgId(String newValue)
    {
        _MsgId = newValue;
    }

    public boolean isValid()
    {
        if(!(this instanceof Validateable))
        {
            return true;
        }
        Vector errors = getValidationErrors(true);
        return errors == null || errors.size() < 1;
    }

    public String birthCertificate()
    {
        return "1hge8b9:gdvifhiw:1up6i57";
    }

    public String getOriginatorInstitutionId()
    {
        return _OriginatorInstitutionId;
    }

    public void setOriginatorInstitutionId(String newValue)
    {
        _OriginatorInstitutionId = newValue;
    }

    public OriginalAmounts getOriginalAmounts()
    {
        return _OriginalAmounts;
    }

    public String getXmlTagName()
    {
        return "AdjustmentComponent";
    }

    public void setOriginalAmounts(OriginalAmounts obj)
    {
        _OriginalAmounts = obj;
    }

    public void setOriginalAmounts(XmlElement xml)
    {
        if(xml == null)
        {
            _OriginalAmounts = null;
            return;
        }
        XmlObjectFactory factory = xml.getObjectFactory();
        if(!"OriginalAmounts".equals(xml.getName()))
        {
            xml = xml.getSubElement("OriginalAmounts");
            if(xml == null)
            {
                _OriginalAmounts = null;
                return;
            }
        }
        _OriginalAmounts = (OriginalAmounts) ObjectFactory.createObject(xml);
    }

    public String getAdditionalData()
    {
        return _AdditionalData;
    }

    public void setAdditionalData(String newValue)
    {
        _AdditionalData = newValue;
    }

    public String getDocumentationIndicator()
    {
        return _DocumentationIndicator;
    }

    public void setDocumentationIndicator(String newValue)
    {
        _DocumentationIndicator = newValue;
    }

    public String getCorrection()
    {
        return _Correction;
    }

    public void setCorrection(String newValue)
    {
        _Correction = newValue;
    }

    public Vector getValidationErrors()
    {
        return getValidationErrors(false);
    }

    public Vector getValidationErrors(boolean return_on_error)
    {
        Vector errors = new Vector();
        if(!XmlValidator.isValid(_OriginalAmounts, "OriginalAmounts", "Element", "AdjustmentComponent->OriginalAmounts", true, errors, return_on_error) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_ProcessingCode, "ProcessingCode", "Element", "AdjustmentComponent->ProcessingCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_RetrievalDocumentCode, "RetrievalDocumentCode", "Element", "AdjustmentComponent->RetrievalDocumentCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_AcquirerRefNr, "AcquirerRefNr", "Element", "AdjustmentComponent->AcquirerRefNr", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_IssuerRefNr, "IssuerRefNr", "Element", "AdjustmentComponent->IssuerRefNr", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_FeeCollectionControlNr, "FeeCollectionControlNr", "Element", "AdjustmentComponent->FeeCollectionControlNr", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_DocumentationIndicator, "DocumentationIndicator", "Element", "AdjustmentComponent->DocumentationIndicator", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_MessageText, "MessageText", "Element", "AdjustmentComponent->MessageText", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_FulfillmentDocumentCode, "FulfillmentDocumentCode", "Element", "AdjustmentComponent->FulfillmentDocumentCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_MasterCardFunctionCode, "MasterCardFunctionCode", "Element", "AdjustmentComponent->MasterCardFunctionCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_DestinationInstitutionId, "DestinationInstitutionId", "Element", "AdjustmentComponent->DestinationInstitutionId", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_OriginatorInstitutionId, "OriginatorInstitutionId", "Element", "AdjustmentComponent->OriginatorInstitutionId", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_AdditionalData, "AdditionalData", "Element", "AdjustmentComponent->AdditionalData", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_DateAction, "DateAction", "Element", "AdjustmentComponent->DateAction", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_OriginalRetrievalReason, "OriginalRetrievalReason", "Element", "AdjustmentComponent->OriginalRetrievalReason", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_AcquiringInstIdCode, "AcquiringInstIdCode", "Element", "AdjustmentComponent->AcquiringInstIdCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_AcquiringInstCountryCode, "AcquiringInstCountryCode", "Element", "AdjustmentComponent->AcquiringInstCountryCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_ForwardingInstIdCode, "ForwardingInstIdCode", "Element", "AdjustmentComponent->ForwardingInstIdCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_ForwardingInstCountryCode, "ForwardingInstCountryCode", "Element", "AdjustmentComponent->ForwardingInstCountryCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_ReceivingInstCountryCode, "ReceivingInstCountryCode", "Element", "AdjustmentComponent->ReceivingInstCountryCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_ReceivingInstIdCode, "ReceivingInstIdCode", "Element", "AdjustmentComponent->ReceivingInstIdCode", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_AuthIdRsp, "AuthIdRsp", "Element", "AdjustmentComponent->AuthIdRsp", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_Role, "Role", "Element", "AdjustmentComponent->Role", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_MsgId, "MsgId", "Element", "AdjustmentComponent->MsgId", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_MsgMatching, "MsgMatching", "Element", "AdjustmentComponent->MsgMatching", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_RejectFields, "RejectFields", "Element", "AdjustmentComponent->RejectFields", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_RejectCodes, "RejectCodes", "Element", "AdjustmentComponent->RejectCodes", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_Correction, "Correction", "Element", "AdjustmentComponent->Correction", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(!XmlValidator.isValid(_PreviousPostTranID, "PreviousPostTranID", "Element", "AdjustmentComponent->PreviousPostTranID", -1, true, errors) && return_on_error)
        {
            return errors;
        }
        if(errors.size() < 1)
        {
            return null;
        } else
        {
            return errors;
        }
    }

    public void fromXml(XmlElement xml)
    {
        if(xml == null)
        {
            return;
        }
        if(!"AdjustmentComponent".equals(xml.getName()))
        {
            XmlObjectFactory factory = xml.getObjectFactory();
            xml = xml.getSubElement("AdjustmentComponent");
            if(xml == null)
            {
                return;
            }
            xml.setObjectFactory(factory);
        }
        setOriginalAmounts(xml);
        setProcessingCode(xml.getData("ProcessingCode"));
        setRetrievalDocumentCode(xml.getData("RetrievalDocumentCode"));
        setAcquirerRefNr(xml.getData("AcquirerRefNr"));
        setIssuerRefNr(xml.getData("IssuerRefNr"));
        setFeeCollectionControlNr(xml.getData("FeeCollectionControlNr"));
        setDocumentationIndicator(xml.getData("DocumentationIndicator"));
        setMessageText(xml.getData("MessageText"));
        setFulfillmentDocumentCode(xml.getData("FulfillmentDocumentCode"));
        setMasterCardFunctionCode(xml.getData("MasterCardFunctionCode"));
        setDestinationInstitutionId(xml.getData("DestinationInstitutionId"));
        setOriginatorInstitutionId(xml.getData("OriginatorInstitutionId"));
        setAdditionalData(xml.getData("AdditionalData"));
        setDateAction(xml.getData("DateAction"));
        setOriginalRetrievalReason(xml.getData("OriginalRetrievalReason"));
        setAcquiringInstIdCode(xml.getData("AcquiringInstIdCode"));
        setAcquiringInstCountryCode(xml.getData("AcquiringInstCountryCode"));
        setForwardingInstIdCode(xml.getData("ForwardingInstIdCode"));
        setForwardingInstCountryCode(xml.getData("ForwardingInstCountryCode"));
        setReceivingInstCountryCode(xml.getData("ReceivingInstCountryCode"));
        setReceivingInstIdCode(xml.getData("ReceivingInstIdCode"));
        setAuthIdRsp(xml.getData("AuthIdRsp"));
        setRole(xml.getData("Role"));
        setMsgId(xml.getData("MsgId"));
        setMsgMatching(xml.getData("MsgMatching"));
        setRejectFields(xml.getData("RejectFields"));
        setRejectCodes(xml.getData("RejectCodes"));
        setCorrection(xml.getData("Correction"));
        setPreviousPostTranID(xml.getData("PreviousPostTranID"));
    }

    public String getAuthIdRsp()
    {
        return _AuthIdRsp;
    }

    public void setAuthIdRsp(String newValue)
    {
        _AuthIdRsp = newValue;
    }

    public void toXml(OutputStream stream, String indent, boolean embed_files)
    {
        if(stream instanceof XmlOutputStream)
        {
            toXml(stream, embed_files);
        } else
        {
            FormattedOutputStream out = new FormattedOutputStream(stream);
            out.setIndentString(indent);
            toXml(((OutputStream) (out)), embed_files);
        }
    }

    public void toXml(OutputStream stream, boolean embed_files)
    {
        XmlOutputStream out = null;
        if(stream instanceof XmlOutputStream)
        {
            out = (XmlOutputStream)stream;
        } else
        {
            out = new RawOutputStream(stream);
        }
        out.writeStartTag("AdjustmentComponent", false);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).incrementIndent();
        }
        Object OriginalAmounts_value = getOriginalAmounts();
        if(OriginalAmounts_value instanceof XmlObject)
        {
            out.write(null, (XmlObject)OriginalAmounts_value, embed_files);
        }
        out.write("ProcessingCode", _ProcessingCode);
        out.write("RetrievalDocumentCode", _RetrievalDocumentCode);
        out.write("AcquirerRefNr", _AcquirerRefNr);
        out.write("IssuerRefNr", _IssuerRefNr);
        out.write("FeeCollectionControlNr", _FeeCollectionControlNr);
        out.write("DocumentationIndicator", _DocumentationIndicator);
        out.write("MessageText", _MessageText);
        out.write("FulfillmentDocumentCode", _FulfillmentDocumentCode);
        out.write("MasterCardFunctionCode", _MasterCardFunctionCode);
        out.write("DestinationInstitutionId", _DestinationInstitutionId);
        out.write("OriginatorInstitutionId", _OriginatorInstitutionId);
        out.write("AdditionalData", _AdditionalData);
        out.write("DateAction", _DateAction);
        out.write("OriginalRetrievalReason", _OriginalRetrievalReason);
        out.write("AcquiringInstIdCode", _AcquiringInstIdCode);
        out.write("AcquiringInstCountryCode", _AcquiringInstCountryCode);
        out.write("ForwardingInstIdCode", _ForwardingInstIdCode);
        out.write("ForwardingInstCountryCode", _ForwardingInstCountryCode);
        out.write("ReceivingInstCountryCode", _ReceivingInstCountryCode);
        out.write("ReceivingInstIdCode", _ReceivingInstIdCode);
        out.write("AuthIdRsp", _AuthIdRsp);
        out.write("Role", _Role);
        out.write("MsgId", _MsgId);
        out.write("MsgMatching", _MsgMatching);
        out.write("RejectFields", _RejectFields);
        out.write("RejectCodes", _RejectCodes);
        out.write("Correction", _Correction);
        out.write("PreviousPostTranID", _PreviousPostTranID);
        if(out instanceof FormattedOutputStream)
        {
            ((FormattedOutputStream)out).decrementIndent();
        }
        out.writeEndTag("AdjustmentComponent");
    }

    public String getRejectFields()
    {
        return _RejectFields;
    }

    public void setRejectFields(String newValue)
    {
        _RejectFields = newValue;
    }

    public String getProcessingCode()
    {
        return _ProcessingCode;
    }

    public void setProcessingCode(String newValue)
    {
        _ProcessingCode = newValue;
    }

    public String getFulfillmentDocumentCode()
    {
        return _FulfillmentDocumentCode;
    }

    public void setFulfillmentDocumentCode(String newValue)
    {
        _FulfillmentDocumentCode = newValue;
    }

    public String getAcquiringInstIdCode()
    {
        return _AcquiringInstIdCode;
    }

    public void setAcquiringInstIdCode(String newValue)
    {
        _AcquiringInstIdCode = newValue;
    }

    public String getReceivingInstIdCode()
    {
        return _ReceivingInstIdCode;
    }

    public void setReceivingInstIdCode(String newValue)
    {
        _ReceivingInstIdCode = newValue;
    }

    public String getRole()
    {
        return _Role;
    }

    public void setRole(String newValue)
    {
        _Role = newValue;
    }

    public static String getClassXmlTagName()
    {
        return "AdjustmentComponent";
    }

    public String getFeeCollectionControlNr()
    {
        return _FeeCollectionControlNr;
    }

    public void setFeeCollectionControlNr(String newValue)
    {
        _FeeCollectionControlNr = newValue;
    }

    public String getAcquirerRefNr()
    {
        return _AcquirerRefNr;
    }

    public void setAcquirerRefNr(String newValue)
    {
        _AcquirerRefNr = newValue;
    }

    public String getIssuerRefNr()
    {
        return _IssuerRefNr;
    }

    public void setIssuerRefNr(String newValue)
    {
        _IssuerRefNr = newValue;
    }

    public String getDateAction()
    {
        return _DateAction;
    }

    public void setDateAction(String newValue)
    {
        _DateAction = newValue;
    }

    public String getRejectCodes()
    {
        return _RejectCodes;
    }

    public void setRejectCodes(String newValue)
    {
        _RejectCodes = newValue;
    }

    public AdjustmentComponent()
    {
        _OriginalAmounts = null;
        _ProcessingCode = null;
        _RetrievalDocumentCode = null;
        _AcquirerRefNr = null;
        _IssuerRefNr = null;
        _FeeCollectionControlNr = null;
        _DocumentationIndicator = null;
        _MessageText = null;
        _FulfillmentDocumentCode = null;
        _MasterCardFunctionCode = null;
        _DestinationInstitutionId = null;
        _OriginatorInstitutionId = null;
        _AdditionalData = null;
        _DateAction = null;
        _OriginalRetrievalReason = null;
        _AcquiringInstIdCode = null;
        _AcquiringInstCountryCode = null;
        _ForwardingInstIdCode = null;
        _ForwardingInstCountryCode = null;
        _ReceivingInstCountryCode = null;
        _ReceivingInstIdCode = null;
        _AuthIdRsp = null;
        _Role = null;
        _MsgId = null;
        _MsgMatching = null;
        _RejectFields = null;
        _RejectCodes = null;
        _Correction = null;
        _PreviousPostTranID = null;
    }

    public AdjustmentComponent(XmlElement root)
    {
        _OriginalAmounts = null;
        _ProcessingCode = null;
        _RetrievalDocumentCode = null;
        _AcquirerRefNr = null;
        _IssuerRefNr = null;
        _FeeCollectionControlNr = null;
        _DocumentationIndicator = null;
        _MessageText = null;
        _FulfillmentDocumentCode = null;
        _MasterCardFunctionCode = null;
        _DestinationInstitutionId = null;
        _OriginatorInstitutionId = null;
        _AdditionalData = null;
        _DateAction = null;
        _OriginalRetrievalReason = null;
        _AcquiringInstIdCode = null;
        _AcquiringInstCountryCode = null;
        _ForwardingInstIdCode = null;
        _ForwardingInstCountryCode = null;
        _ReceivingInstCountryCode = null;
        _ReceivingInstIdCode = null;
        _AuthIdRsp = null;
        _Role = null;
        _MsgId = null;
        _MsgMatching = null;
        _RejectFields = null;
        _RejectCodes = null;
        _Correction = null;
        _PreviousPostTranID = null;
        fromXml(root);
    }

    public String getForwardingInstIdCode()
    {
        return _ForwardingInstIdCode;
    }

    public void setForwardingInstIdCode(String newValue)
    {
        _ForwardingInstIdCode = newValue;
    }

    public String getMsgMatching()
    {
        return _MsgMatching;
    }

    public String getAcquiringInstCountryCode()
    {
        return _AcquiringInstCountryCode;
    }

    public void setAcquiringInstCountryCode(String newValue)
    {
        _AcquiringInstCountryCode = newValue;
    }

    public String getReceivingInstCountryCode()
    {
        return _ReceivingInstCountryCode;
    }

    public void setReceivingInstCountryCode(String newValue)
    {
        _ReceivingInstCountryCode = newValue;
    }

    public void setMsgMatching(String newValue)
    {
        _MsgMatching = newValue;
    }

    public String getMessageText()
    {
        return _MessageText;
    }

    public void setMessageText(String newValue)
    {
        _MessageText = newValue;
    }
}

