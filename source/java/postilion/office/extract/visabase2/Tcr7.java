package postilion.office.extract.visabase2;


import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;
import postilion.core.util.Convert;

public class Tcr7 extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String TRANSACTION_TYPE = new String("transaction type");
        public static final String CARD_SEQUENCE_NUMBER = new String("card sequence number");
        public static final String TERMINAL_TRANSACTION_DATE = new String("terminal transaction date");
        public static final String TERMINAL_CAPABILITY_PROFILE = new String("terminal capability profile");
        public static final String TERMINAL_COUNTRY_CODE = new String("terminal country code");
        public static final String TERMINAL_SERIAL_NUMBER = new String("terminal serial number");
        public static final String UNPREDICTABLE_NUMBER = new String("unpredictable number");
        public static final String APPLICATION_TRANSACTION_COUNTER = new String("application transaction counter");
        public static final String APPLICATION_INTERCHANGE_PROFILE = new String("application interchange profile");
        public static final String CRYPTOGRAM = new String("cryptogram");
        public static final String DERIVATION_KEY_INDEX = new String("derivation key index");
        public static final String CRYPTOGRAM_VERSION = new String("cryptogram version");
        public static final String TERMINAL_VERIFICATION_RESULTS = new String("terminal verification results");
        public static final String CARD_VERIFICATION_RESULTS = new String("card verification results");
        public static final String CRYPTOGRAM_AMOUNT = new String("cryptogram amount");
        public static final String IAD_BYTE_8 = new String("issuer application data byte 8");
        public static final String IAD_BYTE_9_TO_16 = new String("issuer application data byte 9 to 16");
        public static final String IAD_BYTE_1 = new String("issuer application data byte 1");
        public static final String IAD_BYTE_17 = new String("issuer application data byte 17");
        public static final String IAD_BYTE_18_TO_32 = new String("issuer application data byte 18 to 32");
        public static final String FORM_FACTOR_INDICATOR = new String("form factor indicator");
        public static final String ISSUER_SCRIPT_1_RESULTS = new String("issuer script 1 results");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr7()
    {
        super(168, stream);
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "7");
    }

    public static void main(String args[])
    {
        Tcr7 tcr7 = new Tcr7();
        tcr7.putField(Field.TRANSACTION_CODE, Convert.resize("", 2, '0', true));
        tcr7.putField(Field.CODE_QUALIFIER, "0");
        tcr7.putField(Field.COMPONENT_SEQUENCE, "7");
        tcr7.putField(Field.TRANSACTION_TYPE, "00");
        tcr7.putField(Field.CARD_SEQUENCE_NUMBER, "000");
        tcr7.putField(Field.TERMINAL_TRANSACTION_DATE, Convert.resize("", 6, '0', true));
        tcr7.putField(Field.TERMINAL_CAPABILITY_PROFILE, Convert.resize("", 6, '0', true));
        tcr7.putField(Field.TERMINAL_COUNTRY_CODE, "000");
        tcr7.putField(Field.TERMINAL_SERIAL_NUMBER, Convert.resize("", 8, '0', true));
        tcr7.putField(Field.UNPREDICTABLE_NUMBER, Convert.resize("", 8, '0', true));
        tcr7.putField(Field.APPLICATION_TRANSACTION_COUNTER, "0000");
        tcr7.putField(Field.APPLICATION_INTERCHANGE_PROFILE, "0000");
        tcr7.putField(Field.CRYPTOGRAM, Convert.resize("", 16, '0', true));
        tcr7.putField(Field.TERMINAL_VERIFICATION_RESULTS, Convert.resize("", 10, '0', true));
        tcr7.putField(Field.CRYPTOGRAM_AMOUNT, Convert.resize("", 12, '0', true));
        System.out.println(tcr7);
        try
        {
            System.out.println("'" + Convert.getString(tcr7.toMsg()) + "'");
        }
        catch (Exception e)
        {
            System.out.println("Exception" + e);
            e.printStackTrace();
        }
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN
                (), 1);
        IStreamFormatter transaction_type = new StreamFormatterFieldFixed(Field.TRANSACTION_TYPE, Validator.getAn(), 2);
        IStreamFormatter card_sequence_number = new StreamFormatterFieldFixed(Field.CARD_SEQUENCE_NUMBER, Validator
                .getN(), 3);
        IStreamFormatter terminal_transaction_date = new StreamFormatterFieldFixed(Field.TERMINAL_TRANSACTION_DATE,
                Validator.getN(), 6);
        IStreamFormatter terminal_capability_profile = new StreamFormatterFieldFixed(Field
                .TERMINAL_CAPABILITY_PROFILE, Validator.getAns(), 6);
        IStreamFormatter terminal_country_code = new StreamFormatterFieldFixed(Field.TERMINAL_COUNTRY_CODE, Validator
                .getN(), 3);
        IStreamFormatter terminal_serial_number = new StreamFormatterFieldFixed(Field.TERMINAL_SERIAL_NUMBER,
                Validator.getAns(), 8, ' ', true, true);
        IStreamFormatter unpredictable_number = new StreamFormatterFieldFixed(Field.UNPREDICTABLE_NUMBER, Validator
                .getAns(), 8);
        IStreamFormatter application_transaction_counter = new StreamFormatterFieldFixed(Field
                .APPLICATION_TRANSACTION_COUNTER, Validator.getAns(), 4);
        IStreamFormatter application_interchange_profile = new StreamFormatterFieldFixed(Field
                .APPLICATION_INTERCHANGE_PROFILE, Validator.getAns(), 4);
        IStreamFormatter cryptogram = new StreamFormatterFieldFixed(Field.CRYPTOGRAM, Validator.getAns(), 16);
        IStreamFormatter derivation_key_index = new StreamFormatterFieldFixed(Field.DERIVATION_KEY_INDEX, Validator
                .getAns(), 2, ' ', true, true);
        IStreamFormatter cryptogram_version = new StreamFormatterFieldFixed(Field.CRYPTOGRAM_VERSION, Validator
                .getAns(), 2, ' ', true, true);
        IStreamFormatter terminal_verification_results = new StreamFormatterFieldFixed(Field
                .TERMINAL_VERIFICATION_RESULTS, Validator.getAns(), 10);
        IStreamFormatter card_verification_results = new StreamFormatterFieldFixed(Field.CARD_VERIFICATION_RESULTS,
                Validator.getAns(), 8, ' ', true, true);
        IStreamFormatter cryptogram_amount = new StreamFormatterFieldFixed(Field.CRYPTOGRAM_AMOUNT, Validator.getN(),
                12);
        IStreamFormatter iad_byte_8 = new StreamFormatterFieldFixed(Field.IAD_BYTE_8, Validator.getAns(), 2, ' ',
                true, true);
        IStreamFormatter iad_byte_9_to_16 = new StreamFormatterFieldFixed(Field.IAD_BYTE_9_TO_16, Validator.getAns(),
                16, ' ', true, true);
        IStreamFormatter iad_byte_1 = new StreamFormatterFieldFixed(Field.IAD_BYTE_1, Validator.getAns(), 2, ' ',
                true, true);
        IStreamFormatter iad_byte_17 = new StreamFormatterFieldFixed(Field.IAD_BYTE_17, Validator.getAns(), 2, ' ',
                true, true);
        IStreamFormatter iad_byte_18_to_32 = new StreamFormatterFieldFixed(Field.IAD_BYTE_18_TO_32, Validator.getAns
                (), 30, ' ', true, true);
        IStreamFormatter form_factor_indicator = new StreamFormatterFieldFixed(Field.FORM_FACTOR_INDICATOR, Validator
                .getAns(), 8, ' ', true, true);
        IStreamFormatter issuer_script_1_results = new StreamFormatterFieldFixed(Field.ISSUER_SCRIPT_1_RESULTS,
                Validator.getAns(), 10, ' ', true, true);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(transaction_type);
        msg.add(card_sequence_number);
        msg.add(terminal_transaction_date);
        msg.add(terminal_capability_profile);
        msg.add(terminal_country_code);
        msg.add(terminal_serial_number);
        msg.add(unpredictable_number);
        msg.add(application_transaction_counter);
        msg.add(application_interchange_profile);
        msg.add(cryptogram);
        msg.add(derivation_key_index);
        msg.add(cryptogram_version);
        msg.add(terminal_verification_results);
        msg.add(card_verification_results);
        msg.add(cryptogram_amount);
        msg.add(iad_byte_8);
        msg.add(iad_byte_9_to_16);
        msg.add(iad_byte_1);
        msg.add(iad_byte_17);
        msg.add(iad_byte_18_to_32);
        msg.add(form_factor_indicator);
        msg.add(issuer_script_1_results);
        stream = msg;
    }
}
