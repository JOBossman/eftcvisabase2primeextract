package postilion.office.extract.visabase2;


import postilion.core.message.Validator;
import postilion.core.message.stream.*;

public class Tcr4 extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String RESERVED = new String("reserved");
        public static final String DEBIT_PRODUCT_CODE = new String("debit product code");
        public static final String TCR4_REMAINDER = new String("tcr4 remainder");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr4()
    {
        super(168, stream);
    }

    public static void main(String args[])
    {
        Tcr4 tcr4 = new Tcr4();
        System.out.println(tcr4.describeStructure());
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN(), 1);
        IStreamFormatter reserved = new StreamFormatterFieldFixed(Field.RESERVED, Validator.getAns(), 12);
        IStreamFormatter debit_product_code = new StreamFormatterFieldFixed(Field.DEBIT_PRODUCT_CODE, Validator.getN(), 4);
        IStreamFormatter tcr4_remainder = new StreamFormatterFieldVar(Field.TCR4_REMAINDER, Validator.getAns(), 148, false);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(reserved);
        msg.add(debit_product_code);
        msg.add(tcr4_remainder);
        stream = msg;
    }
}
