package postilion.office.extract.visabase2;


import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;

public class Tcr0 extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String ACCOUNT_NUMBER = new String("account number");
        public static final String FLOOR_LIMIT_IND = new String("floor limit ind");
        public static final String CRB_EXCEPTION = new String("crb exception");
        public static final String PCAS_IND = new String("pcas ind");
        public static final String FORMAT_CODE = new String("format code");
        public static final String BIN = new String("bin");
        public static final String DATE = new String("date");
        public static final String FILM_LOCATOR = new String("film locator");
        public static final String CHECK_DIGIT = new String("check digit");
        public static final String ACQUIRER_BUSS_ID = new String("acquirer buss id");
        public static final String PURCHASE_DATE = new String("purchase date");
        public static final String DESTINATION_AMOUNT = new String("destination amount");
        public static final String DESTINATION_CURRENCY_CODE = new String("destination currency code");
        public static final String SOURCE_AMOUNT = new String("source amount");
        public static final String SOURCE_CURRENCY_CODE = new String("source currency code");
        public static final String MERCHANT_NAME = new String("merchant name");
        public static final String MERCHANT_CITY = new String("merchant city");
        public static final String MERCHANT_COUNTRY_CODE = new String("merchant country code");
        public static final String MERCHANT_CATEGORY_CODE = new String("merchant category code");
        public static final String MERCHANT_ZIP_CODE = new String("merchant zip code");
        public static final String MERCHANT_STATE_CODE = new String("merchant state code");
        public static final String REQ_PAYMENT_SERVICE = new String("req payment service");
        public static final String RESERVED = new String("reserved");
        public static final String USAGE_CODE = new String("usage code");
        public static final String REASON_CODE = new String("reason code");
        public static final String SETTLEMENT_FLAG = new String("settlement flag");
        public static final String AUTH_CHARACTERISTICS = new String("auth characteristics");
        public static final String AUTH_CODE = new String("auth code");
        public static final String POS_TERM_CAPAB = new String("pos term capab");
        public static final String INTL_FEE_IND = new String("intl fee ind");
        public static final String CARDHOLDER_ID_METHOD = new String("cardholder id method");
        public static final String COLLECTION_ONLY_FLAG = new String("collection only flag");
        public static final String POS_ENTRY_MODE = new String("pos entry mode");
        public static final String CENTRAL_PROC_DATE = new String("central proc date");
        public static final String REIMBURSEMENT_ATTR = new String("reimbursement attr");


        public Field()
        {
        }
    }

    public static class Settlement
    {

        public static final String _0_INTERNATIONAL = new String("0");
        public static final String _3_CLEARING_ONLY = new String("3");
        public static final String _8_NATIONAL = new String("8");
        public static final String _9_BASE2_SELECTS = new String("9");


        public Settlement()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr0()
    {
        super(168, stream);
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "0");
        putField(Field.FLOOR_LIMIT_IND, " ");
        putField(Field.PCAS_IND, " ");
        putField(Field.DESTINATION_AMOUNT, "000000000000");
        putField(Field.DESTINATION_CURRENCY_CODE, "   ");
        putField(Field.RESERVED, " ");
        putField(Field.REASON_CODE, "00");
        putField(Field.INTL_FEE_IND, " ");
        putField(Field.COLLECTION_ONLY_FLAG, " ");
    }

    public static void main(String args[])
    {
        Tcr0 tcr0 = new Tcr0();
        System.out.println(tcr0.describeStructure());
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN(), 1);
        IStreamFormatter account_number = new StreamFormatterFieldFixed(Field.ACCOUNT_NUMBER, Validator.getN(), 19);
        IStreamFormatter floor_limit_ind = new StreamFormatterFieldFixed(Field.FLOOR_LIMIT_IND, Validator.getAns(), 1);
        IStreamFormatter crb_exception = new StreamFormatterFieldFixed(Field.CRB_EXCEPTION, Validator.getAns(), 1, ' ', false, true);
        IStreamFormatter pcas_ind = new StreamFormatterFieldFixed(Field.PCAS_IND, Validator.getAns(), 1);
        IStreamFormatter format_code = new StreamFormatterFieldFixed(Field.FORMAT_CODE, Validator.getN(), 1);
        IStreamFormatter bin = new StreamFormatterFieldFixed(Field.BIN, Validator.getN(), 6);
        IStreamFormatter date = new StreamFormatterFieldFixed(Field.DATE, Validator.getN(), 4);
        IStreamFormatter film_locator = new StreamFormatterFieldFixed(Field.FILM_LOCATOR, Validator.getN(), 11);
        IStreamFormatter check_digit = new StreamFormatterFieldFixed(Field.CHECK_DIGIT, Validator.getN(), 1);
        IStreamFormatter acquirer_buss_id = new StreamFormatterFieldFixed(Field.ACQUIRER_BUSS_ID, Validator.getN(), 8, '0', false, true);
        IStreamFormatter purchase_date = new StreamFormatterFieldFixed(Field.PURCHASE_DATE, Validator.getN(), 4);
        IStreamFormatter destination_amount = new StreamFormatterFieldFixed(Field.DESTINATION_AMOUNT, Validator.getN(), 12, '0', false, true);
        IStreamFormatter destination_currency_code = new StreamFormatterFieldFixed(Field.DESTINATION_CURRENCY_CODE, Validator.getAns(), 3);
        IStreamFormatter source_amount = new StreamFormatterFieldFixed(Field.SOURCE_AMOUNT, Validator.getN(), 12, '0', false, true);
        IStreamFormatter source_currency_code = new StreamFormatterFieldFixed(Field.SOURCE_CURRENCY_CODE, Validator.getN(), 3);
        IStreamFormatter merchant_name = new StreamFormatterFieldFixed(Field.MERCHANT_NAME, Validator.getAns(), 25);
        IStreamFormatter merchant_city = new StreamFormatterFieldFixed(Field.MERCHANT_CITY, Validator.getAns(), 13);
        IStreamFormatter merchant_country_code = new StreamFormatterFieldFixed(Field.MERCHANT_COUNTRY_CODE, Validator.getAns(), 3);
        IStreamFormatter merchant_category_code = new StreamFormatterFieldFixed(Field.MERCHANT_CATEGORY_CODE, Validator.getN(), 4);
        IStreamFormatter merchant_zip_code = new StreamFormatterFieldFixed(Field.MERCHANT_ZIP_CODE, Validator.getN(), 5);
        IStreamFormatter merchant_state_code = new StreamFormatterFieldFixed(Field.MERCHANT_STATE_CODE, Validator.getAns(), 3);
        IStreamFormatter req_payment_service = new StreamFormatterFieldFixed(Field.REQ_PAYMENT_SERVICE, Validator.getAns(), 1);
        IStreamFormatter reserved = new StreamFormatterFieldFixed(Field.RESERVED, Validator.getAns(), 1);
        IStreamFormatter usage_code = new StreamFormatterFieldFixed(Field.USAGE_CODE, Validator.getN(), 1);
        IStreamFormatter reason_code = new StreamFormatterFieldFixed(Field.REASON_CODE, Validator.getN(), 2);
        IStreamFormatter settlement_flag = new StreamFormatterFieldFixed(Field.SETTLEMENT_FLAG, Validator.getAns(), 1);
        IStreamFormatter auth_characteristics = new StreamFormatterFieldFixed(Field.AUTH_CHARACTERISTICS, Validator.getAns(), 1);
        IStreamFormatter auth_code = new StreamFormatterFieldFixed(Field.AUTH_CODE, Validator.getAns(), 6);
        IStreamFormatter pos_term_capab = new StreamFormatterFieldFixed(Field.POS_TERM_CAPAB, Validator.getAns(), 1);
        IStreamFormatter intl_fee_ind = new StreamFormatterFieldFixed(Field.INTL_FEE_IND, Validator.getAns(), 1);
        IStreamFormatter cardholder_id_method = new StreamFormatterFieldFixed(Field.CARDHOLDER_ID_METHOD, Validator.getAns(), 1);
        IStreamFormatter collection_only_flag = new StreamFormatterFieldFixed(Field.COLLECTION_ONLY_FLAG, Validator.getAns(), 1);
        IStreamFormatter pos_entry_mode = new StreamFormatterFieldFixed(Field.POS_ENTRY_MODE, Validator.getAns(), 2);
        IStreamFormatter central_proc_date = new StreamFormatterFieldFixed(Field.CENTRAL_PROC_DATE, Validator.getN(), 4);
        IStreamFormatter reimbursement_attr = new StreamFormatterFieldFixed(Field.REIMBURSEMENT_ATTR, Validator.getAn(), 1);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(account_number);
        msg.add(floor_limit_ind);
        msg.add(crb_exception);
        msg.add(pcas_ind);
        msg.add(format_code);
        msg.add(bin);
        msg.add(date);
        msg.add(film_locator);
        msg.add(check_digit);
        msg.add(acquirer_buss_id);
        msg.add(purchase_date);
        msg.add(destination_amount);
        msg.add(destination_currency_code);
        msg.add(source_amount);
        msg.add(source_currency_code);
        msg.add(merchant_name);
        msg.add(merchant_city);
        msg.add(merchant_country_code);
        msg.add(merchant_category_code);
        msg.add(merchant_zip_code);
        msg.add(merchant_state_code);
        msg.add(req_payment_service);
        msg.add(reserved);
        msg.add(usage_code);
        msg.add(reason_code);
        msg.add(settlement_flag);
        msg.add(auth_characteristics);
        msg.add(auth_code);
        msg.add(pos_term_capab);
        msg.add(intl_fee_ind);
        msg.add(cardholder_id_method);
        msg.add(collection_only_flag);
        msg.add(pos_entry_mode);
        msg.add(central_proc_date);
        msg.add(reimbursement_attr);
        stream = msg;
    }
}
