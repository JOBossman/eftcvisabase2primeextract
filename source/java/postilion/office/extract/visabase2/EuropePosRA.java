package postilion.office.extract.visabase2;

// Referenced classes of package postilion.office.extract.visabase2:
//            Tcr0, Tcr1, IReimbursementAttributeTcrs, Tcr5,
//            Tcr7

public class EuropePosRA
        implements IReimbursementAttributeTcrs
{

    public EuropePosRA()
    {
    }

    public String getReimbursementAttribute(Tcr0 tcr0, Tcr1 tcr1, Tcr5 tcr5, Tcr7 tcr7)
    {
        if(tcr0.getField(Tcr0.Field.POS_ENTRY_MODE).equals("05") || tcr0.getField(Tcr0.Field.POS_ENTRY_MODE).equals("07") || tcr0.getField(Tcr0.Field.POS_ENTRY_MODE).equals("90") || tcr0.getField(Tcr0.Field.POS_ENTRY_MODE).equals("91"))
        {
            return "B";
        }
        if(tcr0.getField(Tcr0.Field.POS_ENTRY_MODE).equals("02") || tcr0.getField(Tcr0.Field.POS_ENTRY_MODE).equals("95"))
        {
            return "7";
        }
        if(tcr0.getField(Tcr0.Field.POS_ENTRY_MODE).equals("01"))
        {
            if(tcr1.getField(Tcr1.Field.MAIL_TEL_IND).equals("5") || tcr1.getField(Tcr1.Field.MAIL_TEL_IND).equals("6"))
            {
                return "5";
            } else
            {
                return "7";
            }
        } else
        {
            return "0";
        }
    }
}

