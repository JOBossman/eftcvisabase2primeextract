package postilion.office.extract.visabase2;



public class GenericAmount
{

    private long amount;
    private String currency;

    public String getCurrency()
    {
        return currency;
    }

    public void setAmount(long amount)
    {
        this.amount = amount;
    }

    public long getAmount()
    {
        return amount;
    }

    public GenericAmount()
    {
        amount = 0L;
        currency = "000";
    }

    public GenericAmount(long amount, String currency)
    {
        this.amount = 0L;
        this.currency = "000";
        this.amount = amount;
        this.currency = currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }
}

