package postilion.office.extract.visabase2;


import postilion.office.extract.Extract;
import postilion.office.extract.visabase2.PrimeBase2Extract;

import java.io.PrintWriter;
import java.io.StringWriter;

// Referenced classes of package postilion.office.extract.visabase2:
//            VisaBase2Extract

public class VisaBase2ExtractLog
{

    private static String replaceSlashNs(String str)
    {
        StringBuffer out = new StringBuffer(str);
        for (int i = 0; i < out.length(); i++)
        {
            if (out.charAt(i) == '\n' && (i == 0 || out.charAt(i - 1) != '\r'))
            {
                out.insert(i, '\r');
                i++;
            }
        }

        return out.toString();
    }

    private static String getStackTracesString(Throwable e)
    {
        StringWriter output = new StringWriter();
        e.printStackTrace(new PrintWriter(output, true));
        return output.toString();
    }

    public VisaBase2ExtractLog()
    {
    }

    public static void reportError(String error)
    {
        PrimeBase2Extract.debugPrintln(error);
        Extract.reportPluginError(replaceSlashNs(error));
    }

    public static void reportError(String error, String params[])
    {
        reportError(insertParams(error, params));
    }

    public static void reportError(String error, String params[], Throwable e)
    {
        reportError(insertParams(error, params) + ". The following exception was thrown: " + e.getMessage() + "\n\n"
                + getStackTracesString(e));
    }

    public static void reportError(String error, Throwable e)
    {
        reportError(error + ". The following exception was thrown: " + e.getMessage() + "\n\n" + getStackTracesString
                (e));
    }

    public static void reportWarning(String warning)
    {
        PrimeBase2Extract.debugPrintln(warning);
        Extract.reportPluginWarning(replaceSlashNs(warning));
    }

    public static void reportWarning(String warning, String param)
    {
        reportWarning(insertParams(warning, new String[]{param}));
    }

    public static void reportWarning(String warning, String params[])
    {
        reportWarning(insertParams(warning, params));
    }

    private static String insertParams(String descr, String params[])
    {
        StringBuffer ret = new StringBuffer();
        int prevOffset = 0;
        int offset = 0;
        for (int i = 1; i <= params.length && (offset = descr.indexOf("%" + i, prevOffset)) != -1; i++)
        {
            ret.append(descr.substring(prevOffset, offset));
            ret.append(params[i - 1]);
            prevOffset = offset + 2;
        }

        ret.append(descr.substring(prevOffset));
        return ret.toString();
    }
}
