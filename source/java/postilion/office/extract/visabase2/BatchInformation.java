package postilion.office.extract.visabase2;


public class BatchInformation
{

    public long batch_monetary_tc_counter;
    public long batch_tc_counter;
    public long batch_tcr_counter;
    public long batch_source_amount;
    public long file_monetary_tc_counter;
    public long file_tc_counter;
    public long file_tcr_counter;
    public long file_source_amount;
    public long batch_no_counter;
    public long exception_counter;
    public int last_transaction_batch_nr;

    public BatchInformation()
    {
        batch_monetary_tc_counter = 0L;
        batch_tc_counter = 0L;
        batch_tcr_counter = 0L;
        batch_source_amount = 0L;
        file_monetary_tc_counter = 0L;
        file_tc_counter = 0L;
        file_tcr_counter = 0L;
        file_source_amount = 0L;
        batch_no_counter = 1L;
        exception_counter = 0L;
        last_transaction_batch_nr = 0;
    }
}

