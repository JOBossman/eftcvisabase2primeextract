package postilion.office.extract.visabase2;

import postilion.office.OfficeLib;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;


public class PolandRA extends EuropePosRA
{
    private static final class AuthRspCode
    {

        public static final String _Y1_OFFLINE_APPROVAL = "Y1";
        public static final String _Z1_OFFLINE_DECLINE = "Z1";
        public static final String _Y3_UNABLE_TO_GO_ONLINE_APPROVAL = "Y3";
        public static final String _Z3_UNABLE_TO_GO_ONLINE_DECLINE = "Z3";

        AuthRspCode()
        {
        }
    }


    private static String _4111_MERCHANT_TYPE = "4111";
    private static String _4814_MERCHANT_TYPE = "4814";
    private static String _4899_MERCHANT_TYPE = "4899";
    private static String _4900_MERCHANT_TYPE = "4900";
    private static String _6011_MERCHANT_TYPE = "6011";
    private static final String UNKNOWN_SQL_ERROR = "An unknown SQL exception occured while determining if the transaction is domesti" +
            "c. Exception detail: %1"
            ;
    private static final String ISSUER_COUNTRY_LOOKUP = "{CALL visabase2_get_issuer_country_code (?)}";

    private boolean isValidMcc(Tcr0 tcr0)
    {
        return tcr0.getField(Tcr0.Field.MERCHANT_CATEGORY_CODE).equals("5300");
    }

    public PolandRA()
    {
    }

    boolean shouldApplyPolandBillPaymentIncentiveRule(Tcr0 tcr0, Tcr1 tcr1, Tcr5 tcr5)
    {
        String mcc = tcr0.getField(Tcr0.Field.MERCHANT_CATEGORY_CODE);
        boolean valid_mcc_4111 = _4111_MERCHANT_TYPE.equals(mcc);
        boolean valid_mcc_4814 = _4814_MERCHANT_TYPE.equals(mcc);
        boolean valid_mcc_4899 = _4899_MERCHANT_TYPE.equals(mcc);
        boolean valid_mcc_4900 = _4900_MERCHANT_TYPE.equals(mcc);
        if(!valid_mcc_4111 && !valid_mcc_4814 && !valid_mcc_4899 && !valid_mcc_4900)
        {
            return false;
        }
        boolean valid_pos_term_capability_5 = isValidPosTermCapability(tcr0);
        if(!valid_pos_term_capability_5)
        {
            return false;
        } else
        {
            String card_holder_id_method = tcr0.getField(Tcr0.Field.CARDHOLDER_ID_METHOD);
            boolean valid_cardholder_id_method_1_or_2 = "1".equals(card_holder_id_method) || "2".equalsIgnoreCase(card_holder_id_method);
            boolean valid_cardholder_id_method_space_or_2 = " ".equals(card_holder_id_method) || "2".equalsIgnoreCase(card_holder_id_method);
            String mode = tcr0.getField(Tcr0.Field.POS_ENTRY_MODE);
            boolean valid_pos_entry_mode_90 = "90".equals(mode);
            boolean valid_pos_entry_mode_05_or_07 = "05".equals(mode) || "07".equals(mode);
            boolean valid_auth_code = isValidAuthCode(tcr0);
            boolean auth_code_ends_with_0000Y = isAuthCodeEndsWith0000Y(tcr0);
            String cardholder_active_terminal = tcr1.getField(Tcr1.Field.CARDHOLDER_ACTIVATED_TERM);
            boolean valid_uat_space = " ".equals(cardholder_active_terminal);
            boolean valid_uat_2 = "2".equals(cardholder_active_terminal);
            boolean valid_scenario_1 = valid_cardholder_id_method_1_or_2 && valid_pos_entry_mode_90 && valid_auth_code && valid_uat_space;
            boolean valid_scenario_2 = valid_cardholder_id_method_1_or_2 && valid_pos_entry_mode_05_or_07 && valid_auth_code && valid_uat_space && isValidAuthRespCode_00_01_11(tcr5);
            boolean valid_scenario_3 = valid_cardholder_id_method_1_or_2 && valid_pos_entry_mode_05_or_07 && !valid_auth_code && !auth_code_ends_with_0000Y && valid_uat_space && isValidAuthRespCode_Y1_Y3(tcr5);
            boolean valid_scenario_4 = valid_cardholder_id_method_space_or_2 && valid_pos_entry_mode_90 && valid_auth_code && valid_uat_2;
            boolean valid_scenario_5 = valid_cardholder_id_method_space_or_2 && valid_pos_entry_mode_05_or_07 && valid_auth_code && valid_uat_2 && isValidAuthRespCode_00_01_11(tcr5);
            boolean valid_scenario_6 = valid_cardholder_id_method_space_or_2 && valid_pos_entry_mode_05_or_07 && !valid_auth_code && !auth_code_ends_with_0000Y && valid_uat_2 && isValidAuthRespCode_Y1_Y3(tcr5);
            return isDomestic(tcr0) && (valid_scenario_1 || valid_scenario_2 || valid_scenario_3 || valid_scenario_4 || valid_scenario_5 || valid_scenario_6);
        }
    }

    private boolean isAuthCodeEndsWith0000Y(Tcr0 tcr0)
    {
        String auth_code = tcr0.getField(Tcr0.Field.AUTH_CODE);
        String last_5_positions = auth_code.substring(1, 6);
        return last_5_positions.equals("0000Y");
    }

    private boolean isAtmTransaction(Tcr0 tcr0)
    {
        if(tcr0 == null)
        {
            return false;
        }
        String merchant_type = tcr0.getField(Tcr0.Field.MERCHANT_CATEGORY_CODE);
        return merchant_type != null && merchant_type.equals(_6011_MERCHANT_TYPE);
    }

    private boolean isValidPosTermCapability(Tcr0 tcr0)
    {
        return tcr0.getField(Tcr0.Field.POS_TERM_CAPAB).equals("5");
    }

    public String getReimbursementAttribute(Tcr0 tcr0, Tcr1 tcr1, Tcr5 tcr5, Tcr7 tcr7)
    {
        if(shouldApplyPolandWholesaleIncentiveRule(tcr0, tcr5))
        {
            return "D";
        }
        if(shouldApplyPolandBillPaymentIncentiveRule(tcr0, tcr1, tcr5))
        {
            return "D";
        }
        if(isAtmTransaction(tcr0))
        {
            String auth_char = tcr0.getField(Tcr0.Field.AUTH_CHARACTERISTICS);
            if(auth_char != null && (auth_char.equals("E") || auth_char.equals("M") || auth_char.equals("C")))
            {
                return "H";
            } else
            {
                return "2";
            }
        } else
        {
            return super.getReimbursementAttribute(tcr0, tcr1, tcr5, tcr7);
        }
    }

    private boolean isValidAuthCode(Tcr0 tcr0)
    {
        String auth_code = tcr0.getField(Tcr0.Field.AUTH_CODE);
        String last_5_positions = auth_code.substring(1, 6);
        return auth_code.charAt(5) != 'X' && !last_5_positions.startsWith("SVC") && !last_5_positions.equals("     ") && !last_5_positions.equals("00000") && !last_5_positions.equals("0000 ") && !last_5_positions.equals("0000N") && !last_5_positions.equals("0000P") && !last_5_positions.equals("0000Y");
    }

    boolean shouldApplyPolandWholesaleIncentiveRule(Tcr0 tcr0, Tcr5 tcr5)
    {
        boolean valid_mcc_5300 = isValidMcc(tcr0);
        if(!valid_mcc_5300)
        {
            return false;
        } else
        {
            boolean valid_cardholder_id_method_1_or_2 = isValidCardholderIdMethod(tcr0);
            String mode = tcr0.getField(Tcr0.Field.POS_ENTRY_MODE);
            boolean valid_pos_entry_mode_90 = mode.equals("90");
            boolean valid_pos_entry_mode_05_or_07 = mode.equals("05") || mode.equals("07");
            boolean valid_auth_code = isValidAuthCode(tcr0);
            boolean valid_pos_term_capability_5 = isValidPosTermCapability(tcr0);
            boolean valid_scenario_1 = valid_mcc_5300 && valid_cardholder_id_method_1_or_2 && valid_pos_entry_mode_90 && valid_auth_code && valid_pos_term_capability_5;
            boolean valid_scenario_2 = valid_mcc_5300 && valid_cardholder_id_method_1_or_2 && valid_pos_entry_mode_05_or_07 && valid_auth_code && valid_pos_term_capability_5 && isValidAuthRespCode_00_01_11(tcr5);
            boolean valid_scenario_3 = valid_mcc_5300 && valid_cardholder_id_method_1_or_2 && valid_pos_entry_mode_05_or_07 && !valid_auth_code && valid_pos_term_capability_5 && isValidAuthRespCode_Y1_Y3(tcr5);
            return valid_scenario_1 || valid_scenario_2 || valid_scenario_3;
        }
    }

    private boolean isValidCardholderIdMethod(Tcr0 tcr0)
    {
        String method = tcr0.getField(Tcr0.Field.CARDHOLDER_ID_METHOD);
        return method.equals("1") || method.equals("2");
    }

    private boolean isDomestic(Tcr0 tcr0)
    {
        String issuer_country_code = null;
        String merchant_country_code = tcr0.getField(Tcr0.Field.MERCHANT_COUNTRY_CODE).substring(0, 2);
        String pan = tcr0.getField(Tcr0.Field.ACCOUNT_NUMBER);
        String account_range = pan.substring(0, 9);
        try
        {
            Connection connection = OfficeLib.getOfficeConnection();
            CallableStatement stmt = null;
            ResultSet rs = null;
            try
            {
                stmt = connection.prepareCall("{CALL visabase2_get_issuer_country_code (?)}");
                stmt.setString(1, account_range);
                for(rs = stmt.executeQuery(); rs.next();)
                {
                    issuer_country_code = rs.getString(1);
                }

            }
            finally
            {
                OfficeLib.cleanupConnection(connection);
            }
            if(issuer_country_code != null && merchant_country_code.equalsIgnoreCase(issuer_country_code))
            {
                return true;
            }
        }
        catch(SQLException ex)
        {
            VisaBase2ExtractLog.reportError("An unknown SQL exception occured while determining if the transaction is domesti" +
                            "c. Exception detail: %1"
                    , new String[] {
                            ex.toString()
                    });
        }
        return false;
    }

    private boolean isValidAuthRespCode_Y1_Y3(Tcr5 tcr5)
    {
        String code = tcr5.getField(Tcr5.Field.AUTH_RESP_CODE);
        return code.equals("Y1") || code.equals("Y3");
    }

    private boolean isValidAuthRespCode_00_01_11(Tcr5 tcr5)
    {
        String code = tcr5.getField(Tcr5.Field.AUTH_RESP_CODE);
        return code.equals("00") || code.equals("01") || code.equals("11");
    }

}

