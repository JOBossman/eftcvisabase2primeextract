package postilion.office.extract.visabase2;


import postilion.core.message.Validator;
import postilion.core.message.stream.*;
import postilion.core.util.Convert;

public final class AdditionalPosInfo extends StreamMessage
{
    public static final class Field
    {

        public static final String TERM_TYPE = "Terminal Type             ";
        public static final String TERM_ENTRY_CAP = "Terminal Entry Capability ";
        public static final String CHIP_CONDITION_CODE = "Chip Condition Code       ";
        public static final String NEW_SERVICE_DEV = "New Service Development   ";
        public static final String MERCHANT_GROUP = "Merchant Group Id         ";
        public static final String CCPS_TRANSACTION_INDICATOR = "CCPS Transaction Indicator";
        public static final String CARD_AUTH_REL_INDICATOR = "Card Auth Rel Indicator   ";
        public static final String ELECTRONIC_COMMERCE = "Electronic Commerce       ";

        public Field()
        {
        }
    }

    public static final class TerminalType
    {

        public static final String UNSPECIFIED = new String("0");
        public static final String LIMITED_AMT_TERMINAL = new String("1");
        public static final String UNATTENDED_ATM = new String("2");
        public static final String UNATTENDED_SST = new String("3");
        public static final String UNATTENDED_ECR = new String("4");
        public static final String CARDHOLDER_PREMISES_UNATTENDED = new String("5");
        public static final String TELEPHONE = new String("7");


        public TerminalType()
        {
        }
    }

    public static final class TerminalEntryCap
    {

        public static final String DOES_NOT_READ_CARD = new String("9");


        public TerminalEntryCap()
        {
        }
    }

    public static final class TransactionIndicator
    {

        public static final String NON_VSDC = "0";
        public static final String VSDC = "1";

        public TransactionIndicator()
        {
        }
    }

    public static final class CardAuthRelIndicator
    {

        public static final String CARD_AUTH_NOT_RELIABLE = "1";
        public static final String ACQUIRER_INACTIVE = "2";
        public static final String ISSUER_INACTIVE = "3";

        public CardAuthRelIndicator()
        {
        }
    }

    public static final class ECommerce
    {

        public static final String SET_WITH_CARDHOLDER_CERT = "05";
        public static final String NON_SET_WITH_MERCHANT_CERT = "06";
        public static final String CHANNEL_ENCRYPTED = "07";
        public static final String NON_SECURE = "08";
        public static final String NON_SET_FROM_SET_MERCHANT = "09";

        public ECommerce()
        {
        }
    }

    public static final class MOTO
    {

        public static final String SET_WITH_CARDHOLDER_CERT = "5";
        public static final String NON_SET_WITH_MERCHANT_CERT = "6";
        public static final String CHANNEL_ENCRYPTED = "7";
        public static final String NON_SECURE = "8";
        public static final String NON_SET_FROM_SET_MERCHANT = "9";

        public MOTO()
        {
        }
    }


    private static IStreamFormatter stream;
    private static final int MAX_SIZE = 10;

    public byte[] toMsg()
            throws XStreamBase
    {
        byte byte_array[] = super.toMsg();
        int last;
        for(last = 9; last >= 2 && byte_array[last] == 48; last--) { }
        byte new_array[] = new byte[last + 1];
        System.arraycopy(byte_array, 0, new_array, 0, last + 1);
        return new_array;
    }

    public AdditionalPosInfo()
    {
        super(10, stream);
    }

    public static void main(String args[])
    {
        AdditionalPosInfo add_pos_info = new AdditionalPosInfo();
        try
        {
            String raw_str = "12345678";
            byte raw_byte[] = Convert.getData(raw_str);
            add_pos_info.fromMsg(raw_byte);
            String str = add_pos_info.getField("Terminal Type             ");
            System.out.println(str);
            str = add_pos_info.getField("Terminal Entry Capability ");
            System.out.println(str);
            str = add_pos_info.getField("Chip Condition Code       ");
            System.out.println(str);
            str = add_pos_info.getField("New Service Development   ");
            System.out.println(str);
            str = add_pos_info.getField("Merchant Group Id         ");
            System.out.println(str);
            System.out.println(add_pos_info.getField("Electronic Commerce       "));
            add_pos_info.putField("Electronic Commerce       ", "0");
            raw_byte = add_pos_info.toMsg();
            System.out.println(Convert.getString(raw_byte));
        }
        catch(XStreamLogicFailure e)
        {
            System.out.println("XStreamLogicFailure ");
            System.out.println(e);
        }
        catch(XStreamUnableToExtract e)
        {
            System.out.println("XStreamUnableToExtract");
            System.out.println(e);
        }
        catch(Exception e)
        {
            System.out.println("Exception");
            System.out.println(e);
        }
    }

    public int fromMsg(byte msg[], int offset)
            throws XStreamBase
    {
        if(msg.length - offset < 10)
        {
            byte new_array[] = {
                    48, 48, 48, 48, 48, 48, 48, 48, 48, 48
            };
            System.arraycopy(msg, offset, new_array, 0, msg.length - offset);
            msg = new_array;
            return super.fromMsg(msg, 0);
        } else
        {
            return super.fromMsg(msg, offset);
        }
    }

    static
    {
        StreamFormatterFieldFixed term_type = new StreamFormatterFieldFixed("Terminal Type             ", Validator.getN(), 1, '0', false, true);
        StreamFormatterFieldFixed term_entry_cap = new StreamFormatterFieldFixed("Terminal Entry Capability ", Validator.getN(), 1, '0', false, true);
        StreamFormatterFieldFixed reserved = new StreamFormatterFieldFixed("Chip Condition Code       ", Validator.getN(), 1, '0', false, true);
        StreamFormatterFieldFixed new_service_dev = new StreamFormatterFieldFixed("New Service Development   ", Validator.getN(), 1, '0', false, true);
        StreamFormatterFieldFixed merchant_group = new StreamFormatterFieldFixed("Merchant Group Id         ", Validator.getN(), 2, '0', false, true);
        StreamFormatterFieldFixed transaction_indicator = new StreamFormatterFieldFixed("CCPS Transaction Indicator", Validator.getN(), 1, '0', false, true);
        StreamFormatterFieldFixed card_auth_rel_indicator = new StreamFormatterFieldFixed("Card Auth Rel Indicator   ", Validator.getN(), 1, '0', false, true);
        StreamFormatterFieldFixed electronic_commerce = new StreamFormatterFieldFixed("Electronic Commerce       ", Validator.getN(), 2, '0', false, true);
        StreamFormatterContainer all_fields = new StreamFormatterContainer();
        all_fields.add(term_type);
        all_fields.add(term_entry_cap);
        all_fields.add(reserved);
        all_fields.add(new_service_dev);
        all_fields.add(merchant_group);
        all_fields.add(transaction_indicator);
        all_fields.add(card_auth_rel_indicator);
        all_fields.add(electronic_commerce);
        stream = all_fields;
    }
}
