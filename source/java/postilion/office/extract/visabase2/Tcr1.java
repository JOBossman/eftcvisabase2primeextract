package postilion.office.extract.visabase2;

import postilion.core.message.Validator;
import postilion.core.message.stream.IStreamFormatter;
import postilion.core.message.stream.StreamFormatterContainer;
import postilion.core.message.stream.StreamFormatterFieldFixed;
import postilion.core.message.stream.StreamMessage;

public class Tcr1 extends StreamMessage
{
    public static class Field
    {

        public static final String TRANSACTION_CODE = new String("transaction code");
        public static final String CODE_QUALIFIER = new String("code qualifier");
        public static final String COMPONENT_SEQUENCE = new String("component sequence");
        public static final String ISSUER_WS_BIN = new String("issuer ws bin");
        public static final String ACQUIRER_WS_BIN = new String("acquirer ws bin");
        public static final String CHARGE_BACK_REF = new String("charge back ref");
        public static final String DOC_IND = new String("doc ind");
        public static final String MEMBER_MSG = new String("member msg");
        public static final String SPECIAL_COND_IND_1 = new String("special cond ind 1");
        public static final String SPECIAL_COND_IND_2 = new String("special cond ind 2");
        public static final String RESERVED = new String("reserved");
        public static final String CARD_ACCEPTOR_ID_CODE = new String("card acceptor id code");
        public static final String CARD_ACCEPTOR_TERM_ID = new String("card acceptor term id");
        public static final String NATIONAL_REIMBURSEMENT_FEE = new String("national reimbursement fee");
        public static final String MAIL_TEL_IND = new String("mail tel ind");
        public static final String SP_CHARGE_BACK_IND = new String("sp charge back ind");
        public static final String IF_TRACE_NUMBER = new String("if trace number");
        public static final String CARDHOLDER_ACTIVATED_TERM = new String("cardholder activated term");
        public static final String PREPAID_CARD_IND = new String("prepaid card ind");
        public static final String SERVICE_DEV_FIELD = new String("service dev field");
        public static final String AVS_RESP_CODE = new String("avs resp code");
        public static final String AUTH_SOURCE_CODE = new String("auth source code");
        public static final String PURCHASE_ID_FORMAT = new String("purchase id format");
        public static final String ATM_ACCOUNT_SELECTION = new String("atm account selection");
        public static final String INSTALLMENT_PMT_COUNT = new String("installment pmt count");
        public static final String PURCHASE_ID = new String("pucrhase id");
        public static final String CASH_BACK = new String("cash back");
        public static final String CHIP_COND_CODE = new String("chip condition code");
        public static final String POS_ENVIRONMENT = new String("pos environment");


        public Field()
        {
        }
    }


    private static IStreamFormatter stream;
    static final int MAX_SIZE = 168;
    static final byte FS = 45;

    public Tcr1()
    {
        super(168, stream);
        putField(Field.CODE_QUALIFIER, "0");
        putField(Field.COMPONENT_SEQUENCE, "1");
        putField(Field.DOC_IND, " ");
        putField(Field.SERVICE_DEV_FIELD, "0");
        putField(Field.SPECIAL_COND_IND_2, " ");
        putField(Field.PREPAID_CARD_IND, " ");
        putField(Field.PURCHASE_ID_FORMAT, " ");
    }

    public static void main(String args[])
    {
        Tcr1 tcr1 = new Tcr1();
        System.out.println(tcr1.describeStructure());
    }

    static
    {
        StreamFormatterContainer msg = new StreamFormatterContainer();
        IStreamFormatter transaction_code = new StreamFormatterFieldFixed(Field.TRANSACTION_CODE, Validator.getN(), 2);
        IStreamFormatter code_qualifier = new StreamFormatterFieldFixed(Field.CODE_QUALIFIER, Validator.getN(), 1);
        IStreamFormatter component_sequence = new StreamFormatterFieldFixed(Field.COMPONENT_SEQUENCE, Validator.getN(), 1);
        IStreamFormatter issuer_ws_bin = new StreamFormatterFieldFixed(Field.ISSUER_WS_BIN, Validator.getAns(), 6, ' ', true, true);
        IStreamFormatter acquirer_ws_bin = new StreamFormatterFieldFixed(Field.ACQUIRER_WS_BIN, Validator.getAns(), 6, ' ', true, true);
        IStreamFormatter charge_back_ref = new StreamFormatterFieldFixed(Field.CHARGE_BACK_REF, Validator.getN(), 6, '0', true, true);
        IStreamFormatter doc_ind = new StreamFormatterFieldFixed(Field.DOC_IND, Validator.getAns(), 1);
        IStreamFormatter member_msg = new StreamFormatterFieldFixed(Field.MEMBER_MSG, Validator.getAns(), 50, ' ', true, true);
        IStreamFormatter special_cond_ind_1 = new StreamFormatterFieldFixed(Field.SPECIAL_COND_IND_1, Validator.getAns(), 1);
        IStreamFormatter special_cond_ind_2 = new StreamFormatterFieldFixed(Field.SPECIAL_COND_IND_2, Validator.getAns(), 1);
        IStreamFormatter reserved = new StreamFormatterFieldFixed(Field.RESERVED, Validator.getAns(), 5, ' ', true, true);
        IStreamFormatter card_acceptor_id_code = new StreamFormatterFieldFixed(Field.CARD_ACCEPTOR_ID_CODE, Validator.getAns(), 15);
        IStreamFormatter card_acceptor_term_id = new StreamFormatterFieldFixed(Field.CARD_ACCEPTOR_TERM_ID, Validator.getAns(), 8);
        IStreamFormatter national_reimbursement_fee = new StreamFormatterFieldFixed(Field.NATIONAL_REIMBURSEMENT_FEE, Validator.getN(), 12, '0', true, true);
        IStreamFormatter mail_tel_ind = new StreamFormatterFieldFixed(Field.MAIL_TEL_IND, Validator.getNs(), 1);
        IStreamFormatter sp_charge_back_ind = new StreamFormatterFieldFixed(Field.SP_CHARGE_BACK_IND, Validator.getAns(), 1, ' ', true, true);
        IStreamFormatter if_trace_number = new StreamFormatterFieldFixed(Field.IF_TRACE_NUMBER, Validator.getAns(), 6, '0', true, true);
        IStreamFormatter cardholder_activated_term = new StreamFormatterFieldFixed(Field.CARDHOLDER_ACTIVATED_TERM, Validator.getAns(), 1);
        IStreamFormatter prepaid_card_ind = new StreamFormatterFieldFixed(Field.PREPAID_CARD_IND, Validator.getAns(), 1);
        IStreamFormatter reserved_2 = new StreamFormatterFieldFixed(Field.SERVICE_DEV_FIELD, Validator.getAns(), 1);
        IStreamFormatter avs_resp_code = new StreamFormatterFieldFixed(Field.AVS_RESP_CODE, Validator.getAns(), 1);
        IStreamFormatter auth_source_code = new StreamFormatterFieldFixed(Field.AUTH_SOURCE_CODE, Validator.getAns(), 1);
        IStreamFormatter purchase_id_format = new StreamFormatterFieldFixed(Field.PURCHASE_ID_FORMAT, Validator.getAns(), 1);
        IStreamFormatter atm_account_selection = new StreamFormatterFieldFixed(Field.ATM_ACCOUNT_SELECTION, Validator.getAns(), 1);
        IStreamFormatter installment_pmt_count = new StreamFormatterFieldFixed(Field.INSTALLMENT_PMT_COUNT, Validator.getAns(), 2);
        IStreamFormatter purchase_id = new StreamFormatterFieldFixed(Field.PURCHASE_ID, Validator.getAns(), 25, ' ', true, true);
        IStreamFormatter cash_back = new StreamFormatterFieldFixed(Field.CASH_BACK, Validator.getN(), 9, '0', false, true);
        IStreamFormatter chip_cond_code = new StreamFormatterFieldFixed(Field.CHIP_COND_CODE, Validator.getAnp(), 1, ' ', true, true);
        IStreamFormatter pos_environment = new StreamFormatterFieldFixed(Field.POS_ENVIRONMENT, Validator.getAn(), 1, ' ', true, true);
        msg.add(transaction_code);
        msg.add(code_qualifier);
        msg.add(component_sequence);
        msg.add(issuer_ws_bin);
        msg.add(acquirer_ws_bin);
        msg.add(charge_back_ref);
        msg.add(doc_ind);
        msg.add(member_msg);
        msg.add(special_cond_ind_1);
        msg.add(special_cond_ind_2);
        msg.add(reserved);
        msg.add(card_acceptor_id_code);
        msg.add(card_acceptor_term_id);
        msg.add(national_reimbursement_fee);
        msg.add(mail_tel_ind);
        msg.add(sp_charge_back_ind);
        msg.add(if_trace_number);
        msg.add(cardholder_activated_term);
        msg.add(prepaid_card_ind);
        msg.add(reserved_2);
        msg.add(avs_resp_code);
        msg.add(auth_source_code);
        msg.add(purchase_id_format);
        msg.add(atm_account_selection);
        msg.add(installment_pmt_count);
        msg.add(purchase_id);
        msg.add(cash_back);
        msg.add(chip_cond_code);
        msg.add(pos_environment);
        stream = msg;
    }
}

