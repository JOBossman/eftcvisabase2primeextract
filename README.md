# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is an extension of the visabase2 extract developed by ACI to allow for flexible extract configs.
* as a pre-requisite, requires that the visabase2 extract should be installed before installing this extension
* This allows you to configure, file format, source nodes from which records should be pulled as well 
* as specify terminal types for which the settlement report should
* be extracted for.
* Version 1.00.0000

### How do I get set up? ###

* Check postilion extract configuration document for setting up the extract job in post office

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact